//
//  CommentsMessageViewController.h
//  Messenger
//
//  Created by Ignacio Romero Zurbuchen on 8/15/14.
//  Copyright (c) 2014 Slack Technologies, Inc. All rights reserved.
//

#import <SLKTextViewController.h>
@class Event;
@interface CommentsMessageViewController : SLKTextViewController
@property (nonatomic)Event*event;
@end
