//
//  CommentsMessageViewController.m
//  Messenger
//
//  Created by Ignacio Romero Zurbuchen on 8/15/14.
//  Copyright (c) 2014 Slack Technologies, Inc. All rights reserved.
//

#import "CommentsMessageViewController.h"
#import "CommentTableViewCell.h"
#import "MessageTextView.h"
#import "Comment.h"
#import "Event+Comments.h"
#import <LoremIpsum/LoremIpsum.h>
#import "AccountManager.h"
#import "UIHelper.h"
#import <TTTTimeIntervalFormatter.h>
#import "NSDate+ISO8601.h"
#import "AccountManager.h"
#import "QuickSignInViewController.h"
static NSString *MessengerCellIdentifier = @"CommentCell";



@interface CommentsMessageViewController (){
    Comment *_tempComment;

}

@property (nonatomic, strong) NSMutableArray *messages;

//@property (nonatomic, strong) NSArray *users;
//@property (nonatomic, strong) NSArray *channels;
//@property (nonatomic, strong) NSArray *emojis;

//@property (nonatomic, strong) NSArray *searchResult;

@end

@implementation CommentsMessageViewController

- (id)init
{
    self = [super initWithTableViewStyle:UITableViewStylePlain];
    if (self) {
        // Register a subclass of SLKTextView, if you need any special appearance and/or behavior customisation.
        [self registerClassForTextView:[MessageTextView class]];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Register a subclass of SLKTextView, if you need any special appearance and/or behavior customisation.
        [self registerClassForTextView:[MessageTextView class]];
    }
    return self;
}

+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return UITableViewStylePlain;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.event getLimitedComments:0 complete:^(NSArray *comments) {
        self.messages = comments.mutableCopy;
        
        [self.tableView reloadData];
    }];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"评论";
    

    self.messages = [NSMutableArray array];
    
//    self.users = @[@"Allen", @"Anna", @"Alicia", @"Arnold", @"Armando", @"Antonio", @"Brad", @"Catalaya", @"Christoph", @"Emerson", @"Eric", @"Everyone", @"Steve"];
//    self.channels = @[@"General", @"Random", @"iOS", @"Bugs", @"Sports", @"Android", @"UI", @"SSB"];
//    self.emojis = @[@"m", @"man", @"machine", @"block-a", @"block-b", @"bowtie", @"boar", @"boat", @"book", @"bookmark", @"neckbeard", @"metal", @"fu", @"feelsgood"];

    self.bounces = YES;
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = NO;
    self.inverted = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:MessengerCellIdentifier];

//有了相机功能了再用
//    [self.leftButton setImage:[UIImage imageNamed:@"icn_upload"] forState:UIControlStateNormal];
//    [self.leftButton setTintColor:[UIColor grayColor]];
    
    [self.rightButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
    
    [self.textInputbar.editorTitle setTextColor:[UIColor darkGrayColor]];
    [self.textInputbar.editortLeftButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
    [self.textInputbar.editortRightButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
    
    self.textInputbar.autoHideRightButton = YES;
    self.textInputbar.maxCharCount = 256;
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
    //self.textInputbar.counterPosition = SLKCounterPositionTop;

    self.typingIndicatorView.canResignByTouch = YES;
    

    // 能获取好友了再用
//    [self registerPrefixesForAutoCompletion:@[@"@", @"#", @":"]];
}


#pragma mark - Action Methods



#pragma mark - Overriden Methods

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status
{
    // Notifies the view controller that the keyboard changed status.
}

- (void)textWillUpdate
{
    // Notifies the view controller that the text will update.

    [super textWillUpdate];
}

- (void)textDidUpdate:(BOOL)animated
{
    // Notifies the view controller that the text did update.

    [super textDidUpdate:animated];
}

- (void)didPressLeftButton:(id)sender
{
    // Notifies the view controller when the left button's action has been triggered, manually.
    
    [super didPressLeftButton:sender];
}

- (void)didPressRightButton:(id)sender
{
    
    
    if (![[AccountManager sharedAccountManager] checkUserSigned]) {
        
        [self.textView resignFirstResponder];
        
        [self showInAppAuthView];

        return;
    }
    
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    
    // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
    [self.textView refreshFirstResponder];
    
    Comment *message = [Comment new];
    
    message.user = [[AccountManager sharedAccountManager] currenSimpleUser];
    
    message.comment_text = [self.textView.text copy];
    message.comment_date = [[NSDate date] ISOString];
    _tempComment = message;
    
    [super didPressRightButton:sender];
    

    
    [_event comment:message.comment_text  complete:^(bool succeed) {
       
        if (succeed) {
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
            UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
            
            [self.tableView beginUpdates];
            [self.messages insertObject:_tempComment atIndex:0];
            [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
            [self.tableView endUpdates];
            
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
            
            // Fixes the cell from blinking (because of the transform, when using translucent cells)
            // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            _tempComment = nil;
        }
        
    }];
}



- (NSString *)keyForTextCaching
{
    return [[NSBundle mainBundle] bundleIdentifier];
}




- (void)willRequestUndo
{

    [super willRequestUndo];
}


- (BOOL)canPressRightButton
{
    return [super canPressRightButton];
}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tableView]) {
        return self.messages.count;
    }
//    else {
//        return self.searchResult.count;
//    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tableView]) {
        return [self messageCellForRowAtIndexPath:indexPath];
    }
//    else {
//        return [self autoCompletionCellForRowAtIndexPath:indexPath];
//    }
    
    return nil;
}

- (CommentTableViewCell *)messageCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = (CommentTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
    

    Comment *message = self.messages[indexPath.row];
    
    cell.nameLabel.text = message.user.username;
    cell.commentLabel.text = message.comment_text;
    
    cell.nameLabel.text = message.user.username?:@"未知用户";
    
    
    FAKFontAwesome*fa;
    if ([message.user.gender isEqualToString:@"M"]) {
        fa = [FAKFontAwesome maleIconWithSize:12];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper maleColor]];
    }else if ([message.user.gender isEqualToString:@"F"]) {
        fa = [FAKFontAwesome femaleIconWithSize:12];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper femaleColor]];
    }else{
        fa = [FAKFontAwesome userIconWithSize:12];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper ghostColor]];
    }
    
    cell.genderLabel.attributedText = fa.attributedString;
    
    cell.commentDateLabel.text = message.formatedCommentDate;
    
    [cell.profileView loadAvatarWithURLComponents:message.user.avatar_path];
    
    // Cells must inherit the table view's transform
    // This is very important, since the main table view may be inverted
    cell.transform = self.tableView.transform;
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tableView]) {
        Comment *message = self.messages[indexPath.row];
        
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentLeft;
        
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16.0],
                                     NSParagraphStyleAttributeName: paragraphStyle};
        
        CGFloat width = CGRectGetWidth(tableView.frame)-kAvatarSize;
        width -= 25.0;
        
        CGRect titleBounds = [message.user.username boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:NULL];
        CGRect bodyBounds = [message.comment_text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:NULL];
        
        if (message.comment_text.length == 0) {
            return 0.0;
        }
        
        CGFloat height = CGRectGetHeight(titleBounds);
        height += CGRectGetHeight(bodyBounds);
    

        if (height < kMinimumHeight) {
            height = kMinimumHeight;
        }
        
        return height;
    }
    else {
        return kMinimumHeight;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if ([tableView isEqual:self.autoCompletionView]) {
//        UIView *topView = [UIView new];
//        topView.backgroundColor = self.autoCompletionView.separatorColor;
//        return topView;
//    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if ([tableView isEqual:self.autoCompletionView]) {
//        return 0.5;
//    }
    return 0.0;
}


#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if ([tableView isEqual:self.autoCompletionView]) {
//        
//        NSMutableString *item = [self.searchResult[indexPath.row] mutableCopy];
//        
//        if ([self.foundPrefix isEqualToString:@"@"] && self.foundPrefixRange.location == 0) {
//            [item appendString:@":"];
//        }
//        else if ([self.foundPrefix isEqualToString:@":"]) {
//            [item appendString:@":"];
//        }
//        
//        [item appendString:@" "];
//        
//        [self acceptAutoCompletionWithString:item keepPrefix:YES];
//    }
}


#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you ovveride this method, to call super.
    [super scrollViewDidScroll:scrollView];
}


#pragma mark - UIScrollViewDelegate Methods

/** UITextViewDelegate */
- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return [super textView:textView shouldChangeTextInRange:range replacementText:text];
}

- (void)textViewDidChangeSelection:(SLKTextView *)textView
{
    [super textViewDidChangeSelection:textView];
}

@end
