//
//  CompressingLogFileManager.h
//  Braveaux
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>
@interface CompressingLogFileManager : DDLogFileManagerDefault{
    BOOL upToDate;
    BOOL isCompressing;
}

@end
