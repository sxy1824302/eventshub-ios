//
//  NSDate+Helpers.m
//  EventsHub
//
//  Created by alexander on 1/7/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "NSDate+Helpers.h"


#define CYCalendarUnitYear NSCalendarUnitYear
#define CYCalendarUnitMonth NSCalendarUnitMonth
#define CYCalendarUnitWeek NSCalendarUnitWeekOfYear
#define CYCalendarUnitDay NSCalendarUnitDay
#define CYCalendarUnitHour NSCalendarUnitHour
#define CYCalendarUnitMinute NSCalendarUnitMinute
#define CYCalendarUnitSecond NSCalendarUnitSecond
#define CYCalendarUnitWeekday NSCalendarUnitWeekday
#define CYDateComponentUndefined NSDateComponentUndefined


@implementation NSDate (Helpers)

- (NSDate *)beginningOfDay {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:CYCalendarUnitYear | CYCalendarUnitMonth | CYCalendarUnitDay fromDate:self];
    
    return [calendar dateFromComponents:components];
}

- (NSDate *)endOfDay {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 1;
    
    return [[calendar dateByAddingComponents:components toDate:[self beginningOfDay] options:0] dateByAddingTimeInterval:-1];
}
#pragma mark -

- (NSDate *)beginningOfWeek {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:CYCalendarUnitYear | CYCalendarUnitMonth | CYCalendarUnitWeekday | CYCalendarUnitDay fromDate:self];
    
    NSInteger offset = components.weekday - (NSInteger)calendar.firstWeekday;
    components.day = components.day - offset;
    
    return [calendar dateFromComponents:components];
}

- (NSDate *)endOfWeek {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.weekOfMonth = 1;
    
    return [[calendar dateByAddingComponents:components toDate:[self beginningOfWeek] options:0] dateByAddingTimeInterval:-1];
}


- (NSDate *)endOfNextWeek {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    components.weekOfMonth = 1;
    
    return [calendar dateByAddingComponents:components toDate:[self endOfWeek] options:0];
}



#pragma mark -

- (NSDate *)beginningOfMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:CYCalendarUnitYear | CYCalendarUnitMonth fromDate:self];
    
    return [calendar dateFromComponents:components];
}

- (NSDate *)endOfMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 1;
    
    return [[calendar dateByAddingComponents:components toDate:[self beginningOfMonth] options:0] dateByAddingTimeInterval:-1];
}
- (NSDate *)endOfNextMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = 1;
    
    return [calendar dateByAddingComponents:components toDate:[self endOfMonth] options:0];
}




+ (NSDate*)dateFromYear:(NSInteger)year{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [NSDateComponents new];
    components.year = year;
    components.month=1;
    components.day = 1;
    
   return  [calendar dateFromComponents:components];
}


@end
