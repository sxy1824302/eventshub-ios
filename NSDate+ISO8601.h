//
//  NSDate+ISO8601.h
//  EventsHub-Demo1
//
//  Created by alexander on 11/4/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ISO8601)
-(NSString*)ISOString;
+(NSDate*)dateFromISOString:(NSString*)dateS;
@end
