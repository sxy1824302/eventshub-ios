//
//  NSDate+ISO8601.m
//  EventsHub-Demo1
//
//  Created by alexander on 11/4/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "NSDate+ISO8601.h"

static NSDateFormatter *dateFormatter;

static NSDateFormatter *decodeDateFormatter;

@implementation NSDate (ISO8601)

-(NSString*)ISOString{
    if (!dateFormatter) {
        dateFormatter = [NSDateFormatter new];
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    }

    return [dateFormatter stringFromDate:self];
}

+(NSDate*)dateFromISOString:(NSString*)dateS{
    if (!decodeDateFormatter) {
        decodeDateFormatter = [NSDateFormatter new];
        decodeDateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        decodeDateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        decodeDateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    }
    NSDate *date= [decodeDateFormatter dateFromString:dateS];
    return date;
}

@end
