//
//  TagCollectionViewCell.h
//  EventsHub
//
//  Created by alexander on 1/20/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventTagView.h"
@interface TagCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet EventTagView *stickView;
@end
