//
//  PhotoPicker.h
//  EventsHub
//
//  Created by 孙翔宇 on 3/18/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
#import "ELCImagePickerController.h"

@interface PhotoPicker : NSObject

@property(nonatomic)BOOL allowEdit;

@property (nonatomic,weak)UIViewController<UIActionSheetDelegate,ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate>*attatedViewCotroller;

-(void)showOnView:(UIView*)view withMaxCount:(NSInteger)maxCount;

-(void)showAlbumPickerWithPhotoLimit:(NSInteger)limit;
-(void)showCamara;
@end
