//
//  CircleButton.m
//  EventsHub
//
//  Created by apple on 15/1/29.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import "CircleButton.h"

@implementation CircleButton{
    UIBezierPath *_outerPath;
    CAShapeLayer *circle ;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/





- (void)initializePayload{
    
    CGFloat d = MIN(self.bounds.size.width, self.bounds.size.height);
    
    CGFloat outerWidth = 3;
    
    _outerPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds)) radius:d*0.5-outerWidth startAngle:0 endAngle:2*M_PI clockwise:YES];
    
    _outerPath.lineWidth = outerWidth;
    
    
    CAShapeLayer *mask = [CAShapeLayer layer];
    mask.path =_outerPath.CGPath;
    mask.fillColor=[UIColor whiteColor].CGColor;
    self.imageView.layer.mask = mask;
    
    
    circle = [CAShapeLayer layer];
    circle.fillColor = NULL;
    circle.path = _outerPath.CGPath;
    
    [self.layer addSublayer:circle];
    
}

@end
