//
//  NetworkManager.m
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "NetworkManager.h"
#import "MessageCenter.h"

@implementation NetworkManager{
    
    AFNetworkReachabilityManager *_reachabilityManager;
    
}
+ (id)sharedNetworkManager
{
    static dispatch_once_t onceQueue;
    static NetworkManager *networkManager = nil;
    
    dispatch_once(&onceQueue, ^{ networkManager = [[self alloc] init]; });
    return networkManager;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        

        _reachabilityManager = [AFNetworkReachabilityManager managerForDomain:HostAPI];
        
        [_reachabilityManager startMonitoring];

        [_reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
           
            switch (status) {
                case AFNetworkReachabilityStatusReachableViaWWAN:
                     DDLogVerbose(@"using wwlan");
                    break;
                case AFNetworkReachabilityStatusReachableViaWiFi:
                    DDLogVerbose(@"using wifi");
                    break;
                default:
                    break;
            }
            
        }];
        
        self.networkStatus = _reachabilityManager.networkReachabilityStatus;
        
        
        
        
        _JSONEncodeHttpRequestManager = [[EHHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:HostAPI]];
        
        [_JSONEncodeHttpRequestManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
        
        
        _fileHttpRequestManager = [[EHHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:HostAPI]];
        
        
        _downloadHttpRequestManager = [AFHTTPSessionManager manager];
        _downloadHttpRequestManager.responseSerializer =[AFImageResponseSerializer serializer];
        
        
        _generalHttpRequestManager = [[AFHTTPRequestOperationManager alloc] init];
        
        
        
    }
    return self;
}


- (void)perform:(NetworkActionBlock)action{
    
    @autoreleasepool {
        
        action(self,^(void) {
            
            
        },^(NSError *error, NSDictionary *responseObject) {
            
            
            if (responseObject[@"message"]) {
                
                [MessageCenter showMessage:[NSString stringWithFormat:@"%@ \n %@",responseObject[@"code"], responseObject[@"message"]]  withType:MessageTypeError inViewController:nil];
                
            }else{
                
                [MessageCenter showMessage:error.localizedDescription?error.localizedDescription:responseObject.description  withType:MessageTypeError inViewController:nil];
            
            }
            
        });
    }
    
}
-(void)downloadImage:(NSString*)iconURL image:(void(^)(UIImage *image))complete{
    [self perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        
        [self.downloadHttpRequestManager GET:iconURL parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            complete(responseObject);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            complete(nil);
            failed(error,nil);;
        }];
    }];
}
@end
