//
//  HelpContactViewController.m
//  EventsHub
//
//  Created by alexander on 2/13/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "HelpContactViewController.h"
@import MessageUI;

#define email @"contact@ijubar.com"


@interface HelpContactViewController ()<UITableViewDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *mailLabel;

@end

@implementation HelpContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"帮助与反馈", nil);
    self.mailLabel.text = NSLocalizedString(@"联系邮件", nil);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row==0) {
        MFMailComposeViewController*mfv = [[MFMailComposeViewController alloc] init];
        
        mfv.mailComposeDelegate = self;
        [mfv setToRecipients:@[email]];
        [mfv setSubject:@"聚吧-使用反馈"];
        [self presentViewController:mfv animated:YES completion:^{
            
        }];
        
    }else if (indexPath.row==1){
        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"15884506228"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }
    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    if (error) {
        
    }else {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}
@end
