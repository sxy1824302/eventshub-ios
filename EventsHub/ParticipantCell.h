//
//  ParticipantCell.h
//  EventsHub
//
//  Created by alexander on 1/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "SWTableViewCell.h"
#import "EHSignStateView.h"
#import "ProfileIconView.h"
@class UserAccount;

@interface ParticipantCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;


@property (weak, nonatomic) IBOutlet EHSignStateView *signStateView;

@property (weak, nonatomic) IBOutlet UILabel *noteLabel;


@end


@interface GuestParticipantCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;

@property (nonatomic) UserAccount *user;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;


@property (weak, nonatomic) IBOutlet ProfileIconView *iconView;

@end
