//
//  ParticipantsListTableViewController.h
//  EventsHub
//
//  Created by alexander on 1/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseUIViewController.h"
@class Event;

@interface ParticipantsListTableViewController : BaseTableViewController
@property (nonatomic,weak)Event *event;

@end
