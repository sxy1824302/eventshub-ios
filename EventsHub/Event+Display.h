//
//  Event+Display.h
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Event.h"

@interface Event (Display)
-(NSString*)startAndEndDisplayString;
@end
