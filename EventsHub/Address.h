//
//  Address.h
//  EventsHub
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "JSONModel.h"
#import "Location.h"
@interface Address : JSONModel
@property (nonatomic)NSString *country;
@property (nonatomic)Location* geopoint;
@property (nonatomic)NSString* province;
@property (nonatomic)NSString* street1;
@property (nonatomic)NSString* city;
@property (nonatomic)NSString *district;
@property (nonatomic)NSString* street2;
@end
