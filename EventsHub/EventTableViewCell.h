//
//  EventTableViewCell.h
//  EventsHub
//
//  Created by alexander on 2/10/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventInfoLabel.h"

@class Event;

@interface EventTableViewCell : UITableViewCell
@property (nonatomic,weak)Event*event;

@property (weak, nonatomic) IBOutlet UIImageView *activityTitleImage;

@property (weak, nonatomic) IBOutlet UILabel *activityNameLabel;


@property (weak, nonatomic) IBOutlet UILabel *ActivityTimeLabel;

@property (weak, nonatomic) IBOutlet UILabel *activityLikesLabel;

@property (weak, nonatomic) IBOutlet UILabel *activityMemberLabel;

@property (weak, nonatomic) IBOutlet UILabel *adressLabel;

@property (weak, nonatomic) IBOutlet EventInfoLabel *infoLabel;
@end
