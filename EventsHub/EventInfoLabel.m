//
//  EventInfoLabel.m
//  EventsHub
//
//  Created by 孙翔宇 on 2/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "EventInfoLabel.h"
#import "UIHelper.h"
@implementation EventInfoLabel

-(void)awakeFromNib{
    self.bounds = CGRectMake(0, 0, self.bounds.size.width, GuidlineMetric(20));
    self.font =[UIHelper defaultFontWithSize:GuidlineMetric(14)];
    self.textColor = [UIColor whiteColor];
    self.layer.cornerRadius = 10;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
