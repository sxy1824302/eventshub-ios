//
//  CitieSelectionTableViewController.m
//  EventsHub
//
//  Created by alexander on 2/13/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "CitieSelectionTableViewController.h"
#import "LocationManager.h"
#import "EventCityModel.h"
@interface CitieSelectionTableViewController (){
    NSArray*_cities;
    
    NSString*_selectedCity;
}

@end

@implementation CitieSelectionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = NSLocalizedString(@"活动城市", nil);
    _selectedCity = [[LocationManager sharedLocationManager] eventCity];
    
    
    [LocationManager  getEventsCities:^(NSArray *cities) {
        
        _cities = cities;
        
        [self.tableView reloadData];
        
    }];
    
    self.tableView.tableFooterView = [UIView new];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cities.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityCell" forIndexPath:indexPath];
    EventCityModel*city = _cities[indexPath.row];
    cell.textLabel.text = city.city_name;
    if([city.city_name isEqualToString:_selectedCity]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    EventCityModel*city = _cities[indexPath.row];
    
    _selectedCity = city.city_name;
    
    [[LocationManager sharedLocationManager] setEventCity:city.city_name];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
