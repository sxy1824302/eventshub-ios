//
//  QuickSignInViewController.h
//  EventsHub
//
//  Created by alexander on 1/8/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIViewController+MaryPopin.h>
@interface QuickSignInViewController : UIViewController

@end


@interface UIViewController (Auth)
-(void)showInAppAuthView;
@end
