//
//  Tag.h
//  EventsHub
//
//  Created by 孙翔宇 on 12/30/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "JSONModel.h"

@interface Tag : JSONModel

@property (nonatomic)NSString *pinyin;

@property (nonatomic)NSString *tag;

@property (nonatomic)NSNumber *tag_id;

@end
