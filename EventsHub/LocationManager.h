//
//  LocationManager.h
//  EventsHub
//
//  Created by alexander on 1/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMapKit.h"
#import <CoreLocation/CoreLocation.h>
#import "EventCityModel.h"
@class Address;


#define kCachedUserCity @"kCachedUserCity"
extern NSString*const UserLocationCityUpdatedNotification ;
extern NSString*const UserLocationUpdatedNotification ;

extern NSString*const EventCityUpdatedNotification;

@interface LocationManager : NSObject<BMKGeoCodeSearchDelegate,BMKLocationServiceDelegate,CLLocationManagerDelegate>
@property (nonatomic)CLLocation *currentUserLocation;

+ (id)sharedLocationManager;
-(void)reloadLocateUser;
//


@property (nonatomic)NSString*eventCity;


+(void)getEventsCities:(void(^)(NSArray* tags))completeBlock;
@end
