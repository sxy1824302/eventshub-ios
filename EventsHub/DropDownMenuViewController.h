//
//  DropDownMenuViewController.h
//  EventsHub
//
//  Created by alexander on 1/6/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "BaseUIViewController.h"

#import "EHDropDownBar.h"


@interface DropDownMenuViewController : BaseUIViewController
@property (nonatomic)BOOL opened;
@property (nonatomic)EHDropDownBar*menu;
-(void)menuChangedWithIndex:(NSInteger)idx;
-(void)setDropDownMenuTitles:(NSArray*)titles;
- (void) showTopMenu;
- (void) hideTopMenu ;
@end
