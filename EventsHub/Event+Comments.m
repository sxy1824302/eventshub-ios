//
//  Event+Comments.m
//  EventsHub
//
//  Created by alexander on 4/10/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event+Comments.h"
#import "NetworkManager.h"
@implementation Event (Comments)
-(void)getLimitedComments:(NSInteger)count complete:(void(^)(NSArray* comments))succeedBlock{
    
    NSDictionary*para;
    
    
    if (count!=0) {
        para =  @{@"limit":@(count)};
    }
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/events/%@/comments",self._id] parameters:para success:^(NSURLSessionDataTask *task, id responseObject) {
            
            NSError*error;
            
            NSArray*comments = [Comment arrayOfModelsFromDictionaries:responseObject[@"comments"] error:&error];
            
            succeedBlock(comments);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);;
            
            succeedBlock(nil);
        }];
    }];
}
-(void)comment:(NSString*)comment complete:(void(^)(bool succeed))succeedBlock{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager POST:[NSString stringWithFormat:@"/events/%@/comments",self._id] parameters:@{@"comment_text":comment} success:^(NSURLSessionDataTask *task, id responseObject) {
            
 
            succeedBlock(YES);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);;
            
            succeedBlock(NO);
        }];
    }];
}
@end
