//
//  NSString+Interest.m
//  EventsHub
//
//  Created by alexander on 11/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Event+Interest.h"
#import "NetworkManager.h"
@implementation Event (Interest)

-(void)interest:(void(^)(NSDictionary* responce,BOOL success))completeBlock{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.fileHttpRequestManager POST:[NSString stringWithFormat:@"/events/%@/like",self._id] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"code"] integerValue]==1) {
                
                completeBlock(responseObject,YES);
                
            }else{
                
                completeBlock(responseObject,NO);
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            completeBlock(responseObject,NO);
            failed(error,responseObject);;
        }];
    }];
}

-(void)uninterest:(void(^)(NSDictionary* responce,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.fileHttpRequestManager DELETE:[NSString stringWithFormat:@"/events/%@/like",self._id] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
           
            if ([responseObject[@"code"] integerValue]==0) {
                
                completeBlock(responseObject,YES);
                
            }else{
                
                completeBlock(responseObject,NO);
            }
            
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            completeBlock(responseObject,NO);

            failed(error,responseObject);;
            
        }];
    }];
}

-(void)checkInterest:(void(^)(NSDictionary* responce,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/events/%@/like",self._id] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
       
            completeBlock(responseObject,YES);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
 
            completeBlock(responseObject,NO);
            failed(error,responseObject);;
            
        }];
    }];
}

@end
