//
//  ImageDataConvertor.m
//  EventsHub
//
//  Created by 孙翔宇 on 2/8/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "ImageDataConvertor.h"

#define kMaxMegaByte 2

@implementation ImageDataConvertor


+(NSData*)convertImage:(UIImage*)image infoBlock:(void(^)(float compression,float megabytes, BOOL *invalidate, NSString *httpType))infoCheck{
    
    BOOL validData = YES;
    
    NSData*data = UIImageJPEGRepresentation(image, kCompressionRate);
    
    double  perMBBytes = 1024*1024;
    
    infoCheck(kCompressionRate,data.length/perMBBytes,&validData,@"image/jpeg");
    
    if (!validData) {
        data = nil;
    }
    return data;
}


+(void)convertImage:(NSArray*)images forEachImages:(void(^)(NSData*aData, float compression,float megabytes, NSString *httpType))eachImage{
    
    double  perMBBytes = 1024*1024;
    
    [images enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        float rate = 1;
        
        NSData*data= UIImageJPEGRepresentation(obj, rate);
        
        
        if (data.length>perMBBytes*kMaxMegaByte){
            rate = kCompressionRate;
            
            data= UIImageJPEGRepresentation(obj, rate);
        }
       
        
        eachImage(data, rate,data.length/perMBBytes,@"image/jpeg");
    }];
    

}

+(UIImage*)thumbnmailData:(NSData*)imageData MIMEType:(NSString *)MIMEType{
    CGDataProviderRef   provider;
    CGImageRef          sourceImage;
    CGFloat             thumbnailSize;
    
    
    CGImageRef thumbnail;
    
    // Latch thumbnailSize for performance, and also to prevent it changing out from underneath us.
    
    thumbnailSize = 80;
    
    
    // Set up the source CGImage.
    
    provider = CGDataProviderCreateWithCFData( (CFDataRef) imageData);
    assert(provider != NULL);
    
    if ( [MIMEType isEqual:@"image/jpeg"] ) {
        sourceImage = CGImageCreateWithJPEGDataProvider(provider, NULL, true, kCGRenderingIntentDefault);
    } else if ( [MIMEType isEqual:@"image/png"] ) {
        sourceImage =  CGImageCreateWithPNGDataProvider(provider, NULL, true, kCGRenderingIntentDefault);
    } else {
        sourceImage = NULL;
    }
    
    // Render it to a bitmap context and then create an image from that context.
    
    if (sourceImage != NULL) {
        static const CGFloat kWhite[4] = {0.0f, 0.0f, 0.0f, 1.0f};
        CGColorRef      white;
        CGContextRef    context;
        CGColorSpaceRef space;
        
        space = CGColorSpaceCreateDeviceRGB();
        assert(space != NULL);
        
        white = CGColorCreate(space, kWhite);
        assert(white != NULL);
        
        // Create the context that's thumbnailSize x thumbnailSize.
        
        context = CGBitmapContextCreate(NULL, thumbnailSize, thumbnailSize, 8, 0, space, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
        if (context != NULL) {
            CGRect  r;
            
            // Make sure anything we don't cover comes out white.  While the next
            // steps ensures that we cover the entire image, there's a possibility
            // that we're dealing with a transparent PNG.
            
            CGContextSetFillColorWithColor(context, white);
            CGContextFillRect(context, CGRectMake(0.0f, 0.0f, thumbnailSize, thumbnailSize));
            
            // Calculate the drawing rectangle so that the image fills the entire
            // thumbnail.  That is, for a tall image, we scale it so that the
            // width matches thumbnailSize and the it's centred vertically.
            // Similarly for a wide image.
            
            r = CGRectZero;
            r.size.width  = CGImageGetWidth(sourceImage);
            r.size.height = CGImageGetWidth(sourceImage);
            if (r.size.height > r.size.width) {
                // tall image
                r.size.height = (r.size.height / r.size.width) * thumbnailSize;
                r.size.width  = thumbnailSize;
                r.origin.y = - ((r.size.height - thumbnailSize) / 2);
            } else {
                // wide image
                r.size.width  = (r.size.width / r.size.height) * thumbnailSize;
                r.size.height = thumbnailSize;
                r.origin.x = - ((r.size.width - thumbnailSize) / 2);
            }
            
            // Draw the source image and get then create the thumbnail from the
            // context.
            
            CGContextDrawImage(context, r, sourceImage);
            
            thumbnail = CGBitmapContextCreateImage(context);
            assert(thumbnail != NULL);
        }
        
        CGContextRelease(context);
        CGColorSpaceRelease(space);
        CGColorRelease(white);
    }
    
    CGImageRelease(sourceImage);
    CGDataProviderRelease(provider);
    
    return [UIImage imageWithCGImage:thumbnail];
}
@end
