//
//  ProfileIconView.h
//  Braveaux
//
//  Created by alexander on 2/28/14.
//  Copyright (c) 2014 alexander. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^TapCallBack)(void);



IB_DESIGNABLE
@interface ProfileIconView : UIControl

@property (nonatomic,strong) UIImageView *imageView;


@property (nonatomic,strong)TapCallBack tapCallBack;


-(void)loadAvatarWithURLComponents:(NSString*)path;

-(void)loadAvatarWithURLComponents:(NSString*)path placeHolder:(UIImage*)placeholder;

-(void)setUpdating:(BOOL)updating;

-(void)reset;
@end
