//
//  Event.h
//  EventsHub
//
//  Created by alexander on 11/25/14.
//  Copyright (c) 2014 ;. All rights reserved.
//

#import "JSONModel.h"
#import "Address.h"




//@class Address;
@interface Event : JSONModel

@property (nonatomic)NSString *contact_phone;
@property (nonatomic)NSString *fee_description;
@property (nonatomic)NSString *pio;
@property (nonatomic)NSNumber *require_participants_phone;
@property (nonatomic)NSNumber *total_participants_limit;
@property (nonatomic)NSString *date_start;
@property (nonatomic)Address *address;
@property (nonatomic)NSString *date_end;
@property (nonatomic)NSString *owner_name;
@property (nonatomic)NSString *owner;
@property (nonatomic)NSString *title;
@property (nonatomic)NSString *eventDescription;
@property (nonatomic)NSArray *tags;
@property (nonatomic)NSDictionary* config;


@property (nonatomic)NSNumber *comments_count;
@property (nonatomic)NSNumber *like_count;
@property (nonatomic)NSNumber *privacy_state;
@property (nonatomic)NSArray *posters;
@property (nonatomic)NSNumber *participant_count;
@property (nonatomic)NSString *_id;
@property (nonatomic)NSString *state;



-(BOOL)isExpired;
@end
