//
//  Event+Register.h
//  EventsHub
//
//  Created by alexander on 4/23/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event.h"

@interface Event (Register)
/***
 签到和反签到
 ***/
-(void)signup:(BOOL)isRegister participantId:(NSString*)participantId participantName:(NSString*)participantName handle:(void(^)(BOOL success))completeBlock;


@end
