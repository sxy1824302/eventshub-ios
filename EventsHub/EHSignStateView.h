//
//  EHSignStateView.h
//  EventsHub
//
//  Created by apple on 15/1/13.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIHelper.h"




@interface EHSignStateView : UIView

-(void)userIsSigned:(BOOL)isSign;

@end
