//
//  TencentService.h
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TencentOpenAPI/TencentOAuthObject.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApi.h>
#import "SocialDefine.h"
#import <TencentOpenAPI/QQApiInterface.h>


@class TencentServiceManager;


@interface TencentService : NSObject<NSCoding,SocialService>

-(TencentServiceManager*)manager;

@end


@interface TencentServiceManager : NSObject<TencentSessionDelegate,QQApiInterfaceDelegate>
@property (nonatomic) TencentService*service;
- (instancetype)initWithService:(TencentService*)servic;


-(void)logoutSerivice;

-(void)startTencentAuthComplete:(AuthCompletedBlock)complete;

-(void)getUserInfoComplete:(GetProfileCompletedBlock)complete;

//-(void)shareWithTitle:(NSString*)title description:(NSString*)desc  thumbNail:(NSString*)thumbnailLink andLink:(NSString*)link andFinish:(ShareCompletedBlock)action;

-(void)shareToQQWithTitle:(NSString*)title description:(NSString*)desc  thumbNail:(UIImage*)thumbnail andLink:(NSString*)link andFinish:(ShareCompletedBlock)action;
@end
