//
//  Event+Register.m
//  EventsHub
//
//  Created by alexander on 4/23/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event+Register.h"
#import "NetworkManager.h"
@implementation Event (Register)
//签到和反签到
-(void)signup:(BOOL)isRegister participantId:(NSString*)participantId participantName:(NSString*)participantName handle:(void(^)(BOOL success))completeBlock{
    NSMutableDictionary*md = @{@"check":@(isRegister),@"user_name":participantName}.mutableCopy;
    
    if (participantId) {
        [md addEntriesFromDictionary:@{@"user_id":participantId}];
    }
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager PUT:[NSString stringWithFormat:@"/events/%@/attend",self._id] parameters:md success:^(NSURLSessionDataTask *task, id responseObject) {
            
            completeBlock(YES);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            failed(error,responseObject);
            completeBlock(NO);
            
        }];
    }];
}

@end
