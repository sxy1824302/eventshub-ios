//
//  EHTabBar.h
//  EventsHub
//
//  Created by alexander on 12/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EHTabBarTappedDelegate <NSObject>

-(void)tabBarDidClickedButtonAtIndex:(NSInteger)index;


@end

IB_DESIGNABLE
@interface EHTabBar : UIView

@property(nonatomic)BOOL warning;

@property (nonatomic)id<EHTabBarTappedDelegate>delegate;

@property (nonatomic)NSInteger fontSize;
@property (nonatomic)   NSMutableArray *buttons;

-(void)applyNewTitles:(NSArray*)titles;


@end
