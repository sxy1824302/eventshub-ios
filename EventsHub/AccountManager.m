//
//  AccountManager.m
//  EventsHub
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "AccountManager.h"
#import "NetworkManager.h"
#import "Config.h"
#import "MessageCenter.h"
#import "OpenUDID.h"
#import "ProfileIconView.h"


@interface AccountManager ()<WBHttpRequestDelegate>{
  
}

@end

@implementation AccountManager{

}

+ (id)sharedAccountManager
{
    static dispatch_once_t onceQueue;
    static AccountManager *accountControl = nil;
    
    dispatch_once(&onceQueue, ^{ accountControl = [[self alloc] init]; });
    return accountControl;
}

#pragma mark weibo

-(SimpleUserModel*)currenSimpleUser{
    SimpleUserModel*sm = [SimpleUserModel new];
    
    sm.username = _userAccount.username;
    sm.avatar_path = _userAccount.avatar_path;
    
    return sm;
}

- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    DDLogVerbose(@"WB result %@",result);
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error;
{
   DDLogVerbose(@"WB error:%@",error);
}

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}


-(NSString*)currentAccountID{
    return self.userAccount._id;
}

-(BOOL)checkUserSigned{
    return self.currentAccountID != nil;
}

-(void)userLogOut{
    self.userAccount = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:User_LogoutNotification object:nil];
}



-(void)updateUserInfo:(NSDictionary*)info complete:(void(^)(bool succeed))complete{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager PUT:[NSString stringWithFormat:@"/users/%@",self.userAccount._id] parameters:info success:^(NSURLSessionDataTask *task, id responseObject) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:User_InfoUpdatedNotification object:nil];
            
            
            [info enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
               
                [self.userAccount setValue:obj forKey:key];
                
            }];
            
            complete(YES);

        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            complete(NO);
            
            failed(error,responseObject);;
        }];
    }];
    
}


-(void)updateUserAvatar:(NSData*)avatar complete:(void(^)(bool succeed))complete{
    
    if(avatar.length<1){
        return;
    }
    
    self.updating = YES;
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
       
        [networkManager.fileHttpRequestManager POST:@"/profile/photo" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:avatar name:@"image" fileName:[NSString stringWithFormat:@"%@-%@",[NSDate date].description,[[AccountManager sharedAccountManager] currentAccountID]] mimeType:@"image/png"];
            
        } progress:nil  success:^(NSURLSessionDataTask *task, id responseObject) {
            self.updating = NO;
            self.userAccount.avatar_path = responseObject[@"message"][@"name"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:User_InfoUpdatedNotification object:self.userAccount.avatar_path];
            
            if (complete) {
                complete(YES);
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            self.updating = NO;
            if (complete) {
                complete(NO);
            }
            
            failed(error,responseObject);;
            
        }];
    }];
    
}
-(void)setUserAccount:(UserAccount *)userAccount{

    _userAccount = userAccount;
    
}

-(void)updateDeviceToken:(NSString*)token{
    
    NSString* uuid  = [self userDeiveID];
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager PUT:[NSString stringWithFormat:@"/users/%@/device",self.userAccount._id] parameters:@{@"uuid": uuid,@"token":token} success:^(NSURLSessionDataTask *task, id responseObject) {
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            failed(error,responseObject);;
        }];
    }];
}


-(void)loginWithInfo:(NSDictionary*)info succeed:(void(^)(UserAccount *account))succeed{
    
    NSMutableDictionary* para = [NSMutableDictionary dictionaryWithDictionary:info];
    
    [para setValue:[self userDeiveID] forKey:@"login_device_id"];

    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager POST:@"login" parameters:para success:^(NSURLSessionDataTask *task, id responseObject) {
           
            
            NSError *error;
            self.userAccount = [[UserAccount alloc] initWithDictionary:responseObject[@"user"] error:&error];
            
            [[[[NetworkManager sharedNetworkManager] JSONEncodeHttpRequestManager] requestSerializer] setValue:[NSString stringWithFormat:@"Bearer %@",responseObject[@"token"] ] forHTTPHeaderField:@"Authorization"];
            
            [[[[NetworkManager sharedNetworkManager] fileHttpRequestManager] requestSerializer] setValue:[NSString stringWithFormat:@"Bearer %@",responseObject[@"token"]] forHTTPHeaderField:@"Authorization"];
            
            
            DDLogVerbose(@"set headers %@",[NSString stringWithFormat:@"Bearer %@",responseObject[@"token"]]);
            
            
            if (error) {
                 succeed(nil);
                
                 failed(error,responseObject);;
            }else{
                 succeed(_userAccount);
                
                [self registerNotification];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:User_LoginNotification object:nil];
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            succeed(nil);
            NSMutableDictionary*info = [responseObject mutableCopy];
            if ([responseObject[@"message"] isEqualToString:@"LOGIN_NOT_EXIST"]) {
                [info setObject:@"登录帐号不存在" forKey:@"message"];
            }
            
            failed(error,info);
        }];
        
    }];

}

-(NSString*)userDeiveID{
  
    NSString* uuid  = [OpenUDID value];

    return uuid;
}
//The third party account login
//用第三方账号登录

-(void)loginWithThirdAccountID:(NSString*)accountID andThirdPartyType:(NSString*)type succeed:(void(^)(UserAccount *account))succeed{
    
     [self loginWithInfo:@{@"id":accountID,@"login_type":type} succeed:succeed];
   
}

//
//用第三方账号注册
-(void)registrationThirdPartyAccountUserInfo:(NSDictionary*)userInfo succeed:(void(^)(UserAccount *account,int code))succeed{
    

    NSMutableDictionary* para = [NSMutableDictionary dictionaryWithDictionary:userInfo];
    
    [para setValue:[self userDeiveID] forKey:@"login_device_id"];
    
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager POST:@"/registration_other" parameters:para success:^(NSURLSessionDataTask *task, id responseObject) {
            
            
            NSError *error;
            self.userAccount = [[UserAccount alloc] initWithDictionary:responseObject[@"user"] error:&error];
            
            [[[[NetworkManager sharedNetworkManager] JSONEncodeHttpRequestManager] requestSerializer] setValue:[NSString stringWithFormat:@"Bearer %@",responseObject[@"token"] ] forHTTPHeaderField:@"Authorization"];
            
            [[[[NetworkManager sharedNetworkManager] fileHttpRequestManager] requestSerializer] setValue:[NSString stringWithFormat:@"Bearer %@",responseObject[@"token"]] forHTTPHeaderField:@"Authorization"];
            
            
            if (error) {
                
                succeed(nil,-1);
                failed(error,responseObject);
                
            }else{
                
                succeed(_userAccount,1);
                
                [self registerNotification];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:User_LoginNotification object:nil];
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            int code = [responseObject[@"code"] intValue];
            
            succeed(nil,code);
            
            if (code!=kUserRegisterTryExistCode) {
                failed(error,responseObject);
            }

        }];
        
    }];
}

-(void)registerNotification{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings
                                                                                 settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                                                                                 categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }else{
        
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        
    }
}

-(void)getUserProfile:(NSString*)userId succeed:(void(^)(UserAccount*account))complete{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/users/%@",userId]parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            JSONModelError*error;
            
            UserAccount*account = [[UserAccount alloc] initWithDictionary:responseObject  error:&error];
            
            complete(account);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            complete(nil);
            failed(error,responseObject);;
            
        }];
    }];
    
}




@end
