//
//  TencentService.m
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "TencentService.h"
#import "Config.h"
#import "ImageDataConvertor.h"


@implementation TencentService{
    TencentServiceManager*_manager;
}
@synthesize appId;
@synthesize oauthToken;
@synthesize expireDate;
@synthesize userId;
@synthesize status;


-(TencentServiceManager*)manager{
    if (!_manager) {
        _manager =[[TencentServiceManager alloc] initWithService:self];
    }
    return _manager;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.appId = [aDecoder decodeObjectForKey:@"appId"];
        self.oauthToken = [aDecoder decodeObjectForKey:@"oauthToken"];
        self.expireDate = [aDecoder decodeObjectForKey:@"expireDate"];
        self.userId  = [aDecoder decodeObjectForKey:@"userId"];
        
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.appId forKey:@"appId"];
    [aCoder encodeObject:self.oauthToken forKey:@"oauthToken"];
    [aCoder encodeObject:self.expireDate forKey:@"expireDate"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
}
-(NSString*)archieveKey{
    return TencentSerivceArchive;
}
-(NSString*)serverAPIKey{
    return kTencentAPIKey;
}
@end


@implementation TencentServiceManager{
    TencentOAuth* _tencentOAuth;
    ShareCompletedBlock _shareBlock;
    AuthCompletedBlock _authBlock;
    
    GetProfileCompletedBlock _profileUpdateBlock;
}
- (instancetype)initWithService:(TencentService*)service
{
    self = [super init];
    if (self) {
        _service= service;
        
        _tencentOAuth = [[TencentOAuth alloc] initWithAppId:service.appId andDelegate:self];
    }
    return self;
}
-(void)logoutSerivice{
    [_tencentOAuth logout:self];
}

-(void)shareToQQWithTitle:(NSString*)title description:(NSString*)desc  thumbNail:(UIImage*)thumbnail andLink:(NSString*)link andFinish:(ShareCompletedBlock)action{
    _shareBlock = action;
    NSURL* url = [NSURL URLWithString:link];
    
    
    if (!thumbnail) {
        thumbnail = [UIImage imageNamed:@"AppIcon40x40@2x.png"];
    }else{
        thumbnail = [ImageDataConvertor thumbnmailData:UIImageJPEGRepresentation(thumbnail, 1) MIMEType:JPEGMIME];
    }
    
    
    QQApiNewsObject* img = [QQApiNewsObject objectWithURL:url title:title description:desc previewImageData:UIImagePNGRepresentation(thumbnail)];
    
    SendMessageToQQReq* req = [SendMessageToQQReq reqWithContent:img];
    
    QQApiSendResultCode sent = [QQApiInterface SendReqToQZone:req];
    
    [self handleSendResult:sent];
}
- (void)handleSendResult:(QQApiSendResultCode)sendResult
{
    if (sendResult == EQQAPISENDSUCESS)
    {   if(_shareBlock )
            _shareBlock(YES);
    }else{
        if(_shareBlock )
            _shareBlock(NO);
    }
    _shareBlock=nil;
    
    switch (sendResult)
    {
        case EQQAPIAPPNOTREGISTED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"App未注册", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"取消", nil) otherButtonTitles:nil];
            [msgbox show];

            
            break;
        }
        case EQQAPIMESSAGECONTENTINVALID:
        case EQQAPIMESSAGECONTENTNULL:
        case EQQAPIMESSAGETYPEINVALID:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"发送参数错误", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"取消", nil) otherButtonTitles:nil];
            [msgbox show];

            
            break;
        }
        case EQQAPIQQNOTINSTALLED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"未安装手机QQ", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"取消", nil) otherButtonTitles:nil];
            [msgbox show];

            
            break;
        }
        case EQQAPIQQNOTSUPPORTAPI:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"API接口不支持", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"取消", nil) otherButtonTitles:nil];
            [msgbox show];

            
            break;
        }
        case EQQAPISENDFAILD:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"发送失败", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"取消", nil) otherButtonTitles:nil];
            [msgbox show];

            break;
        }
        default:
        {
            break;
        }
    }
}


-(void)startTencentAuthComplete:(AuthCompletedBlock)complete{
 
    _authBlock= complete;
    
    NSArray*_permissions = [NSArray arrayWithObjects:
                            kOPEN_PERMISSION_GET_USER_INFO,
                            kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                            
                            kOPEN_PERMISSION_ADD_PIC_T,
                            kOPEN_PERMISSION_ADD_SHARE,
                            kOPEN_PERMISSION_ADD_TOPIC,
                            
                            kOPEN_PERMISSION_GET_INFO,
                            kOPEN_PERMISSION_GET_OTHER_INFO,
                            
                            nil] ;
    [_tencentOAuth authorize:_permissions];
}
-(void)getUserInfoComplete:(GetProfileCompletedBlock)complete{
    
    _profileUpdateBlock = complete;
    [_tencentOAuth getUserInfo];
}





/**
 * Called when the user successfully logged in.
 */
- (void)tencentDidLogin{
    
    if (_tencentOAuth.accessToken && 0 != [_tencentOAuth.accessToken length])
    {
        
        self.service.oauthToken =_tencentOAuth.accessToken ;
        self.service.userId = _tencentOAuth.openId;
        self.service.expireDate = _tencentOAuth.expirationDate;
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ServiceInfoChangedNotification object:self.service];
        
        NSDictionary*info = @{@"id":_tencentOAuth.openId,@"username":_tencentOAuth.openId,@"login_type":kTencentAPIKey};
        
        _authBlock(info);
        
    }
    else
    {
          _authBlock(nil);
    }
    _authBlock =nil;
}

/**
 * Called when the get_user_info has response.
 */
- (void)getUserInfoResponse:(APIResponse*) response{
    if (response.retCode == URLREQUEST_SUCCEED)
    {
        
        _profileUpdateBlock(response.jsonResponse);
    }else{
        
        _profileUpdateBlock(nil);
    }
    _profileUpdateBlock = nil;
}

- (void)addShareResponse:(APIResponse*) response{
  
    if (response.retCode == URLREQUEST_SUCCEED)
    {
        if(_shareBlock )
            _shareBlock(YES);
    }else{
        if(_shareBlock )
            _shareBlock(NO);
    }
    _shareBlock=nil;
}
- (void)tencentOAuth:(TencentOAuth *)tencentOAuth doCloseViewController:(UIViewController *)viewController
{
    DDLogVerbose(@"dissmiss tencent view");
    
    
}
-(void)tencentDidNotNetWork{
    _authBlock(nil);
    _authBlock = nil;
    DDLogVerbose(@"tencentDidNotNetWork");
}

-(void)tencentDidNotLogin:(BOOL)cancelled{
    _authBlock(nil);
    _authBlock = nil;
    DDLogVerbose(@"tencentDidNotLogin");
}
- (BOOL)tencentNeedPerformReAuth:(TencentOAuth *)tencentOAuth{
    return YES;
}
-(void)tencentDidLogout{
    DDLogVerbose(@"%s",__PRETTY_FUNCTION__);
}

#pragma mark qq api

/**
 处理来至QQ的请求
 */
- (void)onReq:(QQBaseReq *)req{
        DDLogVerbose(@"%@ \n%s",req,__PRETTY_FUNCTION__);
}

/**
 处理来至QQ的响应
 */
- (void)onResp:(QQBaseResp *)resp{
    if(resp.type==ESENDMESSAGETOQQRESPTYPE){
    //share
        if (!resp.errorDescription) {
            if (_shareBlock) {
                _shareBlock(YES);
            }
        }else{
            if (_shareBlock)
                _shareBlock(NO);
        }
    
    }
    DDLogVerbose(@"%@ \n %s",resp,__PRETTY_FUNCTION__);
}

/**
 处理QQ在线状态的回调
 */
- (void)isOnlineResponse:(NSDictionary *)response{
        DDLogVerbose(@"%s",__PRETTY_FUNCTION__);
}
@end