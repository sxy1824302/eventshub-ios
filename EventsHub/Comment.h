//
//  Comment.h
//  EventsHub
//
//  Created by alexander on 4/14/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "JSONModel.h"
#import "SimpleUserModel.h"


@interface Comment : JSONModel
@property (nonatomic)NSString*parent_id;
@property (nonatomic)NSString*event_ref;
@property (nonatomic)SimpleUserModel*user;
@property (nonatomic)NSString*comment_text;
@property (nonatomic)NSString*comment_date;


-(NSString*)formatedCommentDate;
@end
