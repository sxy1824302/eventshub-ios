//
//  Event+Comments.h
//  EventsHub
//
//  Created by alexander on 4/10/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event.h"
#import "Comment.h"
@interface Event (Comments)

-(void)getLimitedComments:(NSInteger)count complete:(void(^)(NSArray* comments))succeedBlock;


-(void)comment:(NSString*)comment complete:(void(^)(bool succeed))succeedBlock;
@end
