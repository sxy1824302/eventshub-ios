//
//  HintView.m
//  EventsHub
//
//  Created by alexander on 1/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "HintView.h"
#import <FAKFontAwesome.h>
#import "UIHelper.h"
@implementation HintView

-(void)awakeFromNib{
    
    
    FAKFontAwesome*cross = [FAKFontAwesome timesIconWithSize:kMinimalFontSize];
    
    [self.cancelButton setAttributedTitle:cross.attributedString forState:UIControlStateNormal];
    
    UITapGestureRecognizer*tpg =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(remove:)];
    
    
    [self addGestureRecognizer:tpg];
}


- (IBAction)remove:(id)sender {
    
    [self removeFromSuperview];
}

@end
