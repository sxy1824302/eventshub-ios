//
//  EHDropDownBar.m
//  EventsHub
//
//  Created by alexander on 1/6/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "EHDropDownBar.h"
#import "UIHelper.h"
@implementation EHDropDownBar{
    
    NSMutableArray*_buttons;
    
    UIButton*_lastButton;

    UIView *_underline;
    
    UIScrollView *_scrollView;
}
-(void)awakeFromNib{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, [UIHelper screenWidth], GuidlineMetric(98));
    
    [self initializeCustomDrawingProperties];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    
    frame = CGRectMake(frame.origin.x, frame.origin.y, [UIHelper screenWidth], GuidlineMetric(98));
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeCustomDrawingProperties];
        
    }
    return self;
}
-(void)prepareForInterfaceBuilder{
    
    [self initializeCustomDrawingProperties];
    
    
    [self applyNewTitles:@[@"今天",@"昨天",@"后天"]];
}
-(void)initializeCustomDrawingProperties{
    
    self.backgroundColor = [UIHelper whiteBackgroundColor];
    
    self.fontSize = GuidlineMetric(32);
    
    _buttons = [NSMutableArray array];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.showsHorizontalScrollIndicator = NO;
    
    [self addSubview:_scrollView];
}

-(void)applyNewTitles:(NSArray *)titles{
    [self needReloadLayoutWithTitles:titles];
}

-(void)selectDefault{

    [self clicked:[_buttons firstObject]];
}
-(void)needReloadLayoutWithTitles:(NSArray*)titles{
    
    
    _titles = titles;
    

    [_buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    
    CGFloat buttonWidth = self.frame.size.width/titles.count;
    
   
    for (int i = 0; i<_titles.count; i++) {
        
        UIButton*button1 = [UIButton buttonWithType:UIButtonTypeSystem];
        
        button1.translatesAutoresizingMaskIntoConstraints = NO;
        
        [button1 setAttributedTitle:[[NSAttributedString alloc] initWithString:titles[i] attributes:@{
                                                                                                      NSFontAttributeName:[UIFont systemFontOfSize:self.fontSize],
                                                                                                      NSForegroundColorAttributeName:[UIHelper blueThemeColor],
                                                                                                      }] forState:UIControlStateNormal];
        button1.tag = i;
        
        [button1 addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [_scrollView addSubview:button1];
        
        [_buttons addObject:button1];
        
        [button1 sizeToFit];
        
        if (button1.frame.size.width>buttonWidth) {
            buttonWidth = button1.frame.size.width;
        }
    }
    
    
    CGSize size = self.frame.size;
    size.width = buttonWidth*_titles.count;
    _scrollView.contentSize = size;
    
    
    [_buttons enumerateObjectsUsingBlock:^(id button1, NSUInteger idx, BOOL *stop) {
        
        [_scrollView addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [_scrollView addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
        
        
        [_scrollView addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeLeading multiplier:1 constant:idx*buttonWidth]];
        [_scrollView addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeLeading multiplier:1 constant:(1+idx)*buttonWidth]];
    }];
    
}

-(void)layoutSubviews{
    [super layoutSubviews];
    if (_lastButton) {
        NSAttributedString*title =[_lastButton attributedTitleForState:UIControlStateNormal];
        
        CGSize size = [title size];
        _underline.center = CGPointMake(_lastButton.center.x, _lastButton.center.y+13);
        _underline.bounds = CGRectMake(0, 0, size.width, 1);
    }
}

-(void)clicked:(UIButton*)btn{
    
    if(!_underline){
        _underline = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)];
        _underline.backgroundColor = [UIHelper blueThemeColor];
        [_scrollView addSubview:_underline];
    }
    
    NSAttributedString*title =[btn attributedTitleForState:UIControlStateNormal];
    
   CGSize size = [title size];
   
    [UIView animateWithDuration:0.1 delay:0 usingSpringWithDamping:0 initialSpringVelocity:0 options:0 animations:^{
        _underline.center = CGPointMake(btn.center.x, btn.center.y+13);
        _underline.bounds = CGRectMake(0, 0, size.width, 1);
    } completion:^(BOOL finished) {
        
    }];
    
    _lastButton = btn;
    
    [self.delegate menuDidClickedButtonAtIndex:btn.tag withTitle:_titles[btn.tag]];
    
    
}


@end
