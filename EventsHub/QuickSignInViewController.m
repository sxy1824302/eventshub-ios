//
//  QuickSignInViewController.m
//  EventsHub
//
//  Created by alexander on 1/8/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "QuickSignInViewController.h"
#import "AccountManager.h"
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import <UIViewController+MaryPopin.h>
#import "MBProgressHUD.h"
#import "SocialServices.h"
#import "Config.h"
@interface QuickSignInViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *littleTittleLabel;
@end

@implementation QuickSignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = NSLocalizedString(@"需要登录才能继续哦", nil);
    self.littleTittleLabel.text = NSLocalizedString(@"快速注册登录", nil);
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 160);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dissmiss:) name:User_LoginNotification object:nil];
    
}

-(void)hideHUD:(id)noti{
    [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
}

-(void)dissmiss:(id)noti{
    [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.parentViewController dismissCurrentPopinControllerAnimated:NO];
}


- (IBAction)qqSign:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    [[SocialServices sharedSocialServices] tryRegisternWithService:ServiceTypeTencent fromViewController:self complete:^(bool succeed) {
        [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
    }];
}
- (IBAction)weiboSign:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    [[SocialServices sharedSocialServices] tryRegisternWithService:ServiceTypeWeibo fromViewController:self  complete:^(bool succeed) {
        [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
    }];
}

- (IBAction)weiXinSign:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];

    [[SocialServices sharedSocialServices] tryRegisternWithService:ServiceTypeWechat fromViewController:self  complete:^(bool succeed) {
        [MBProgressHUD hideHUDForView:self.parentViewController.view animated:YES];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


@implementation UIViewController (Auth)

-(void)showInAppAuthView{
    

    UIViewController *popin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"QuckSignVC"];
    
    [popin setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
    
    
    [popin setPopinTransitionDirection:BKTPopinTransitionDirectionBottom];
    
    [popin setPopinAlignment:BKTPopinAlignementOptionDown];
    
    [self presentPopinController:popin animated:YES completion:^{
        
    }];
}

@end