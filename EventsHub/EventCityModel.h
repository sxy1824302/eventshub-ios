//
//  EventCityModel.h
//  EventsHub
//
//  Created by alexander on 2/13/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "JSONModel.h"

@interface EventCityModel : JSONModel

@property (nonatomic)NSString*city_name;

@property (nonatomic)NSString*name_hash;

@end
