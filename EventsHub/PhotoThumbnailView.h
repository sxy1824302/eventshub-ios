//
//  PhotoThumbnailView.h
//  EventsHub
//
//  Created by 孙翔宇 on 2/26/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoThumbnailView : UIControl
@property (nonatomic)UIImageView*thumbnailImageView;
-(void)loadImageURL:(NSString*)urlString;

-(void)emptyLoading;
@end
