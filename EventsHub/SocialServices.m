//
//  SocialServices.m
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "SocialServices.h"
#import "AccountManager.h"
#import "Config.h"
#import "NetworkManager.h"
#import "NSDate+ISO8601.h"
#import "NSDate+Helpers.h"
@implementation SocialServices{
    
}

+ (id)sharedSocialServices
{
    static dispatch_once_t onceQueue;
    static SocialServices *socialServices = nil;
    
    dispatch_once(&onceQueue, ^{ socialServices = [[self alloc] init]; });
    return socialServices;
}

-(id)serviceOfType:(ServiceType)type{
    return _services[type];
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
        [self initServices];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:ServiceInfoChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
            
            NSData*data = [NSKeyedArchiver archivedDataWithRootObject:note.object];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:[note.object archieveKey]];
            
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:User_LogoutNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:TencentSerivceArchive];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:WeiboSerivceArchive];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:WechatSerivceArchive];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self initServices];
            
        }];
        
    }
    return self;
}

-(void)initServices{
    NSMutableArray*s = [NSMutableArray array];
    NSData*wed = [[NSUserDefaults standardUserDefaults] objectForKey:WechatSerivceArchive];
    if (wed) {
        WechatService*wes =[NSKeyedUnarchiver unarchiveObjectWithData:wed];
        [s addObject:wes];
    }else{
        WechatService*wes =[WechatService new];
        
        wes.appId = kWechatAppId;
        
        [s addObject:wes];
    }
    
    
    
    NSData*wd = [[NSUserDefaults standardUserDefaults] objectForKey:WeiboSerivceArchive];
    if (wd) {
        WeiboService*ws =[NSKeyedUnarchiver unarchiveObjectWithData:wd];
        [s addObject:ws];
    }else{
        WeiboService*ws =[WeiboService new];
        
        ws.appId = kWeiboAppKey;
        
        [s addObject:ws];
    }
    
    
    NSData*td = [[NSUserDefaults standardUserDefaults] objectForKey:TencentSerivceArchive];
    if (td) {
        TencentService*ts =[NSKeyedUnarchiver unarchiveObjectWithData:td];
        [s addObject:ts];
    }else{
        TencentService*ts =[TencentService new];
        ts.appId = kTencentAppID;
        [s addObject:ts];
    }
    
    
    
    [s enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [obj performSelectorOnMainThread:@selector(manager) withObject:nil waitUntilDone:YES];
        
    }];
    
    _services = s;

}



-(NSArray*)shareTitle{
    
//    @[NSLocalizedString(@"分享到微博", nil),NSLocalizedString(@"分享到QQ空间",nil),NSLocalizedString(@"分享到微信朋友圈",nil)]

    NSMutableArray* title = [NSMutableArray array];
    [title addObject:([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"sinaweibo://"]]?NSLocalizedString(@"分享到微博", nil):NSLocalizedString(@"分享到微博(未安装)", nil))];

    [title addObject:NSLocalizedString(@"分享到QQ空间",nil)];
    [title addObject:[[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]?NSLocalizedString(@"分享到微信朋友圈",nil):NSLocalizedString(@"分享到微信朋友圈(未安装)",nil)];
    [title addObject:NSLocalizedString(@"复制活动链接",nil)];
    return title;
}

-(void)archieveAllService{
    
    
    [_services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
       
        NSData*data = [NSKeyedArchiver archivedDataWithRootObject:obj];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:[obj archieveKey]];
        
    }];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
-(void)archieveServiceForType:(ServiceType)type{
    id obj = _services[type];
    
    NSData*data = [NSKeyedArchiver archivedDataWithRootObject:obj];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:[obj archieveKey]];

    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSDictionary*)getAutoSocialSignedIn{
    
    
    __block NSString*userid ;
    
     __block NSString*type ;
    
    [_services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        userid= [obj userId];
        
        if (userid) {
            type = [obj serverAPIKey];
            
            *stop = YES;
        }
        
    }];
    
    if (userid) {
        return @{@"id":userid,@"login_type":type};
    }
    
    return nil;
}


-(void)signInWithServiceInfo:(NSDictionary*)info complete:(void(^)(bool succeed))complete{
    
    [[AccountManager sharedAccountManager] loginWithThirdAccountID:info[@"id"] andThirdPartyType:info[@"login_type"] succeed:^(UserAccount *account){
        if (!account) {
            complete(NO);
        }else{
            complete(YES);
        }
    }];
    
}

-(void)tryRegisternWithService:(ServiceType)type fromViewController:(UIViewController*)vc complete:(void(^)(bool succeed))complete{
    
    id<SocialService> service = _services[type];
    
    
    
    switch (type) {
        case ServiceTypeTencent:{
            TencentService*sv = (TencentService*)service;
            
            [sv.manager startTencentAuthComplete:^(NSDictionary *authInfo) {
                
                [self performRegisterWithService:type complete:complete];
            }];
        }
            break;
        case ServiceTypeWechat:{
            WechatService*sv = (WechatService*)service;
            
            [sv.manager startAuthInViewController:vc  Complete:^(NSDictionary *authInfo) {
                  [self performRegisterWithService:type complete:complete];
            }];
        }
            
            break;
        case ServiceTypeWeibo:{
            WeiboService*sv = (WeiboService*)service;
            
            [sv.manager startAuthComplete:^(NSDictionary *authInfo) {
                  [self performRegisterWithService:type complete:complete];
            }];
        }
            
            break;
        default:
            break;
    }
}

-(void)performRegisterWithService:(ServiceType)type  complete:(void(^)(bool succeed))complete{
    
    id<SocialService> service = _services[type];
    
    if ([[service userId] length]>0) {
        NSDictionary*info = @{@"id":[service userId],@"login_type":[service serverAPIKey]};
        
        [[AccountManager sharedAccountManager] registrationThirdPartyAccountUserInfo:info succeed:^(UserAccount *account,int code) {
            
            if (code==kUserRegisterTryExistCode) {
                DDLogVerbose(@"user alredy resgiterd, now login");
                [[AccountManager sharedAccountManager] loginWithThirdAccountID:[service userId] andThirdPartyType:[service serverAPIKey] succeed:^(UserAccount *account){
                    
                    if(account){
                        [self checkAccountProfileAutoUpload:account withServiceType:type];
                        complete(YES);
                        
                    }else{
                        
                        complete(NO);
                    }
                }];
                
            }else if (code>0){
                [self checkAccountProfileAutoUpload:account withServiceType:type];
                complete(YES);
                
            }else{
                complete(NO);
            }
        }];
    }else{
        complete(NO);
    }

}

-(void)checkAccountProfileAutoUpload:(UserAccount*)account withServiceType:(ServiceType)type{
    
    id<SocialService> service = _services[type];
    
    if (account.avatar_path.length<1) {
        
        switch (type) {
            case ServiceTypeTencent:{
                
                /*
                 {
                 city = "\U6210\U90fd";
                 figureurl = "http://qzapp.qlogo.cn/qzapp/1103396799/9F5C0DDE42222D64FD66FFC4C6FAA540/30";
                 "figureurl_1" = "http://qzapp.qlogo.cn/qzapp/1103396799/9F5C0DDE42222D64FD66FFC4C6FAA540/50";
                 "figureurl_2" = "http://qzapp.qlogo.cn/qzapp/1103396799/9F5C0DDE42222D64FD66FFC4C6FAA540/100";
                 "figureurl_qq_1" = "http://q.qlogo.cn/qqapp/1103396799/9F5C0DDE42222D64FD66FFC4C6FAA540/40";
                 "figureurl_qq_2" = "http://q.qlogo.cn/qqapp/1103396799/9F5C0DDE42222D64FD66FFC4C6FAA540/100";
                 gender = "\U7537";
                 "is_lost" = 0;
                 "is_yellow_vip" = 0;
                 "is_yellow_year_vip" = 0;
                 level = 0;
                 msg = "";
                 nickname = "\U00b7\U3002\U00b7";
                 province = "\U56db\U5ddd";
                 ret = 0;
                 vip = 0;
                 "yellow_vip_level" = 0;
                 }
                
                */
                TencentService*sv = (TencentService*)service;
                
                [sv.manager getUserInfoComplete:^(NSDictionary *profileDict) {
                   
                    NSString*iconURL = profileDict[@"figureurl_2"];
                    
                    NSNumber*year =  profileDict[@"year"];
                    
                    NSString *gender= profileDict[@"gender"];
                    
                    if ([gender isEqualToString:@"男"]) {
                        gender = @"M";
                    }else if ([gender isEqualToString:@"女"]){
                        gender = @"F";
                    }else{
                        gender = @"U";
                    }
                    
                    NSString *nickName = profileDict[@"nickname"];
                    
                    
                    
                    [[NetworkManager sharedNetworkManager] downloadImage:iconURL image:^(UIImage *image) {
                        if (image) {
                            
                            [[AccountManager sharedAccountManager] updateUserAvatar:UIImagePNGRepresentation(image) complete:NULL];
                            
                        }
                    }];
                    
                    NSDate*bday = [NSDate dateFromYear:year.integerValue] ;
                    
                    if ([bday timeIntervalSince1970]<0) {
                        bday = nil;
                    }
                    
                    NSMutableDictionary*info = @{@"username":nickName,@"gender":gender}.mutableCopy;
                    if (bday) {
                        [info setObject:[bday ISOString] forKey:@"birthday"];
                    }
                    [[AccountManager sharedAccountManager] updateUserInfo:info complete:^(bool succeed) {
                        
                        if (succeed) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:User_InfoUpdatedNotification object:nil];
                        }
                    }];

                    
                }];
            }
                break;
            case ServiceTypeWechat:{
                WechatService*sv = (WechatService*)service;
                
                [sv.manager getUserInfoComplete:^(NSDictionary *profileDict) {
                    
                    JSONModelError*error;
                    WechatUserInfo*user = [[WechatUserInfo alloc] initWithDictionary:profileDict error:&error];
                    
                    
                    
                    [[NetworkManager sharedNetworkManager] downloadImage:user.headimgurl image:^(UIImage *image) {
                        if (image) {
                            
                            [[AccountManager sharedAccountManager] updateUserAvatar:UIImagePNGRepresentation(image) complete:NULL];
                            
                        }
                    }];
                    
                    NSString*gender ;
                    
                    if ([user.sex isEqualToNumber:@1]) {
                        gender = @"M";
                    }else if ([user.sex isEqualToNumber:@2]){
                        gender = @"F";
                    }else{
                        gender = @"U";
                    }
                    
                    NSDictionary*info = @{@"username":user.nickname,@"gender":gender,@"location_simple":[NSString stringWithFormat:@"%@,%@,%@",user.country,user.province,user.city]};
                    
                    [[AccountManager sharedAccountManager] updateUserInfo:info complete:^(bool succeed) {
                        
                        if (succeed) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:User_InfoUpdatedNotification object:nil];
                        }
                    }];
                    

                }];
            }
                
                break;
            case ServiceTypeWeibo:{
                WeiboService*sv = (WeiboService*)service;
                
                [sv.manager getUserInfoComplete:^(NSDictionary *profileDict) {
                    
                    
                    NSString*name =  profileDict[@"screen_name"];
                    NSString*gender =  profileDict[@"gender"];
                    NSString*profileURL =  profileDict[@"profile_image_url"];
                    NSString*location =  profileDict[@"location"];
             
                    
                    [[NetworkManager sharedNetworkManager] downloadImage:profileURL image:^(UIImage *image) {
                        if (image) {
                            
                            [[AccountManager sharedAccountManager] updateUserAvatar:UIImagePNGRepresentation(image) complete:NULL];
                            
                        }
                    }];
                    
                    NSDictionary*info = @{@"username":name,@"gender":gender.capitalizedString,@"location_simple":location};
                    
                    [[AccountManager sharedAccountManager] updateUserInfo:info complete:^(bool succeed) {
                        
                        if (succeed) {
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:User_InfoUpdatedNotification object:nil];
                            
                        }
                    }];

                    
                    
                }];
            }
                
                break;
            default:
                break;
        }

    }
    
}


@end
