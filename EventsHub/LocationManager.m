//
//  LocationManager.m
//  EventsHub
//
//  Created by alexander on 1/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "LocationManager.h"
#import "Config.h"
#import "AccountManager.h"
#import "Address.h"
#import "NetworkManager.h"

NSString*const UserLocationUpdatedNotification = @"UserLocationUpdatedNotification";
NSString*const UserLocationCityUpdatedNotification = @"UserLocationCityUpdatedNotification";

NSString*const EventCityUpdatedNotification = @"EventCityUpdatedNotification";


@interface LocationManager ()
@property (nonatomic)Address *currentAdress;
@end


@implementation LocationManager{
    BMKLocationService* _locService;
    BMKGeoCodeSearch *_geocodesearch;
    
    CLLocationManager*_locationManager;
}
@synthesize eventCity=_eventCity;

+ (id)sharedLocationManager
{
    static dispatch_once_t onceQueue;
    static LocationManager *locationManager = nil;
    
    dispatch_once(&onceQueue, ^{ locationManager = [[self alloc] init]; });
    return locationManager;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
        [self setupLocationService];
        
    }
    return self;
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    DDLogVerbose(@"changed auth status %i",status);
    if (status>2) {
        [self reloadLocateUser];
    }
}
-(void)setEventCity:(NSString *)eventCity{
    
    _eventCity = eventCity;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:EventCityUpdatedNotification object:nil];
    
}

-(NSString*)eventCity{
    if (!_eventCity) {
        _eventCity = [[NSUserDefaults standardUserDefaults] objectForKey:kCachedUserCity];
    }
    return _eventCity;
}

+(void)getEventsCities:(void(^)(NSArray* tags))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:@"/cities" parameters:@{@"locale":CURRENT_LOCALE_IDENTIFIER} success:^(NSURLSessionTask *task, id responseObject) {
            
            NSArray*cities = [EventCityModel arrayOfModelsFromDictionaries:responseObject[@"cities"] error:nil];
            
            completeBlock( cities);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            completeBlock(nil);
        }];
    }];
}



-(void)reloadLocateUser{
    [_locService startUserLocationService];
    
}
-(void)setupLocationService{

    [BMKLocationService setLocationDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
 
    [BMKLocationService setLocationDistanceFilter:100.f];
 
    _locService = [[BMKLocationService alloc]init];
    _locService.delegate = self;
    
    _geocodesearch = [[BMKGeoCodeSearch alloc]init];
    _geocodesearch.delegate = self;
    
    
    [self reloadLocateUser];
}
- (void)willStartLocatingUser{
    DDLogVerbose(@"%s",__func__);
}
/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    DDLogVerbose(@"用户地址：lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);

    [[NSNotificationCenter defaultCenter] postNotificationName:UserLocationUpdatedNotification object:userLocation];
    self.currentUserLocation = userLocation.location;
    
    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    reverseGeocodeSearchOption.reverseGeoPoint = userLocation.location.coordinate;
    
    BOOL flag = [_geocodesearch reverseGeoCode:reverseGeocodeSearchOption];
    
    if(flag)
    {
        DDLogVerbose(@"反geo检索发送成功");
    }
    else
    {
        DDLogVerbose(@"反geo检索发送失败");
    }
    
    [_locService stopUserLocationService];
    
}
-(void)didFailToLocateUserWithError:(NSError *)error{
     DDLogVerbose(@"LOCATE USER FAILED %@",error.localizedDescription);
}




-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error
{
    
    if (error == 0) {
        _currentAdress = [Address new];
        _currentAdress.city = result.addressDetail.city;
        _currentAdress.district = result.addressDetail.district;
        _currentAdress.street1 = result.addressDetail.streetName;
        _currentAdress.street2 =  result.addressDetail.streetNumber;
        _currentAdress.province =  result.addressDetail.province;
        _currentAdress.geopoint = [Location geoPointWithCoordicates:result.location];
        
        
        if (!self.eventCity) {
            self.eventCity = _currentAdress.city;
        }
      
        [[NSNotificationCenter defaultCenter] postNotificationName:UserLocationCityUpdatedNotification object:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:result.addressDetail.city forKey:kCachedUserCity];
        
        
        
    }
}
@end
