//
//  EHTagStick.h
//  EventsHub
//
//  Created by apple on 15/1/4.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Colours.h>
#import "Tag.h"
#import "UIHelper.h"

#define kDefaultTagHeight    GuidlineMetric(80)
#define kDefaultTagMinWidth    GuidlineMetric(100)

#define kDefaultTagFontSize GuidlineMetric(32)


typedef void(^TagSelectedCallBack)(NSDictionary* tagDic);
typedef void(^CancelCallBack)();

@interface EventTagView : UIView

@property(nonatomic)UILabel* tagNameLabel;
@property (nonatomic,weak)Tag *eventTag;

-(instancetype)initWithTag:(Tag*)tag;

-(void)setEventTag:(Tag *)eventTag andCallBack:(TagSelectedCallBack)action;

-(void)tagStickWithCancel:(CancelCallBack)action;

+(NSAttributedString*)attributeName:(NSString*)name;
-(void)showCancel;
@end
