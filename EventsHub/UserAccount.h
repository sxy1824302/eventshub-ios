//
//  UserAccountManager.h
//  EventsHub
//
//  Created by 亮 姚 on 14-11-28.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "JSONModel.h"

@interface UserAccount : JSONModel

@property (nonatomic)NSString* _id;

@property (nonatomic)NSString* avatar_path;
@property (nonatomic)NSString* email;
@property (nonatomic)NSString* phone;
@property (nonatomic)NSString* gender;
@property (nonatomic)NSString* location_simple;

@property (nonatomic)NSString* in_app_id;
@property (nonatomic)NSString* username;
@property (nonatomic)NSString* birthday;
@property (nonatomic)NSNumber* activated;
@property(nonatomic)NSNumber*created_events_count;
@property (nonatomic)NSString*accountType;

@end
