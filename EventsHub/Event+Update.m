//
//  Event+Update.m
//  EventsHub
//
//  Created by alexander on 2/15/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event+Update.h"
#import "NetworkManager.h"



@implementation Event (Update)
-(void)updateSucceed:(void(^)(BOOL succeed))succeedBlock{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/events/%@",self._id] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            
            Event*event = [[Event alloc] initWithDictionary:responseObject error:nil];
            
            
            self.title = event.title;
            self.eventDescription = event.eventDescription;
            self.pio = event.pio;
            self.contact_phone = event.contact_phone;
            self.fee_description = event.fee_description;
            self.require_participants_phone = event.require_participants_phone;
            self.total_participants_limit = event.total_participants_limit;
            self.date_start = event.date_start;
            self.address = event.address;
            self.date_end = event.date_end;
            self.owner_name = event.owner_name;
            self.tags = event.tags;
            self.config = event.config;
            
            
            
            self.like_count = event.like_count;
            self.posters = event.posters;
            self.participant_count = event.participant_count;

 
            succeedBlock(YES);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);;
            
            succeedBlock(NO);
        }];
    }];
}

-(void)updateEventWithEventDic:(NSDictionary*)eventDic andPosters:(NSArray*)posters succeed:(void(^)(BOOL succeed,NSString* idString))succeedBlock{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager PUT:[NSString stringWithFormat:@"/events/%@",self._id] parameters:eventDic success:^(NSURLSessionDataTask *task, id responseObject) {
            
            
            succeedBlock(YES,nil);
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);;
            succeedBlock(NO,nil);
        }];
    }];
}



@end
