//
//  Location.h
//  EventsHub
//
//  Created by alexander on 12/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "JSONModel.h"
#import <CoreLocation/CoreLocation.h>
@interface Location : JSONModel
@property (nonatomic)NSNumber *latitude;
@property (nonatomic)NSNumber* longitude;


+(instancetype)geoPointWithCoordicates:(CLLocationCoordinate2D)coord;

@end
