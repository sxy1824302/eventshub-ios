//
//  Config.h
//  EventsHub
//
//  Created by alexander on 11/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#ifndef EventsHub_Config_h
#define EventsHub_Config_h




#define APPStore 1



#import <CocoaLumberjack/CocoaLumberjack.h>

#import "EventsHubSystem.h"


#ifdef DEBUG
static  int ddLogLevel = DDLogLevelVerbose;
#else
static  int ddLogLevel = DDLogLevelWarning;
#endif



#if SIMULATOR

#if 0
#define HostAPI @"http://127.0.0.1:5000"
#else
#define HostAPI @"http://test.events-hub.ijubar.cn"
#endif


#elif APPStore

#define HostAPI @"http://events-hub.ijubar.cn"

#else

#define HostAPI @"http://test.events-hub.ijubar.cn"

#endif


























#endif
