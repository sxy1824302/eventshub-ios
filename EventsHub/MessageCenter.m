//
//  MessageCenter.m
//  EventsHub
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "MessageCenter.h"
#import <TSMessage.h>
#import "AppDelegate.h"
#import "RESideMenu.h"
#import "AppDelegate.h"
@implementation MessageCenter
+(UIViewController*)controller{
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    UIViewController*topVc = [MessageCenter findTopViewController:delegate.window.rootViewController];
    
    return topVc;
}
+(UIViewController*)findTopViewController:(UIViewController*)vc{
    
    UIViewController*topVc;
    
    if ([vc isKindOfClass:[UINavigationController class]]) {
        
        topVc =  [(UINavigationController*)vc visibleViewController];
        
    }
    else if ([vc isKindOfClass:[UITabBarController class]]){
        
        topVc =  [(UITabBarController*)vc selectedViewController];
        
    }
    
    if (topVc) {
        return [[self class] findTopViewController:topVc];
    }
    
    return vc;
}


+(void)showMessage:(NSString*)message withType:(MessageType)type inViewController:(UIViewController*)vc{
    if (vc == nil) {
        vc = [MessageCenter controller];
    }
    
    [TSMessage showNotificationInViewController:vc title:nil subtitle:message type:(TSMessageNotificationType)type];
}
@end
