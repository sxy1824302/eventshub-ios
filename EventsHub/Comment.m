//
//  Comment.m
//  EventsHub
//
//  Created by alexander on 4/14/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Comment.h"
#import "NSDate+ISO8601.h"
static NSDateFormatter*dateFormatter;

@implementation Comment

+ (void)initialize
{
    if (self == [Comment class]) {
        
        dateFormatter = [NSDateFormatter new];
        
        dateFormatter.dateStyle = NSDateFormatterShortStyle;
        
        dateFormatter.timeStyle = NSDateFormatterShortStyle;
    }
}

-(NSString*)formatedCommentDate{
    return [dateFormatter stringFromDate:[NSDate dateFromISOString:self.comment_date]];
}

+(BOOL)propertyIsOptional:(NSString *)propertyName{
    return YES;
}
@end
