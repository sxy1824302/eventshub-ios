//
//  AppDelegate.m
//  EventsHub
//
//  Created by alexander on 11/6/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "AppDelegate.h"
#import "MobClick.h"
#import "NetworkManager.h"
#import "MessageCenter.h"
#import "CompressingLogFileManager.h"
#import "BMKMapManager.h"
#import "AccountManager.h"
#import "LocationManager.h"
#import "UIHelper.h"
#import "WeiboService.h"
#import "WechatService.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "SocialServices.h"
#import "EventsHubSystem.h"
#import "IQKeyboardManager.h"
#import "CommentsMessageViewController.h"
#define UMENG_APPKEY @"5459de58fd98c5a9e3004dfd"




@interface AppDelegate ()<BMKGeneralDelegate>{
    BMKMapManager* _mapManager;
}

@end

@implementation AppDelegate

- (void)umengTrack {
    [MobClick setCrashReportEnabled:YES];
    
#ifdef DEBUG
    [MobClick setLogEnabled:YES];

    [MobClick startWithAppkey:UMENG_APPKEY reportPolicy:(ReportPolicy) REALTIME channelId:@"Internal Test"];
#else
    [MobClick startWithAppkey:UMENG_APPKEY reportPolicy:(ReportPolicy) BATCH channelId:"Release"];
#endif
    
    [MobClick setAppVersion:XcodeAppVersion];
    

    [MobClick checkUpdate];

    [MobClick updateOnlineConfig];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineConfigCallBack:) name:UMOnlineConfigDidFinishedNotification object:nil];
    
}

- (void)onlineConfigCallBack:(NSNotification *)note {
    DDLogVerbose(@"online config has fininshed and note = %@", note.userInfo);
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [[EventsHubSystem sharedEventsHubSystem] intializeSystem];
    
    
    [[IQKeyboardManager sharedManager] disableInViewControllerClass:[CommentsMessageViewController class]];
    
    [[IQKeyboardManager sharedManager] disableToolbarInViewControllerClass:[CommentsMessageViewController class]];
    
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIHelper blueThemeColor],NSForegroundColorAttributeName,
                                               [UIHelper defaultFontWithSize:16],NSFontAttributeName,
                                               nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    [[UINavigationBar appearance] setTintColor:[UIHelper blueThemeColor]];
    
    [self umengTrack];
    
    
    _mapManager = [[BMKMapManager alloc]init];
    
    BOOL ret = [_mapManager start:@"VmlyK7XgKI78mBtwHom8ZPMi" generalDelegate:self];
    
    if (!ret) {
        DDLogVerbose(@"BMKMapManager manager start failed!");
    }else{
        DDLogVerbose(@"BMKMapManager manager start!");
    }
    
    

    return YES;
}



- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
   
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSString *error_str = [NSString stringWithFormat: @"%@", error];
    DDLogVerbose(@"Failed to get token, error:%@", error_str);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [MessageCenter showMessage:userInfo[@"aps"][@"alert"] withType:MessageTypeSuccess inViewController:nil];
    DDLogVerbose(@"Receive remote notification : %@",userInfo);
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    
    
    NSMutableString *deviceID = [NSMutableString string];
    
    unsigned char *ptr = (unsigned char *)[deviceToken bytes];
    
    for (NSInteger i=0; i < 32; ++i) {
        
        [deviceID appendString:[NSString stringWithFormat:@"%02x", ptr[i]]];
    }
    
    DDLogInfo(@"recieve device token %@", deviceID);
    
    [[AccountManager sharedAccountManager] updateDeviceToken:deviceID];
}

- (void)applicationWillResignActive:(UIApplication *)application {
     [BMKMapView willBackGround];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
   
    [BMKMapView didForeGround];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}




- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    TencentServiceManager*tmanager = [(TencentService*)[[SocialServices sharedSocialServices] serviceOfType:ServiceTypeTencent] manager];
    
    WeiboServiceManager*wmanager = [[[SocialServices sharedSocialServices] serviceOfType:ServiceTypeWeibo] manager];
    
    WechatServiceManager*wemanager = [(WechatService*)[[SocialServices sharedSocialServices] serviceOfType:ServiceTypeWechat] manager];
    
    if ([QQApiInterface handleOpenURL:url delegate:tmanager]){
        return YES;
    }
    else if ([WeiboSDK handleOpenURL:url delegate:wmanager]){
        return  YES;
    }else if ([WXApi handleOpenURL:url delegate:wemanager]) {
        return YES;
    }
    else if ( [TencentOAuth HandleOpenURL:url]) {
        return YES;
    }
    return NO;
}



#pragma mark  baidu

- (void)onGetNetworkState:(int)iError
{
    if (0 == iError) {
        DDLogVerbose(@"BAIDU MAP 联网成功");
    }
    
    else{
        DDLogVerbose(@"BAIDU MAP onGetNetworkState %d",iError);
    }
    
}

- (void)onGetPermissionState:(int)iError
{
    if (0 == iError) {
        DDLogVerbose(@"BAIDU MAP 授权成功");
//BMKPermissionCheckResultCode
    }
    else {
        DDLogVerbose(@"BAIDU MAP onGetPermissionState %d",iError);
    }
}
@end
