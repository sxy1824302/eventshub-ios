//
//  PhotosCacheManager.m
//  EventsHub
//
//  Created by alexander on 1/30/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "PhotosCacheManager.h"

@implementation PhotosCacheManager{
    NSMutableArray*_mwImages;
    
    
}
+ (id)sharedPhotosCacheManager
{
    static dispatch_once_t onceQueue;
    static PhotosCacheManager *photosCacheManager = nil;
    
    dispatch_once(&onceQueue, ^{ photosCacheManager = [[self alloc] init]; });
    return photosCacheManager;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _mwImages = [NSMutableArray array];
        _cachedImages = [NSMutableArray array];
    }
    return self;
}

-(void)cleanDsiplayCache{
    [_mwImages removeAllObjects];
    [_cachedImages removeAllObjects];
}
-(void)displayImages:(NSArray*)images fromViewController:(UIViewController<PhotoBrowsingDelegate>*)vc showDelete:(BOOL)showDelete{
    
    _currentphotoBrowserBaserViewController = vc;
    
    [_cachedImages addObjectsFromArray:images];
    
    [_cachedImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if ([obj isKindOfClass:[UIImage class]]) {
            MWPhoto*photo = [MWPhoto photoWithImage:obj];
            [_mwImages addObject:photo];
        }else if ([obj isKindOfClass:[NSString class]]){
            MWPhoto*photo = [MWPhoto photoWithURL:[NSURL URLWithString:obj]];
             [_mwImages addObject:photo];
        }
    }];
    
    PhotoBrowserViewController*pvc =[[PhotoBrowserViewController alloc] initWithDelegate:self];
    
    pvc.actionDelegate = self;
    
    [pvc showDeleteButton:showDelete];
    
    [_currentphotoBrowserBaserViewController.navigationController pushViewController:pvc animated:YES];
    
}
-(void)deletePhotoAtIndex:(NSInteger)index{
    [_cachedImages removeObjectAtIndex:index];
    [_mwImages removeObjectAtIndex:index];
    
    if ([_currentphotoBrowserBaserViewController respondsToSelector:@selector(deletePhotoAtIndex:)]) {
        [_currentphotoBrowserBaserViewController deletePhotoAtIndex:index];
        
    }

    if (_cachedImages.count==0) {
        
        if ([_currentphotoBrowserBaserViewController respondsToSelector:@selector(foredQuitPhotoBrowser)]) {
            
            [_currentphotoBrowserBaserViewController foredQuitPhotoBrowser];
            
        }
       
    }
}


- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser{
    return _mwImages.count;
}
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index{
    if (index < _mwImages.count)
        return [_mwImages objectAtIndex:index];
    return nil;
}


@end
