//
//  EventHubRootViewController.m
//  EventsHub
//
//  Created by 亮 姚 on 14-11-12.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "EventHubRootViewController.h"
#import "AccountManager.h"
#import "SocialServices.h"



@interface EventHubRootViewController ()

@end

@implementation EventHubRootViewController



-(void)awakeFromNib{
    self.backgroundImage = [UIImage imageNamed:@"bg-menu"];
    
    self.scaleContentView = NO;
    self.scaleMenuView = NO;
    self.scaleBackgroundImageView = NO;
    self.parallaxEnabled = NO;
    
   
    NSDictionary *info = [[SocialServices sharedSocialServices] getAutoSocialSignedIn];
    
    
    NSNumber*shown =  [[NSUserDefaults standardUserDefaults] objectForKey:@"WelcomeShowed"];
    
    if (!shown.boolValue) {
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
        
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"WelcomeShowed"];
        
    }else{
        
        if (info) {
            
            [[SocialServices sharedSocialServices] signInWithServiceInfo:info complete:^(bool succed) {
                
            }];
        }
        
        self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainNavi"];
    }

    
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
    self.delegate = self;
    
}




- (void)sideMenu:(RESideMenu *)sideMenu didRecognizePanGesture:(UIPanGestureRecognizer *)recognizer{
    
}
- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController{
    [menuViewController viewWillAppear:YES];
}
- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController{
    
}
- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController{
    
}
- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController{
    
}

@end




