//
//  UIHelper.m
//  EventsHub
//
//  Created by alexander on 12/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "UIHelper.h"
#import "AppDelegate.h"
@implementation UIHelper

static NSInteger ImageThumbnailScale = 2;


+(NSInteger)ImageThumbnailScale{
    return ImageThumbnailScale;
}

+(void)setImageThumbnailScale:(NSInteger)scale{
    ImageThumbnailScale =scale;
}

+(UIColor*)greenColor{
    return [UIColor colorFromHexString:@"#35c781"];
}

+(UIColor*)blueColor{
    return [UIColor colorFromHexString:@"#009dda"];
}

+(UIColor*)darkColor {
    return [UIColor colorFromHexString:@"#313741"];
}

+(UIColor*)whiteColor {
    return [UIColor colorFromHexString:@"#fafafa"];
}

+(UIColor*)stoneColor {
    return [UIColor colorFromHexString:@"#748288"];
}

+(UIColor*)greyColor {
    return [UIColor colorFromHexString:@"#b3bdc1"];
}

+(UIColor*)orangeColor {
    return [UIColor colorFromHexString:@"#ff6046"];
}

+(UIColor*)ghostColor {
    return [UIColor colorFromHexString:@"#d7d8db"];
}

+(UIColor*)whiteBackgroundColor {
    return [UIColor colorFromHexString:@"#f5f5f5"];
}
+(UIColor*)blueThemeColor{
    return [UIColor colorWithRed:45/255. green:170/255. blue:225/255. alpha:1];
}

+ (UIFont*)defaultFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"Heiti SC" size:size];
}
+(UIColor*)femaleColor {
    return [UIColor colorWithRed:255/255. green:105/255. blue:195/255. alpha:1];
}

+(UIColor*)maleColor {
    return [UIColor colorWithRed:102/255. green:168/255. blue:217/255. alpha:1];
}

+(CGFloat)screenWidth{
    return [[[[(AppDelegate*)[[UIApplication sharedApplication] delegate] window] rootViewController] view] frame].size.width;
}
@end
