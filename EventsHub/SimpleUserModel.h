//
//  SimpleUserModel.h
//  EventsHub
//
//  Created by alexander on 4/14/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "JSONModel.h"

@interface SimpleUserModel : JSONModel
@property (nonatomic)NSString*ref;
@property (nonatomic)NSString*username;
@property (nonatomic)NSString*avatar_path;
@property (nonatomic)NSString*gender;
@end
