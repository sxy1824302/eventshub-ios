//
//  BaseUIViewController.h
//  EventsHub
//
//  Created by alexander on 1/30/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FontAwesomeKit.h>
#import "UIHelper.h"
#import <RESideMenu.h>
#import "PhotoPicker.h"

@interface BaseUIViewController : UIViewController
@property (nonatomic)UIBarButtonItem*backBarButton;
@property (nonatomic)UIBarButtonItem*menuBarButton;
@property (nonatomic)PhotoPicker*photoPicker;
@end


@interface BaseTableViewController : UITableViewController
@property (nonatomic)PhotoPicker*photoPicker;
@end



@interface BaseCollectionViewController : UICollectionViewController
@property (nonatomic)UIBarButtonItem*backBarButton;
@property (nonatomic)UIBarButtonItem*menuBarButton;
@property (nonatomic)PhotoPicker*photoPicker;
@end