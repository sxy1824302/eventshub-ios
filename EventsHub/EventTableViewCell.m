//
//  EventTableViewCell.m
//  EventsHub
//
//  Created by alexander on 2/10/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "EventTableViewCell.h"
#import "EventsManager.h"
#import "UIHelper.h"
#import "NetworkManager.h"
#import "AccountManager.h"
#import <UIImageView+WebCache.h>
#import "NSDate+ISO8601.h"
#import "Event+Display.h"
@implementation EventTableViewCell{
    UIView*_maskView;
}

- (void)awakeFromNib {
    // Initialization code
    if ([self respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        
        self.preservesSuperviewLayoutMargins = NO;
        self.activityTitleImage.contentMode = UIViewContentModeScaleAspectFill;
        self.activityTitleImage.layer.masksToBounds = YES;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setEvent:(Event *)event{
    _event = event;
    
    
    self.activityNameLabel.text = _event.title;//
    
    
    NSArray*items =  @[_event.address.city?:@"",_event.address.district?:@"",_event.address.street1?:@"",_event.address.street2?:@"",_event.pio?:@""];
    
   
    self.adressLabel.text = [items componentsJoinedByString:@" "];
    
    
    
    self.ActivityTimeLabel.text = [_event startAndEndDisplayString];
    
    
    
    
    
    
    NSString* urlString = @"";
    if ([_event.posters count]>0) {
        urlString = [NSString stringWithFormat:@"%@/thumbnails/%@/%i.jpg",HostAPI,[[_event posters] firstObject],(int)(_activityTitleImage.frame.size.width*[UIHelper ImageThumbnailScale])];
        
        [self.activityTitleImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"default-qt"]];
        
    }else{
        
        if (_event.tags.count>0) {
            
            self.activityTitleImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"default-%@", [[EventsManager sharedEventsManager] tagPinyinFromTagID:_event.tags[0]]]];
        }else{
            self.activityTitleImage.image = [UIImage imageNamed:@"default-qt"];
        }
        
    }
    
    self.ActivityTimeLabel.textColor = self.adressLabel.textColor =[UIHelper stoneColor];
    
    if ([[NSDate dateFromISOString:_event.date_end] timeIntervalSinceNow]<=0){
        self.activityNameLabel.textColor =[UIHelper stoneColor];
        
        self.ActivityTimeLabel.textColor = self.adressLabel.textColor = self.activityNameLabel.textColor =[UIHelper stoneColor];
        
        if (!_maskView) {
            _maskView = [[UIView alloc] initWithFrame:self.activityTitleImage.frame];
            [self.contentView addSubview:_maskView];
            _maskView.backgroundColor = [[UIHelper stoneColor] colorWithAlphaComponent:0.3];
        }
        _maskView.hidden = NO;
        self.infoLabel.backgroundColor = [UIHelper orangeColor];
        
        self.infoLabel.text = NSLocalizedString(@"已结束", nil);
    }
    else{
        
        if ([_event.owner isEqualToString:[[AccountManager sharedAccountManager] currentAccountID]]){
            
            self.infoLabel.backgroundColor = [UIColor colorFromHexString:@"#4dbde9"];
            
            self.infoLabel.text = NSLocalizedString(@"我组织的", nil);
            
        }
        
        _maskView.hidden = YES;
        
        self.activityNameLabel.textColor = [UIHelper darkColor];
        
        self.infoLabel.hidden = YES;
        
    }
    
    FAKFontAwesome *heart = [FAKFontAwesome heartIconWithSize:kMinimalFontSize];
    NSMutableAttributedString*att = [[NSMutableAttributedString alloc ] initWithAttributedString:heart.attributedString];
    NSAttributedString*number = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %i", _event.like_count.intValue] attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:kMinimalFontSize]}];
    [att appendAttributedString:number];
    [att addAttributes:@{NSForegroundColorAttributeName:[UIHelper stoneColor]} range:NSMakeRange(0, att.length)];
    
    self.activityLikesLabel.attributedText =att;
    
    
    
    
    
    NSNumber* parCount = _event.participant_count;
    int totalLimit = [_event.total_participants_limit intValue];
    
    FAKFontAwesome *fa = [FAKFontAwesome groupIconWithSize:kMinimalFontSize];
    att = [[NSMutableAttributedString alloc ] initWithAttributedString:fa.attributedString];
    
    NSString*string =[NSString stringWithFormat:@" %i%@",parCount.intValue,totalLimit>0?[NSString stringWithFormat:@"/%i",totalLimit]:@"/50"] ;
    
    number = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:kMinimalFontSize]}];
    [att appendAttributedString:number];
    [att addAttributes:@{NSForegroundColorAttributeName:[UIHelper stoneColor]} range:NSMakeRange(0, att.length)];
    
    self.activityMemberLabel.attributedText =att;
    
}
@end
