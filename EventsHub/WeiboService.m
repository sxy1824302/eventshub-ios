//
//  WeiboService.m
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "WeiboService.h"
#import "NetworkManager.h"
#import "ImageDataConvertor.h"

@implementation WeiboService
{
    WeiboServiceManager*_manager;
}
@synthesize appId;
@synthesize oauthToken;
@synthesize expireDate;
@synthesize userId;
@synthesize status;
-(WeiboServiceManager*)manager{
    if (!_manager) {
        _manager =[[WeiboServiceManager alloc] initWithService:self];
    }
    return _manager;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.appId = [aDecoder decodeObjectForKey:@"appId"];
        self.oauthToken = [aDecoder decodeObjectForKey:@"oauthToken"];
        self.expireDate = [aDecoder decodeObjectForKey:@"expireDate"];
        self.userId  = [aDecoder decodeObjectForKey:@"userId"];
        
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.appId forKey:@"appId"];
    [aCoder encodeObject:self.oauthToken forKey:@"oauthToken"];
    [aCoder encodeObject:self.expireDate forKey:@"expireDate"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
}
-(NSString*)archieveKey{
    return WeiboSerivceArchive;
}
-(NSString*)serverAPIKey{
    return kWeiboAPIKey;
}
@end




@implementation WeiboServiceManager{

    ShareCompletedBlock _shareBlock;
    AuthCompletedBlock _authBlock;
    id _observer;
}
- (instancetype)initWithService:(WeiboService*)service
{
    self = [super init];
    if (self) {
        _service= service;
        
#ifdef DEBUG
        [WeiboSDK enableDebugMode:YES];
#endif
        [WeiboSDK registerApp:service.appId];
        

    }
    return self;
}
-(void)logoutSerivice{
    [WeiboSDK logOutWithToken:_service.oauthToken delegate:self withTag:nil];
}



-(void)shareWithTitle:(NSString*)title description:(NSString*)desc thumbNail:(UIImage*)thumbnail  andLink:(NSString*)link eventID:(NSString*)eventID andFinish:(ShareCompletedBlock)action{
    _shareBlock = action;
    
    if (!thumbnail) {
        thumbnail = [UIImage imageNamed:@"AppIcon40x40@2x.png"];
    }else{
        thumbnail = [ImageDataConvertor thumbnmailData:UIImageJPEGRepresentation(thumbnail, 1) MIMEType:JPEGMIME];
    }
    
    
    WBMessageObject *message = [WBMessageObject message];
    
    WBWebpageObject *webpage = [WBWebpageObject object];
    
    webpage.objectID = eventID;
    
    webpage.title = title;
    
    webpage.description = desc;
    
    
    webpage.thumbnailData = UIImagePNGRepresentation(thumbnail);
    
    webpage.webpageUrl = link;
    
//    message.mediaObject = webpage;
    message.text = [NSString stringWithFormat:@"%@ %@ (分享自@聚吧EventsHub)",title,link];

    WBImageObject* imageObject = [WBImageObject object];
    imageObject.imageData = UIImagePNGRepresentation(thumbnail);
    message.imageObject = imageObject;
    //incase expired
    WBAuthorizeRequest *authRequest = [WBAuthorizeRequest request];
    authRequest.redirectURI = kWeiboOauthRedirectURI;
    authRequest.scope = @"all";
    
    WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:message authInfo:authRequest access_token:_service.oauthToken];

    [WeiboSDK sendRequest:request];
}



-(void)startAuthComplete:(AuthCompletedBlock)complete{
    
    _authBlock= complete;
    
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = kWeiboOauthRedirectURI;
    request.scope = @"all";
    [WeiboSDK sendRequest:request];
}


-(void)getUserInfoComplete:(GetProfileCompletedBlock)complete{
    

    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.generalHttpRequestManager GET:@"https://api.weibo.com/2/users/show.json" parameters:@{@"source":kWeiboAppKey,@"access_token":_service.oauthToken,@"uid":@([_service.userId integerValue])} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            complete(operation.responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(nil);
        }];
    }];
}


-(void)WBResponseStatusCode:(WeiboSDKResponseStatusCode)code{

}
- (void)request:(WBHttpRequest *)request didReceiveResponse:(NSURLResponse *)response{
    DDLogVerbose(@"%@",request);
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error{
    DDLogVerbose(@"%@",error.localizedDescription);
}

-(void)sinaAuthedWithResponce:(WBAuthorizeResponse*)response{
    self.service.oauthToken =[(WBAuthorizeResponse *)response accessToken];
    self.service.userId =  [(WBAuthorizeResponse *)response userID];
    self.service.expireDate = [(WBAuthorizeResponse *)response expirationDate];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ServiceInfoChangedNotification object:self.service];
    
    NSDictionary*info = @{@"id": self.service.userId,@"username": self.service.userId,@"login_type":kWeiboAPIKey};
        
    _authBlock(info);
    
    _authBlock =nil;
}


#pragma mark delegate

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    DDLogVerbose(@"weibo request:%@",request);
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    
    if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
        
     
        if (response && response.statusCode == 0) {
            
            NSString *message = [NSString stringWithFormat:@"响应状态: %d\nresponse.userId: %@\nresponse.accessToken: %@\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode,[(WBAuthorizeResponse *)response userID], [(WBAuthorizeResponse *)response accessToken], response.userInfo, response.requestUserInfo];
            
            DDLogVerbose(message);
            
            [self sinaAuthedWithResponce:(WBAuthorizeResponse*)response];
            
        }else{
            
            _authBlock(nil);
            
            _authBlock= nil;
            
        }

        
    }else {
        DDLogVerbose(@"weibo respon:%@",response);
        BOOL shareFinish = YES;
        if (response.statusCode!=WeiboSDKResponseStatusCodeSuccess) {
            shareFinish = NO;
        }
        if (_shareBlock) {
            _shareBlock(shareFinish);
        }
    }
    
}

@end