//
//  Event+Join.h
//  EventsHub
//
//  Created by 亮 姚 on 14-11-29.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "Event.h"

@interface Event (Join)
-(void)joinWithData:(NSDictionary*)data   complete:(void(^)(NSDictionary* responce,BOOL success))completeBlock;
-(void)deleteParticipantWithUserID:(NSString*)userID andHandle:(void(^)(NSDictionary* responce,BOOL success))completeBlock;
-(void)checkJoinedWithUserID:(NSString*)userID andHandle:(void(^)(NSDictionary* responce,BOOL success))completeBlock;
@end
