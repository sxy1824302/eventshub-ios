//
//  MyEventsViewController.m
//  EventsHub
//
//  Created by alexander on 1/6/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "MyEventsViewController.h"
#import "EventTableViewCell.h"
#import "EventsManager.h"
#import "UIHelper.h"
#import "NetworkManager.h"
#import "AccountManager.h"
#import "EventDetailViewController.h"


typedef NS_ENUM(NSInteger, EventType) {
    EventTypeCreated,
    EventTypeParticipanted,
    EventTypeInterested,
};


@interface MyEventsViewController (){
    NSArray *_eventsArray;
    Event*_tempEvent;
    
    EventType _filterType;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation MyEventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    
    [self setDropDownMenuTitles:@[NSLocalizedString(@"我创建的", nil),NSLocalizedString(@"我参加的",nil),NSLocalizedString(@"我感兴趣的",nil)]];
}

-(void)viewWillAppear:(BOOL)animated{
    [self reloadEvents];
}

-(void)menuChangedWithIndex:(NSInteger)idx{
    
    switch (idx) {
        case 0:
            
            _filterType = EventTypeCreated;
            break;
            
        case 1:
            
            _filterType = EventTypeParticipanted;
            break;
            
        case 2:
            
            _filterType = EventTypeInterested;
            break;
            
        default:
            break;
    }
    
    
    [self reloadEvents];
    
    [self hideTopMenu];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)reloadEvents{
    
    
    
    
    MBProgressHUD*hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"LookingForEvents", nil);
    
    
    switch (_filterType) {
        case EventTypeCreated:
        {
            [[EventsManager sharedEventsManager] getCreatedActivity:^(NSArray *events, BOOL success) {
                if (events) {
                    _eventsArray = events;
                    [self.tableView reloadData];
                    
                }
                [hud hide:YES];
            }];
        }
            break;
        case EventTypeInterested:
        {
            [[EventsManager sharedEventsManager] getInterestedEvents:^(NSArray *events) {
                if (events) {
                    _eventsArray = events;
                    [self.tableView reloadData];
                    
                }
                [hud hide:YES];
            }];
        }
            break;
        case EventTypeParticipanted:
        {
            [[EventsManager sharedEventsManager] getAttendedActivity:^(NSArray *events, BOOL success) {
                if (events) {
                    _eventsArray = events;
                    [self.tableView reloadData];
                    
                }
                [hud hide:YES];
            }];
        }
            break;
        default:
            break;
    }
    
    
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_eventsArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EventTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"EventsTableViewCell"];
    
    Event* showEvent = [_eventsArray objectAtIndex:indexPath.row];
    cell.event = showEvent;

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _tempEvent= [_eventsArray objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"DetailSegue" sender:self];
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"DetailSegue"]) {
        EventDetailViewController*dvc = segue.destinationViewController;
        
        dvc.dataSource.event = _tempEvent;
    }
}

@end
