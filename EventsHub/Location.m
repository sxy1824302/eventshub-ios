//
//  Location.m
//  EventsHub
//
//  Created by alexander on 12/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Location.h"

@implementation Location
+(instancetype)geoPointWithCoordicates:(CLLocationCoordinate2D)coord{
    Location *l = [Location new];
    l.latitude = @(coord.latitude);
    l.longitude = @(coord.longitude);
    return l;
}
@end
