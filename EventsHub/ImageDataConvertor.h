//
//  ImageDataConvertor.h
//  EventsHub
//
//  Created by 孙翔宇 on 2/8/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kCompressionRate  0.4


#define JPEGMIME @"image/jpeg"


#define PNGMIME @"image/png"

@interface ImageDataConvertor : NSObject
+(NSData*)convertImage:(UIImage*)image infoBlock:(void(^)(float compression,float megabytes, BOOL *invalidate, NSString *httpType))infoCheck;

+(void)convertImage:(NSArray*)images forEachImages:(void(^)(NSData*aData, float compression,float megabytes, NSString *httpType))eachImage;


+(UIImage*)thumbnmailData:(NSData*)imageData MIMEType:(NSString *)MIMEType;

@end
