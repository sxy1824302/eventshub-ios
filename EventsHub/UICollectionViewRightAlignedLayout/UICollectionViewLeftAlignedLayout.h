//
//  UICollectionViewLeftAlignedLayout.h
//  EventsHub
//
//  Created by alexander on 3/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewLeftAlignedLayout : UICollectionViewFlowLayout

@end


/**
 *  Just a convenience protocol to keep things consistent.
 *  Someone could find it confusing for a delegate object to conform to UICollectionViewDelegateFlowLayout
 *  while using UICollectionViewRightAlignedLayout.
 */
@protocol UICollectionViewDelegateRightAlignedLayout <UICollectionViewDelegateFlowLayout>

@end