//
//  UICollectionViewLeftAlignedLayout.m
//  EventsHub
//
//  Created by alexander on 3/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "UICollectionViewLeftAlignedLayout.h"

@interface UICollectionViewLayoutAttributes (LeftAligned)

- (void)leftAlignFrameOnWidth:(CGFloat)width;

@end

@implementation UICollectionViewLayoutAttributes (LeftAligned)

- (void)leftAlignFrameOnWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.origin.x = 0;
    self.frame = frame;
}

@end


@implementation UICollectionViewLeftAlignedLayout

#pragma mark - UICollectionViewLayout

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray* attributesToReturn = [super layoutAttributesForElementsInRect:rect];
    for (UICollectionViewLayoutAttributes* attributes in attributesToReturn) {
        if (nil == attributes.representedElementKind) {
            NSIndexPath* indexPath = attributes.indexPath;
            attributes.frame = [self layoutAttributesForItemAtIndexPath:indexPath].frame;
        }
    }
    return attributesToReturn;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes* currentItemAttributes = [super layoutAttributesForItemAtIndexPath:indexPath];
    
    BOOL isFirstItemInSection = indexPath.item == 0;
    
    if (isFirstItemInSection) {
        [currentItemAttributes leftAlignFrameOnWidth:self.collectionView.frame.size.width];
        return currentItemAttributes;
    }
    
    NSIndexPath* previousIndexPath = [NSIndexPath indexPathForItem:indexPath.item-1 inSection:indexPath.section];
    
    CGRect previousFrame = [self layoutAttributesForItemAtIndexPath:previousIndexPath].frame;
    
    CGRect currentFrame = currentItemAttributes.frame;
    
    CGRect strecthedCurrentFrame = CGRectMake(0,
                                              currentFrame.origin.y,
                                              self.collectionView.frame.size.width,
                                              currentFrame.size.height);
    
    // if the current frame, once left aligned to the left and stretched to the full collection view
    // widht intersects the previous frame then they are on the same line
    BOOL isFirstItemInRow = !CGRectIntersectsRect(previousFrame, strecthedCurrentFrame);
    
    if (isFirstItemInRow) {
        [currentItemAttributes leftAlignFrameOnWidth:self.collectionView.frame.size.width];
        return currentItemAttributes;
    }
    
    CGFloat previousFrameRightPoint = CGRectGetMaxX(previousFrame);
    
    CGRect frame = currentItemAttributes.frame;
    
    CGFloat minimumInteritemSpacing = [self evaluatedMinimumInteritemSpacingForItemAtIndex:indexPath.row];
    
    frame.origin.x = previousFrameRightPoint + minimumInteritemSpacing ;
    
    currentItemAttributes.frame = frame;
    return currentItemAttributes;
}

- (CGFloat)evaluatedMinimumInteritemSpacingForItemAtIndex:(NSInteger)index
{
    if ([self.collectionView.delegate respondsToSelector:@selector(collectionView:layout:minimumInteritemSpacingForSectionAtIndex:)]) {
        id<UICollectionViewDelegateRightAlignedLayout> delegate = (id<UICollectionViewDelegateRightAlignedLayout>)self.collectionView.delegate;
        
        return [delegate collectionView:self.collectionView layout:self minimumInteritemSpacingForSectionAtIndex:index];
    } else {
        return self.minimumInteritemSpacing;
    }
}

@end
