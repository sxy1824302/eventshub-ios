//
//  MessageCenter.h
//  EventsHub
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"

@import UIKit;

typedef NS_ENUM(NSInteger, MessageType) {
    MessageTypeMessage = 0,
    MessageTypeWarning,
    MessageTypeError,
    MessageTypeSuccess,
};
@interface MessageCenter : NSObject

+(void)showMessage:(NSString*)message withType:(MessageType)type inViewController:(UIViewController*)vc;
@end
