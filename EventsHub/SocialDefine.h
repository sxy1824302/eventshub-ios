//
//  SocialDefine.h
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#ifndef EventsHub_SocialDefine_h
#define EventsHub_SocialDefine_h

#define kTencentAppID @"1103396799"


#define kWeiboAppKey         @"752177610"
#define kWeiboOauthRedirectURI    @"https://api.weibo.com/oauth2/default.html"


#define kWechatAppId @"wx6cc87f5cd95d2608"

typedef void(^ShareCompletedBlock)(BOOL succeed);
typedef void(^AuthCompletedBlock)(NSDictionary* authInfo);
typedef void(^GetProfileCompletedBlock)(NSDictionary* profileDict);


#define kTencentAPIKey @"qq"
#define kWeiboAPIKey @"weibo"
#define kWechatAPIKey @"wechat"


#define TencentSerivceArchive @"TencentSerivceArchive"
#define WeiboSerivceArchive @"WeiboSerivceArchive"
#define WechatSerivceArchive @"WechatSerivceArchive"


#define ServiceInfoChangedNotification  @"ServiceInfoChangedNotification"


@protocol SocialService <NSObject>

@property (nonatomic)NSString*appId;
@property (nonatomic)NSString*oauthToken;
@property (nonatomic)NSDate*expireDate;
@property (nonatomic)NSString*userId;

@property (nonatomic)BOOL status;

-(NSString*)archieveKey;
-(NSString*)serverAPIKey;
@end


#endif
