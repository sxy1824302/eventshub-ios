//
//  UIHelper.h
//  EventsHub
//
//  Created by alexander on 12/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Colours.h>
#import <FontAwesomeKit.h>
#import <UIImageView+WebCache.h>
# define GuidlineMetric(__pixel__) (__pixel__*.5)

#define kMinimalFontSize GuidlineMetric(20)

#define kIconSizeNormal CGSizeMake(GuidlineMetric(88),GuidlineMetric(88))

#define kIconFontSize GuidlineMetric(32)





@interface UIHelper : NSObject

+(NSInteger)ImageThumbnailScale;
+(void)setImageThumbnailScale:(NSInteger)scale;

+(UIColor*)greenColor;

+(UIColor*)blueThemeColor;

+(UIColor*)blueColor;

+(UIColor*)darkColor ;

+(UIColor*)whiteColor ;

+(UIColor*)stoneColor ;

+(UIColor*)greyColor ;

+(UIColor*)orangeColor ;

+(UIColor*)ghostColor ;

+(UIColor*)whiteBackgroundColor ;


+ (UIFont*)defaultFontWithSize:(CGFloat)size;

+(UIColor*)femaleColor ;
+(UIColor*)maleColor ;

+(CGFloat)screenWidth;
@end



