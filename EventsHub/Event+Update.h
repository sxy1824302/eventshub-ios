//
//  Event+Update.h
//  EventsHub
//
//  Created by alexander on 2/15/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event.h"

@interface Event (Update)

-(void)updateSucceed:(void(^)(BOOL succeed))succeedBlock;


-(void)updateEventWithEventDic:(NSDictionary*)eventDic andPosters:(NSArray*)posters succeed:(void(^)(BOOL succeed,NSString* idString))succeedBlock;


@end
