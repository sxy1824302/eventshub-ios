//
//  Event+Participant.m
//  EventsHub
//
//  Created by alexander on 4/23/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event+Participant.h"
#import "NetworkManager.h"
@implementation Event (Participant)
//获取当前活动参与者的列表
-(void)getParticipantComplete:(void(^)(NSArray* participants))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/events/%@/participants",self._id] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            completeBlock(responseObject[@"participants"]);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);
            
            completeBlock(nil);
            
        }];
    }];
}
@end
