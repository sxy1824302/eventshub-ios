//
//  HintView.h
//  EventsHub
//
//  Created by alexander on 1/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HintView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end
