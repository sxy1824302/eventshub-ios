//
//  EventsHubSystem.h
//  EventsHub
//
//  Created by alexander on 3/20/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DDTTYLogger.h>
#import <GBDeviceInfo_iOS.h>



extern  NSString*const User_LoginNotification ;
extern  NSString*const User_LogoutNotification ;
extern  NSString*const User_InfoUpdatedNotification;



@interface EventsHubSystem : NSObject
@property (nonatomic)GBDeviceInfo*deviceInfo;

@property (nonatomic)NSString*appVersion;
@property (nonatomic)NSString*appID;

+ (id)sharedEventsHubSystem;

- (void)intializeSystem;
@end
