//
//  CircleButton.h
//  EventsHub
//
//  Created by apple on 15/1/29.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleButton : UIButton

- (void)initializePayload;

@end
