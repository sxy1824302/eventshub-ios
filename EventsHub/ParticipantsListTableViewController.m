//
//  ParticipantsListTableViewController.m
//  EventsHub
//
//  Created by alexander on 1/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "ParticipantsListTableViewController.h"
#import "ParticipantCell.h"
#import "UIHelper.h"
#import "Participant.h"
#import "NetworkManager.h"
#import "EventsManager.h"
#import "HintView.h"
#import "MBProgressHUD.h"
#import "MessageCenter.h"
#import "OtherProfileViewController.h"
#import <FAKFontAwesome.h>
@interface ParticipantsListTableViewController ()<SWTableViewCellDelegate>{
    NSMutableArray*_participants;
    
    BOOL _shownHint;
    
    NSInteger _selectedIndex;
    
    BOOL _isMineEvent;
    
    NSString*_selectedTempUser;
}

@end

@implementation ParticipantsListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _isMineEvent = IsMyEvent(_event);
    
   
    self.tableView.tableFooterView = [UIView new];
    [_event getParticipantComplete:^(NSArray *participants) {
        if (participants) {
            
            NSError* error;
            
            _participants = [Participant arrayOfModelsFromDictionaries:participants error:&error];
            
            [self.tableView reloadData];
        }
    }];

    
    
    
    if (_isMineEvent) {
        
        HintView *hv = [[UINib nibWithNibName:@"HintView" bundle:nil] instantiateWithOwner:nil options:nil][0];
        
        hv.backgroundColor = [UIHelper blueThemeColor];
        hv.frame = CGRectMake(0, 0, self.view.frame.size.width, 20);
        
        self.tableView.tableHeaderView = hv;
    }
 
   
//    
//    
//    FAKFontAwesome *heart = [FAKFontAwesome heartOIconWithSize:20];
//   
//    UIBarButtonItem* search = [[UIBarButtonItem alloc] initWithImage:[heart imageWithSize:CGSizeMake(40,40)] style:UIBarButtonItemStyleBordered target:self action:@selector(interest:)];
//    
//    FAKFontAwesome *heart1 = [FAKFontAwesome heartIconWithSize:20];
//    UIBarButtonItem *filter = [[UIBarButtonItem alloc] initWithImage:[heart1 imageWithSize:CGSizeMake(40,40)] style:UIBarButtonItemStyleBordered target:self action:@selector(interest:)];
//    

}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _participants.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isMineEvent) {
        NSInteger row = indexPath.row;
        Participant*p = _participants[row];
        if ([p.note length]>0) {
            if ([indexPath isEqual:[tableView indexPathForSelectedRow]]) {
                return 76;
            }
            return 52;
        }
        return 40;
    }else{
        
        return 50;
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_isMineEvent) {
        
        NSInteger row = indexPath.row;
        Participant*p = _participants[row];
        if ([p.note length]>0) {
            if (_selectedIndex == indexPath.row) {
                _selectedIndex = -1;
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }else{
                _selectedIndex = indexPath.row;
            }
        }
        [tableView beginUpdates];
        [tableView endUpdates];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else{
        
        Participant*p = _participants[indexPath.row];
    
        
        
        if (p.registered_user_ref) {
            
            _selectedTempUser = p.registered_user_ref;
            [self performSegueWithIdentifier:@"ProfileSegue" sender:self];
            
            
        }
    }

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"ProfileSegue"]) {
        
        OtherProfileViewController*ovc = segue.destinationViewController;
        
        ovc.userID = _selectedTempUser;
        
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    
    NSInteger row = [self.tableView indexPathForCell:cell].row;
    
    Participant*p = _participants[row];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = p.check.boolValue?NSLocalizedString(@"正在取消签到...", nil):NSLocalizedString(@"正在进行签到...", nil);
    
    NSString*phone_id = p.registered_user_ref;
    
    
    [_event signup:!p.check.boolValue participantId:phone_id participantName:p.username handle:^(BOOL success) {
        
        if (success) {
            
            p.check = @(!p.check.boolValue);
            
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
     
        [hud hide:YES];
  
    }];

    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell*acell;
    
    Participant*p = _participants[indexPath.row];
    
    if (_isMineEvent) {
      
        ParticipantCell *cell = (ParticipantCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        
        if (p.phone) {
            cell.rightUtilityButtons = [self rightButtonsWithCheck:p.check.boolValue];
        }
        
        cell.delegate = self;
        
        cell.nameLabel.text =p.username;
        
        cell.phoneLabel.text = p.phone;
        
        cell.noteLabel.text = p.note;
        
        [cell.signStateView userIsSigned:p.check.boolValue];
        
        acell = cell;
        
    }else{
        GuestParticipantCell *cell = (GuestParticipantCell *)[tableView dequeueReusableCellWithIdentifier:@"NonOwnerCell"];
        
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"NonOwnerCell"];
        
        cell.nameLabel.text = p.username;
        
        [cell.iconView.imageView setImage:[UIImage imageNamed:@"img-default-avatar"]];
        
        cell.genderLabel.text = nil;
        
        [cell.iconView.imageView sd_cancelCurrentImageLoad];
        
        if (p.registered_user_ref) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            [[AccountManager sharedAccountManager] getUserProfile:p.registered_user_ref succeed:^(UserAccount *account) {
               
                cell.user = account;
                
                FAKFontAwesome*fa ;
                
                if ([account.gender isEqualToString:@"M"]) {
                    fa = [FAKFontAwesome maleIconWithSize:12];
                    [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper maleColor]];
                }else if ([account.gender isEqualToString:@"F"]) {
                    fa = [FAKFontAwesome femaleIconWithSize:12];
                    [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper femaleColor]];
                }else{
                    fa = [FAKFontAwesome userIconWithSize:12];
                    [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper ghostColor]];
                }
                
                cell.genderLabel.attributedText = fa.attributedString;
                
                [cell.iconView loadAvatarWithURLComponents:account.avatar_path placeHolder:[UIImage imageNamed:@"img-default-avatar"]];
            }];
        }
        
        acell = cell;
    }

    
    return acell;
}
- (NSArray *)rightButtonsWithCheck:(BOOL)check
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    if (check) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIHelper orangeColor]title:NSLocalizedString(@"取消签到", nil)];
    }else{
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIHelper blueThemeColor]title:NSLocalizedString(@"签到", nil)];
    }
    
    return rightUtilityButtons;
}



@end
