//
//  Event+Display.m
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Event+Display.h"
#import "NSDate+ISO8601.h"
#import "EventsManager.h"
#import "Config.h"
#import "NetworkManager.h"
@implementation Event (Display)


NSDateFormatter *formatter1;
NSDateFormatter *formatter2;

+ (void)initialize
{
    if (self == [Event class]) {
        formatter1 = [NSDateFormatter new];
        formatter1.locale = [[NSLocale alloc] initWithLocaleIdentifier:kDefaultLocale];
        formatter1.dateStyle = NSDateFormatterFullStyle;
        
        
        formatter2 = [NSDateFormatter new];
        formatter2.locale = [[NSLocale alloc] initWithLocaleIdentifier:kDefaultLocale];
        formatter2.timeStyle = NSDateFormatterShortStyle;
    }
}

-(NSString*)startAndEndDisplayString{
    NSDate *start = [NSDate dateFromISOString:self.date_start];
    NSDate *end = [NSDate dateFromISOString:self.date_end];
    
    NSString*startS = [formatter1 stringFromDate:start] ;
    startS = [self removeYEAR:startS];
    
    
    NSString*startT = [formatter2 stringFromDate:start] ;
    NSString*endT = [formatter2 stringFromDate:end] ;
    

    
    return [NSString stringWithFormat:@"%@ %@-%@",startS,startT,endT];
}
-(NSString*)removeYEAR:(NSString*)startS{
    
    @try {
        
        NSRange range = [startS rangeOfString:@"年" options:NSCaseInsensitiveSearch];
        if (range.location!=NSNotFound) {
            startS = [startS substringWithRange:NSMakeRange(range.location+range.length,startS.length-(range.location+range.length))];
        }else{
            NSMutableArray* array = [[startS componentsSeparatedByString:@","] mutableCopy] ;
            [array removeLastObject];
            startS = [array componentsJoinedByString:@", "];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"date string convert year fialed");
    }
    @finally {
        
    }
    

    return startS;
}
@end
