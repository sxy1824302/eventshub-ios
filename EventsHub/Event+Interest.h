//
//  NSString+Interest.h
//  EventsHub
//
//  Created by alexander on 11/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"
@interface Event (Interest)
-(void)interest:(void(^)(NSDictionary* responce,BOOL success))completeBlock;
-(void)uninterest:(void(^)(NSDictionary* responce,BOOL success))completeBlock;
-(void)checkInterest:(void(^)(NSDictionary* responce,BOOL success))completeBlock;
@end
