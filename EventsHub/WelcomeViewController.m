//
//  WelcomeViewController.m
//  EventsHub
//
//  Created by alexander on 12/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "WelcomeViewController.h"
#import "UIHelper.h"
#import <FontAwesomeKit.h>
#import <RESideMenu.h>
#import "AccountManager.h"
#import <MBProgressHUD.h>
#import "SocialServices.h"
#import "Config.h"
@interface WelcomeViewController ()<UIScrollViewDelegate>{
    id _nob;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpace;
@property (weak, nonatomic) IBOutlet UIView *guidLine;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation WelcomeViewController{
    NSArray *_views;
    UIImageView* _backgroundImageView;
}



- (IBAction)changed:(id)sender {
    
    
    
}

-(void)setPage{
    NSInteger page =  floor(_scrollView.contentOffset.x/_scrollView.frame.size.width);
    
    self.pageControl.currentPage = page;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if (!decelerate) {
        [self setPage];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
     [self setPage];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    
    self.guidLine.backgroundColor = [UIHelper ghostColor];
    
    _infoLabel.textColor = [UIHelper stoneColor];
    [self.view setNeedsUpdateConstraints];
    
    
    
    FAKFontAwesome*faw = [FAKFontAwesome angleDoubleRightIconWithSize:GuidlineMetric(32)];
    [faw addAttribute:NSForegroundColorAttributeName value:[UIHelper blueThemeColor]];
    
    NSMutableAttributedString *m =[[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"跳过",nil) attributes:@{NSForegroundColorAttributeName:[UIHelper blueThemeColor],NSFontAttributeName:[UIHelper defaultFontWithSize:16]}];
    [m appendAttributedString:faw.attributedString];

    [self.continueButton setAttributedTitle:m forState:UIControlStateNormal];
    
    

    _nob = [[NSNotificationCenter defaultCenter] addObserverForName:User_LoginNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        self.sideMenuViewController.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainNavi"];
    }];
    
    UINib*nib = [UINib nibWithNibName:@"WecomePages" bundle:nil];
    _views = [nib instantiateWithOwner:nil options:nil];;
    
    UIView*imgv1 = _views.firstObject;
    
    UIView*imgv2 = _views.lastObject;
    
    [_scrollView addSubview:imgv1];
    [_scrollView addSubview:imgv2];
    
    _scrollView.backgroundColor =[UIColor  clearColor];
    
     _scrollView.delegate = self;
}

-(void)viewDidLayoutSubviews{
    if (!_backgroundImageView) {
        _backgroundImageView =[[UIImageView alloc] initWithFrame:self.scrollView.frame];
        _backgroundImageView.image = [UIImage imageNamed:@"img-profile-bg-default"];
        
        [self.view insertSubview:_backgroundImageView atIndex:0];
    }
    
    _scrollView.contentSize = CGSizeMake(2*self.view.frame.size.width, _scrollView.frame.size.height);
    
    UIView*imgv1 = _views.firstObject;
    
    UIView*imgv2 = _views.lastObject;

    imgv1.bounds = _scrollView.bounds;
    imgv1.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));

    imgv2.bounds = _scrollView.bounds;
    
    imgv2.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds)+CGRectGetWidth(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    
}

-(void)hideHUD:(id)notice{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:_nob];
}
- (IBAction)skip:(id)sender {
    
    [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MainNavi"] animated:YES];
    
}
- (IBAction)sinaSignIn:(id)sender {
    [[SocialServices sharedSocialServices] tryRegisternWithService:ServiceTypeWeibo fromViewController:self complete:^(bool succeed) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
- (IBAction)tencentAuth:(id)sender {
    [[SocialServices sharedSocialServices] tryRegisternWithService:ServiceTypeTencent fromViewController:self complete:^(bool succeed) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
- (IBAction)weiXinSign:(id)sender {
    [[SocialServices sharedSocialServices] tryRegisternWithService:ServiceTypeWechat fromViewController:self complete:^(bool succeed) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
