//
//  Event+Participant.h
//  EventsHub
//
//  Created by alexander on 4/23/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "Event.h"

@interface Event (Participant)
-(void)getParticipantComplete:(void(^)(NSArray* participants))completeBlock;
@end
