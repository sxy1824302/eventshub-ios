//
//  DropDownMenuViewController.m
//  EventsHub
//
//  Created by alexander on 1/6/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "DropDownMenuViewController.h"
#import "UIHelper.h"
#import <FAKFontAwesome.h>
@interface DropDownMenuViewController ()<EHDropDownBarDelegate>{
    UIButton *_titleButton;
    
    UITapGestureRecognizer *_tapGes;
    BOOL _isAnimating;
    
    
    NSString *_selectedTitle;

}
@property (nonatomic) UIView *containerView;
@end

@implementation DropDownMenuViewController




-(UIView*)containerView{
    
    if (!_containerView) {
        
        _containerView = [[UIView alloc] initWithFrame:self.view.bounds];
        _containerView.backgroundColor = [UIColor blackColor];
        _containerView.alpha = 0;
        [self.view insertSubview:_containerView belowSubview:_menu];
    }
    
    return _containerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleButton = [UIButton buttonWithType:UIButtonTypeSystem];
    
    [_titleButton addTarget:self action:@selector(toggleMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.titleView = _titleButton;

    
    _menu = [[EHDropDownBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,GuidlineMetric(98))];
    _menu.delegate = self;
    self.menu.hidden = YES;
    [self.view addSubview:_menu];
    
    _isAnimating= NO;
    
    
    _tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    
    [self.containerView addGestureRecognizer:_tapGes];
}
-(void)tap:(UITapGestureRecognizer*)tap{
    
    [self hideTopMenu];
}
-(void)setDropDownMenuTitles:(NSArray*)titles{
    [_menu applyNewTitles:titles];
    
    [_menu selectDefault];
}

-(void)menuDidClickedButtonAtIndex:(NSInteger)index withTitle:(NSString *)title{
    
    _selectedTitle = title;
    
    NSMutableAttributedString *att =[[NSMutableAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:_menu.fontSize]}];
    
    
    if(_opened){
        
        [att appendAttributedString:[FAKFontAwesome angleUpIconWithSize:_menu.fontSize].attributedString];
        
    }else{
        [att appendAttributedString:[FAKFontAwesome angleDownIconWithSize:_menu.fontSize].attributedString];
        
    }
  
    
    [_titleButton setAttributedTitle:att forState:UIControlStateNormal];
    
    [_titleButton sizeToFit];
    
    [self menuChangedWithIndex:index];
}


-(void)menuChangedWithIndex:(NSInteger)idx{
    
    
}
-(void)setOpened:(BOOL)opened{
    _opened = opened;
    NSMutableAttributedString *att;
    if(_opened){
      
         att =[[NSMutableAttributedString alloc] initWithString:_selectedTitle attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:_menu.fontSize]}];
        
        [att appendAttributedString:[FAKFontAwesome angleUpIconWithSize:_menu.fontSize].attributedString];
        
    }else{
 
         att =[[NSMutableAttributedString alloc] initWithString:_selectedTitle attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:_menu.fontSize]}];
        
        [att appendAttributedString:[FAKFontAwesome angleDownIconWithSize:_menu.fontSize].attributedString];
        
    }
    
    [_titleButton setAttributedTitle:att forState:UIControlStateNormal];
    
    [_titleButton sizeToFit];
}
-(void)toggleMenu:(UIButton*)btn{
    if (self.menu.hidden) {
        [self showTopMenu];
    }else{
        [self hideTopMenu];
    }
}
- (void) showTopMenu {
    if (_isAnimating||!self.menu.hidden) {
        return;
    }
    self.opened = YES;
    self.menu.hidden = NO;
    self.containerView.hidden = NO;
    
  
    CGRect menuFrame = self.menu.frame;
    menuFrame.origin.y = CGRectGetMaxY(self.navigationController.navigationBar.frame);
    
    _isAnimating = YES;
    [UIView animateWithDuration:0.4
                          delay:0.0
         usingSpringWithDamping:1.0
          initialSpringVelocity:4.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         self.menu.frame = menuFrame;
                         [self.containerView setAlpha: 0.3];
                     }
                     completion:^(BOOL finished){
                         _isAnimating = NO;
                          self.containerView.userInteractionEnabled = YES;
                     }];
    
}

- (void) hideTopMenu {
    if (_isAnimating||self.menu.hidden) {
        return;
    }
    self.opened = NO;
    
    self.containerView.userInteractionEnabled = NO;
    CGRect menuFrame = self.menu.frame;
    menuFrame.origin.y = CGRectGetMaxY(self.navigationController.navigationBar.frame)-self.menu.frame.size.height;
    
     _isAnimating = YES;
    [UIView animateWithDuration:0.3f
                          delay:0.05f
         usingSpringWithDamping:1.0
          initialSpringVelocity:4.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.menu.frame = menuFrame;
                        
                         [self.containerView setAlpha: 0];
                     }
                     completion:^(BOOL finished){
                         self.menu.hidden = YES;
                         self.containerView.hidden = YES;
                         self.menu.frame =menuFrame;
                         _isAnimating = NO;
                     }];
    
    
}


@end
