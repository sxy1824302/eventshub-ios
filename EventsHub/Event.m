//
//  Event.m
//  EventsHub
//
//  Created by alexander on 11/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Event.h"
#import "NSDate+ISO8601.h"

NSString *const EventValidationDomin = @"EventValidationDomin";

@implementation Event
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"description": @"eventDescription"
                                                       }];
}

-(NSString*)fee_description{
    if (!_fee_description) {
        return @"免费";
    }else if([_fee_description integerValue]){
        return [NSString stringWithFormat:@"每人 %i",[_fee_description intValue]];
    }
    return _fee_description;
}

+(BOOL)propertyIsOptional:(NSString *)propertyName{
    return YES;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.participant_count = @0;
    }
    return self;
}

-(BOOL)isExpired{
    NSDate *date = [NSDate dateFromISOString:_date_end];
    
    if ([date timeIntervalSinceNow]<=0) {
        return YES;
    }
    return NO;
}

@end
