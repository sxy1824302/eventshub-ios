//
//  WechatService.h
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SocialDefine.h"
#import "OAuth2Result.h"
#import "WXApi.h"
#import "WechatUserInfo.h"


@class WechatServiceManager;


@interface WechatService : NSObject<SocialService>

@property (nonatomic)NSString*refreshToken;

@property (nonatomic)NSString*authCode;

-(WechatServiceManager*)manager;

@end





@interface WechatServiceManager : NSObject<WXApiDelegate>

@property (nonatomic) WechatService*service;

- (instancetype)initWithService:(WechatService*)servic;

-(void)startAuthInViewController:(UIViewController*)vc Complete:(AuthCompletedBlock)complete;

-(void)getUserInfoComplete:(GetProfileCompletedBlock)complete;

-(void)shareWithTitle:(NSString*)title description:(NSString*)desc thumbNail:(UIImage*)thumbnail  andLink:(NSString*)link  andFinish:(ShareCompletedBlock)action;
@end
