//
//  EHSignStateView.m
//  EventsHub
//
//  Created by apple on 15/1/13.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import "EHSignStateView.h"

@implementation EHSignStateView{
    BOOL _isSigned;
    UILabel* _stateLabel;
}

-(void)awakeFromNib{
    NSLog(@"awakeFromNib");
    self.layer.cornerRadius = 3;
    _isSigned = NO;
    
    _stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _stateLabel.textAlignment = NSTextAlignmentCenter;
    _stateLabel.font = [UIFont systemFontOfSize:10];
    _stateLabel.textColor = [UIColor whiteColor];
    [self addSubview:_stateLabel];
    [self setState];
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 3;
        _isSigned = NO;
        
        _stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _stateLabel.textAlignment = NSTextAlignmentCenter;
        _stateLabel.textColor = [UIColor whiteColor];
        [self addSubview:_stateLabel];
        [self setState];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)userIsSigned:(BOOL)isSign{
    _isSigned = isSign;
    [self setState];
}

-(void)setState{
    self.backgroundColor = _isSigned?[UIHelper blueThemeColor]:[UIHelper orangeColor];
    _stateLabel.text = _isSigned?@"已签到":@"未签到";
}


@end
