//
//  BaseUIViewController.m
//  EventsHub
//
//  Created by alexander on 1/30/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "BaseUIViewController.h"

// TODO: refactor me

@interface BaseUIViewController ()

@end

@implementation BaseUIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIHelper whiteBackgroundColor];
    
    
    if (self.navigationController.viewControllers.count!=1) {
        
        FAKFontAwesome*fa = [FAKFontAwesome chevronLeftIconWithSize:kIconFontSize];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper blueThemeColor]];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateNormal];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper greyColor]];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateDisabled];
        [btn setBounds:CGRectMake(0, 0, 44, 44)];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [btn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem*item  = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.leftBarButtonItem = item;
        self.backBarButton = item;
    }else{
        
        FAKFontAwesome*fa = [FAKFontAwesome naviconIconWithSize:kIconFontSize];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper blueThemeColor]];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateNormal];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper greyColor]];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateDisabled];
        [btn setBounds:CGRectMake(0, 0, 44, 44)];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        UIBarButtonItem*item  = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [btn addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = item;
        self.menuBarButton = item;
    }
}


-(void)showMenu:(UIBarButtonItem*)item{
    [self.sideMenuViewController presentLeftMenuViewController];
}
-(void)back:(UIBarButtonItem*)back{
    [self.navigationController popViewControllerAnimated:YES];
}
-(PhotoPicker*)photoPicker{
    if (!_photoPicker) {
        _photoPicker= [PhotoPicker new];
        _photoPicker.attatedViewCotroller = (UIViewController<UIActionSheetDelegate,ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate> *) self;
    }
    return _photoPicker;
}

@end


@implementation BaseTableViewController

-(PhotoPicker*)photoPicker{
    if (!_photoPicker) {
        _photoPicker= [PhotoPicker new];
        _photoPicker.attatedViewCotroller = (UIViewController<UIActionSheetDelegate,ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate> *) self;;
    }
    return _photoPicker;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIHelper whiteBackgroundColor];
    if (self.navigationController.viewControllers.count!=1) {
        
        FAKFontAwesome*fa = [FAKFontAwesome chevronLeftIconWithSize:kIconFontSize];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper blueThemeColor]];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateNormal];
        [btn setBounds:CGRectMake(0, 0, 44, 44)];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [btn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem*item  = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.leftBarButtonItem = item;
        
    }else{
        
        FAKFontAwesome*fa = [FAKFontAwesome naviconIconWithSize:kIconFontSize];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper blueThemeColor]];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateNormal];
        [btn setBounds:CGRectMake(0, 0, 44, 44)];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        UIBarButtonItem*item  = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [btn addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = item;
    }
}


-(void)showMenu:(UIBarButtonItem*)item{
    [self.sideMenuViewController presentLeftMenuViewController];
}
-(void)back:(UIBarButtonItem*)back{
    [self.navigationController popViewControllerAnimated:YES];
}


@end


@implementation BaseCollectionViewController
-(PhotoPicker*)photoPicker{
    if (!_photoPicker) {
        _photoPicker= [PhotoPicker new];
        _photoPicker.attatedViewCotroller = (UIViewController<UIActionSheetDelegate,ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate> *) self;;
    }
    return _photoPicker;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIHelper whiteBackgroundColor];
    
    
    if (self.navigationController.viewControllers.count!=1) {
        
        FAKFontAwesome*fa = [FAKFontAwesome chevronLeftIconWithSize:kIconFontSize];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper blueThemeColor]];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateNormal];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper greyColor]];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateDisabled];
        [btn setBounds:CGRectMake(0, 0, 44, 44)];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [btn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem*item  = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.leftBarButtonItem = item;
        self.backBarButton = item;
    }else{
        
        FAKFontAwesome*fa = [FAKFontAwesome naviconIconWithSize:kIconFontSize];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper blueThemeColor]];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateNormal];
        [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper greyColor]];
        [btn setAttributedTitle:fa.attributedString forState:UIControlStateDisabled];
        [btn setBounds:CGRectMake(0, 0, 44, 44)];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        UIBarButtonItem*item  = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [btn addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = item;
        self.menuBarButton = item;
    }
}


-(void)showMenu:(UIBarButtonItem*)item{
    [self.sideMenuViewController presentLeftMenuViewController];
}
-(void)back:(UIBarButtonItem*)back{
    [self.navigationController popViewControllerAnimated:YES];
}


@end