//
//  EventsHubSystem.m
//  EventsHub
//
//  Created by alexander on 3/20/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "EventsHubSystem.h"
#import "Config.h"
#import "EventsManager.h"
#import "NetworkManager.h"
#import "SocialServices.h"
NSString*const User_LoginNotification = @"User_LoginNotification";
NSString*const User_LogoutNotification = @"User_LogoutNotification";
NSString*const User_InfoUpdatedNotification = @"User_InfoUpdatedNotification";


@implementation EventsHubSystem

+ (id)sharedEventsHubSystem
{
    static dispatch_once_t onceQueue;
    static EventsHubSystem *eventsHubSystem = nil;
    
    dispatch_once(&onceQueue, ^{ eventsHubSystem = [[self alloc] init]; });
    return eventsHubSystem;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        _appID =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
        
        _deviceInfo = [GBDeviceInfo deviceInfo];
    }
    return self;
}

-(void)intializeSystem{
    [EventsHubSystem initializeLogSystem];
    
    DDLogInfo(@"Stsytem started: ID[%@] Version[%@]",_appID,_appVersion);
    
    DDLogInfo(@"Current Memory : %f Gb",_deviceInfo.physicalMemory);
    
    [NetworkManager sharedNetworkManager];
    
    [SocialServices sharedSocialServices];
    
    
    
    [[EventsManager sharedEventsManager] initializeModule];
    
    
    
}


+(void)initializeLogSystem{
    
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
}
@end
