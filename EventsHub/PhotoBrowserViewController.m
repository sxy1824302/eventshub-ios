//
//  PhotoBrowserViewController.m
//  EventsHub
//
//  Created by alexander on 1/15/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "PhotoBrowserViewController.h"
#import "EventsManager.h"
#import "PhotosCacheManager.h"
@interface PhotoBrowserViewController ()

@end

@implementation PhotoBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.displayActionButton = NO;
    self.displayNavArrows = NO;
    self.displaySelectionButtons = NO;
    self.alwaysShowControls = NO;
    self.zoomPhotosToFill = NO;
    self.enableGrid = NO;
    self.startOnGrid = NO;
    self.enableSwipeToDismiss = NO;
    [self setCurrentPhotoIndex:0];
    
    if (IsMyEvent(_event)) {
        [self showDeleteButton:YES];
    }
    
}

-(void)showDeleteButton:(BOOL)isShow{
    if (isShow) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteCurrentPhoto:)];
    }
}

-(void)deleteCurrentPhoto:(UIBarButtonItem*)item{
    [_actionDelegate deletePhotoAtIndex:self.currentIndex];
    
    self.displayActionButton = NO;
    if ([self.delegate numberOfPhotosInPhotoBrowser:self]>0) {
        [self reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[PhotosCacheManager sharedPhotosCacheManager] cleanDsiplayCache];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
