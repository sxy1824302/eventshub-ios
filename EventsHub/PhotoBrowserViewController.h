//
//  PhotoBrowserViewController.h
//  EventsHub
//
//  Created by alexander on 1/15/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "MWPhotoBrowser.h"

@class Event;

@protocol PhotoBrowserViewActionDelegate <NSObject>


-(void)deletePhotoAtIndex:(NSInteger)index;
@end


@interface PhotoBrowserViewController : MWPhotoBrowser
@property(nonatomic,weak)Event*event;
@property (nonatomic,weak)id<PhotoBrowserViewActionDelegate>actionDelegate;
-(void)showDeleteButton:(BOOL)isShow;
@end
