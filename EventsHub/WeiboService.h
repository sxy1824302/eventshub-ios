//
//  WeiboService.h
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocialDefine.h"
#import "WeiboSDK.h"


@class WeiboServiceManager;



@interface WeiboService : NSObject<SocialService>

-(WeiboServiceManager*)manager;
@end




@interface WeiboServiceManager : NSObject<WBHttpRequestDelegate,WeiboSDKDelegate>

@property (nonatomic) WeiboService*service;

- (instancetype)initWithService:(WeiboService*)servic;


-(void)logoutSerivice;

-(void)startAuthComplete:(AuthCompletedBlock)complete;

-(void)getUserInfoComplete:(GetProfileCompletedBlock)complete;

-(void)shareWithTitle:(NSString*)title description:(NSString*)desc thumbNail:(UIImage*)thumbnail  andLink:(NSString*)link eventID:(NSString*)eventID andFinish:(ShareCompletedBlock)action;
@end
