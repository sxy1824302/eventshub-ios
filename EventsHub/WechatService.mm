//
//  WechatService.m
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "WechatService.h"
#import "NetworkManager.h"
#import "ImageDataConvertor.h"

@implementation WechatService
{
    WechatServiceManager*_manager;
}
@synthesize appId;
@synthesize oauthToken;
@synthesize expireDate;
@synthesize userId;
@synthesize status;
-(WechatServiceManager*)manager{
    if (!_manager) {
        _manager =[[WechatServiceManager alloc] initWithService:self];
    }
    return _manager;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.appId = [aDecoder decodeObjectForKey:@"appId"];
        self.oauthToken = [aDecoder decodeObjectForKey:@"oauthToken"];
        self.expireDate = [aDecoder decodeObjectForKey:@"expireDate"];
        self.userId  = [aDecoder decodeObjectForKey:@"userId"];
        self.refreshToken = [aDecoder decodeObjectForKey:@"refreshToken"];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.appId forKey:@"appId"];
    [aCoder encodeObject:self.oauthToken forKey:@"oauthToken"];
    [aCoder encodeObject:self.expireDate forKey:@"expireDate"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.refreshToken forKey:@"refreshToken"];
}
-(NSString*)archieveKey{
    return WechatSerivceArchive;
}
-(NSString*)serverAPIKey{
    return kWechatAPIKey;
}
@end


@implementation WechatServiceManager{
    ShareCompletedBlock _shareBlock;
    AuthCompletedBlock _authBlock;
    
    id _observer;
}
- (instancetype)initWithService:(WechatService*)service
{
    self = [super init];
    if (self) {
        _service= service;
        
        [WXApi registerApp:_service.appId withDescription:NSLocalizedString(@"聚吧", nil)];
        
    }
    return self;
}



-(void)shareWithTitle:(NSString*)title description:(NSString*)desc thumbNail:(UIImage*)thumbnail  andLink:(NSString*)link andFinish:(ShareCompletedBlock)action{
    _shareBlock = action;
    
    WXMediaMessage *message = [WXMediaMessage message];

    message.title = title;
    
    if (!thumbnail) {
        thumbnail = [UIImage imageNamed:@"AppIcon40x40@2x.png"];
    }else{
        thumbnail = [ImageDataConvertor thumbnmailData:UIImageJPEGRepresentation(thumbnail, 1) MIMEType:JPEGMIME];
    }
    

    thumbnail  = [UIImage imageNamed:@"AppIcon40x40@2x.png"];
        
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = link;
        
    [message setThumbImage:thumbnail];
    message.description = desc;
    message.mediaObject = ext;
    

    
    message.mediaTagName = @"WECHAT_TAG_JUMP_SHOWRANK";
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneTimeline;
    [WXApi sendReq:req];
}



-(void)startAuthInViewController:(UIViewController*)vc Complete:(AuthCompletedBlock)complete{
    
    _authBlock= complete;
    
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_message,snsapi_userinfo,snsapi_friend,snsapi_contact,post_timeline"; // @"sns"
    req.state = @"com.uriphium.eventshub.weichat.auth";
    

    [WXApi sendAuthReq:req viewController:vc delegate:self];
    
 
}


//WXSuccess           = 0,    /**< 成功    */
//WXErrCodeCommon     = -1,   /**< 普通错误类型    */
//WXErrCodeUserCancel = -2,   /**< 用户点击取消并返回    */
//WXErrCodeSentFail   = -3,   /**< 发送失败    */
//WXErrCodeAuthDeny   = -4,   /**< 授权失败    */
//WXErrCodeUnsupport  = -5,   /**< 微信不支持    */
-(void)WXResponse:(BaseResp*)resp{
    if([resp isKindOfClass:[SendAuthResp class]]){
        SendAuthResp *temp = (SendAuthResp*)resp;
        
        if( temp.errCode==0){
            
            _service.authCode  = temp.code;
            
            [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
                [networkManager.generalHttpRequestManager GET:@"https://api.weixin.qq.com/sns/oauth2/access_token" parameters:@{@"appid":@"wx6cc87f5cd95d2608",@"secret":@"da9e0857f9373d4fe0537ab932bf1ec5",@"code":_service.authCode,@"grant_type":@"authorization_code"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    //OAuth2Result* oauthResult = [[OAuth2Result alloc] initWithDictionary:responseObject error:nil];
                    //微信不返回json，这里暂时没用
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (operation.response.statusCode==200) {
                        
                        
                        JSONModelError*error;
                        OAuth2Result* oauthResult = [[OAuth2Result alloc] initWithData:operation.responseData error:&error];
                        
                        _service.oauthToken = oauthResult.access_token;
                        _service.refreshToken =oauthResult.refresh_token;
                        _service.expireDate = [NSDate dateWithTimeIntervalSinceNow:oauthResult.expires_in.integerValue];
                        _service.userId = oauthResult.openid;
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:ServiceInfoChangedNotification object:self.service];
                        
                        NSDictionary*info = @{@"id":oauthResult.openid,@"username":oauthResult.openid,@"login_type":kWechatAPIKey};
                        
                        _authBlock(info);
                        
                        [self refreshToken];
                        
                        
                    }else{
                        _authBlock(nil);
                        failed(error,operation.responseObject);;
                    }
                    _authBlock = nil;
                    
                }];
            }];
            
        }else if (temp.errCode==-2||temp.errCode==-4){
            _authBlock(nil);
            _authBlock = nil;
        }
        
        
    }else{
        BOOL shareFinish = YES;
        if (resp.errCode!=WXSuccess) {
            shareFinish = NO;
        }
        if (_shareBlock) {
            _shareBlock(shareFinish);
        }
    }

}

-(void)refreshToken{
    
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.generalHttpRequestManager GET:@"https://api.weixin.qq.com/sns/oauth2/refresh_token" parameters:@{@"appid":@"wx6cc87f5cd95d2608",@"code":_service.authCode,@"grant_type":@"refresh_token",@"refresh_token":_service.refreshToken} success:^(AFHTTPRequestOperation *operation, id responseObject) {
             //JSONModelError*error;
            //OAuth2Result* oauthResult = [[OAuth2Result alloc] initWithData:operation.responseData error:&error];
          
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            if (operation.response.statusCode==200) {
                
                
                JSONModelError*error;
                OAuth2Result* oauthResult = [[OAuth2Result alloc] initWithData:operation.responseData error:&error];
                
                _service.oauthToken = oauthResult.access_token;
                _service.refreshToken =oauthResult.refresh_token;
                _service.expireDate = [NSDate dateWithTimeIntervalSinceNow:oauthResult.expires_in.integerValue];
                _service.userId = oauthResult.openid;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:ServiceInfoChangedNotification object:self.service];
                
                
            }else{
            
                failed(error,operation.responseObject);;
            }
            
            
        }];
    }];
    
}
-(void)getUserInfoComplete:(GetProfileCompletedBlock)complete{

    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.generalHttpRequestManager GET:@"https://api.weixin.qq.com/sns/userinfo" parameters:@{@"access_token":_service.oauthToken,@"openid":_service.userId} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            if (operation.response.statusCode==200) {
                
                
                JSONModelError*error;
                WechatUserInfo*user = [[WechatUserInfo alloc] initWithData:operation.responseData error:&error];
            
                complete(user.toDictionary);
                

            }else{
                complete(nil);
                failed(error,operation.responseObject);;
            }
            
            
        }];
    }];
    
}



#pragma mark - WXApiDelegate

-(void) onResp:(BaseResp*)resp{
    
    [self WXResponse:resp];
}

@end