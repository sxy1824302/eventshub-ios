//
//  SocialServices.h
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TencentService.h"
#import "WeiboService.h"
#import "WechatService.h"
#import "SocialDefine.h"



typedef NS_ENUM(NSInteger, ServiceType) {
    ServiceTypeWechat,
    ServiceTypeWeibo,
    ServiceTypeTencent,
};


@interface SocialServices : NSObject
@property (nonatomic)NSArray*services;


-(id)serviceOfType:(ServiceType)type;

+ (id)sharedSocialServices;


-(void)archieveAllService;


-(void)archieveServiceForType:(ServiceType)type;

-(NSDictionary*)getAutoSocialSignedIn;

-(void)signInWithServiceInfo:(NSDictionary*)info complete:(void(^)(bool succed))complete;

-(void)tryRegisternWithService:(ServiceType)type fromViewController:(UIViewController*)vc complete:(void(^)(bool succeed))complete;

-(NSArray*)shareTitle;

@end
