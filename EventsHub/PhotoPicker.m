//
//  PhotoPicker.m
//  EventsHub
//
//  Created by 孙翔宇 on 3/18/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "PhotoPicker.h"


@implementation PhotoPicker
-(void)showOnView:(UIView*)view withMaxCount:(NSInteger)maxCount{
    
    if (nil!=NSStringFromClass([UIAlertController class])) {
        
        UIAlertController*alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"添加照片", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"从相册选择", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self showAlbumPickerWithPhotoLimit:maxCount];
            
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"拍照",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self showCamara];
            
            
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            
            [self.attatedViewCotroller dismissViewControllerAnimated:YES completion:^{
                
            }];
        }]];
        alert.popoverPresentationController.sourceView = view;
        
        alert.modalTransitionStyle=UIModalPresentationOverCurrentContext;
        
        [self.attatedViewCotroller presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        UIActionSheet*action = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"添加照片", nil) delegate:self.attatedViewCotroller cancelButtonTitle:NSLocalizedString(@"取消", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"从相册选择", nil),NSLocalizedString(@"拍照",nil), nil];
        
        [action showInView:view];
        
    }
    
    
}

-(void)showAlbumPickerWithPhotoLimit:(NSInteger)limit{
    
    if (limit==1) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        imagePickerController.delegate = self.attatedViewCotroller;
       
        imagePickerController.allowsEditing = _allowEdit;
        
        [self.attatedViewCotroller presentViewController:imagePickerController animated:YES completion:^{
            
        }];
        
    }else{
        ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
        elcPicker.maximumImagesCount = limit;
        elcPicker.imagePickerDelegate = self.attatedViewCotroller;
        elcPicker.returnsOriginalImage  = NO;
        [self.attatedViewCotroller presentViewController:elcPicker animated:YES completion:nil];
    }

    
}

-(void)showCamara{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.allowsEditing = _allowEdit;
    
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    imagePickerController.delegate = self.attatedViewCotroller;
    
    [self.attatedViewCotroller presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}
@end
