//
//  OAuth2Result.h
//  EventsHub
//
//  Created by alexander on 2/2/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "JSONModel.h"

@interface OAuth2Result : JSONModel
@property (nonatomic)NSString*access_token;
@property (nonatomic)NSNumber*expires_in;
@property (nonatomic)NSString*refresh_token;
@property (nonatomic)NSString*openid;
@property (nonatomic)NSString*scope;
@end
