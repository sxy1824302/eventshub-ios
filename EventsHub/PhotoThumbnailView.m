//
//  PhotoThumbnailView.m
//  EventsHub
//
//  Created by 孙翔宇 on 2/26/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "PhotoThumbnailView.h"
#import <MMMaterialDesignSpinner.h>
#import <UIImageView+WebCache.h>
#import "UIHelper.h"
@implementation PhotoThumbnailView{
    MMMaterialDesignSpinner*_loadingSpiner;
    
    
}

-(void)awakeFromNib{
    
  
    
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _thumbnailImageView = [[UIImageView alloc] initWithFrame:self.frame];
        _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubview:_thumbnailImageView];
        
        _loadingSpiner =[[MMMaterialDesignSpinner alloc] initWithFrame:self.frame];
        
        _loadingSpiner.hidesWhenStopped = YES;
        
        _loadingSpiner.hidden = YES;
        
        _loadingSpiner.tintColor = [UIColor whiteColor];
        
        [self addSubview:_loadingSpiner];
    }
    return self;
}
-(void)loadImageURL:(NSString*)urlString{
    
    [_thumbnailImageView sd_cancelCurrentImageLoad];
    
    _loadingSpiner.hidden = NO;
    
    [_loadingSpiner startAnimating];
    
    [_thumbnailImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"default-qt"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        [_loadingSpiner stopAnimating];
    }];
    
}
-(void)emptyLoading{
    _loadingSpiner.hidden = NO;
    
    [_thumbnailImageView setImage:[UIImage imageNamed:@"default-qt"] ];
    
    [_loadingSpiner startAnimating];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
