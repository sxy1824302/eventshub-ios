//
//  ProfileIconView.m
//  Braveaux
//
//  Created by alexander on 2/28/14.
//  Copyright (c) 2014 alexander. All rights reserved.
//

#import "ProfileIconView.h"
#import "AccountManager.h"
#import "NetworkManager.h"
#import <MMMaterialDesignSpinner.h>

@implementation ProfileIconView{
    UIBezierPath *_outerPath;
    
    UITapGestureRecognizer *_tapGRecon;
    
    NSNumber *_points;
    
    CAShapeLayer *circle ;
    
    MMMaterialDesignSpinner *_loadingView;
}



- (void)awakeFromNib{
    
    [self initializePayload];
    
    _loadingView = [[MMMaterialDesignSpinner alloc] initWithFrame:self.bounds];
    _loadingView.hidesWhenStopped = YES;
    _loadingView.tintColor = [UIColor whiteColor];
    _loadingView.lineWidth = 2;
    [self addSubview:_loadingView];
    
    _tapGRecon = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    
    [self addGestureRecognizer:_tapGRecon];
    
  
    
}

-(void)reset{
    
    self.imageView.image = [UIImage imageNamed:@"img-default-avatar"];
    
}
-(void)prepareForInterfaceBuilder{
    
    [self initializePayload];
}


- (void)initializePayload{
    
    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:_imageView];
    
    self.imageView.image = [UIImage imageNamed:@"img-default-avatar"];
    
   _imageView.backgroundColor = self.backgroundColor = [UIColor clearColor];

    CGFloat d = MIN(self.bounds.size.width, self.bounds.size.height);
    
    CGFloat outerWidth = 3;
    
    _outerPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds)) radius:d*0.5-outerWidth startAngle:0 endAngle:2*M_PI clockwise:YES];
    
    _outerPath.lineWidth = outerWidth;
    
    self.imageView.image = [UIImage imageNamed:@"img-default-avatar"];
    
  

    CAShapeLayer *mask = [CAShapeLayer layer];
    mask.path =_outerPath.CGPath;
    mask.fillColor=[UIColor whiteColor].CGColor;
    self.imageView.layer.mask = mask;
    
    
    circle = [CAShapeLayer layer];
    circle.fillColor = NULL;
    circle.path = _outerPath.CGPath;

    [self.layer addSublayer:circle];

}

- (void)handleTap:(UIGestureRecognizer*)recon{
    if (_tapCallBack) {
        _tapCallBack();
    }
}
#warning add size
-(void)loadAvatarWithURLComponents:(NSString*)path{
    
    [self loadAvatarWithURLComponents:path placeHolder:nil];
}
-(void)loadAvatarWithURLComponents:(NSString*)path placeHolder:(UIImage*)placeholder{
    
    if (!path) {
        DDLogVerbose(@"set a null profile avatar");
        return;
    }
    UIImage*ph = placeholder;
    if (ph==nil) {
        ph = [UIImage imageNamed:@"img-default-avatar"];
    }
    
    NSString* urlString = [NSString stringWithFormat:@"%@/images/%@",HostAPI,path];
    [_loadingView startAnimating];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:ph completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_loadingView stopAnimating];
    }];
    
}
-(void)setUpdating:(BOOL)updating{
    if (updating) {
        [_loadingView startAnimating];
    }else{
        [_loadingView stopAnimating];
    }
}

@end
