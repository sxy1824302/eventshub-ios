//
//  AccountManager.h
//  EventsHub
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Address.h"
#import "UserAccount.h"
#import "WeiboSDK.h"
#import "SimpleUserModel.h"

#define kUserRegisterTryExistCode 5

@interface AccountManager : NSObject


@property (nonatomic)UserAccount* userAccount;
@property(nonatomic) BOOL updating;


-(SimpleUserModel*)currenSimpleUser;

+ (id)sharedAccountManager;

-(BOOL)checkUserSigned;

-(void)userLogOut;

-(void)updateUserInfo:(NSDictionary*)info complete:(void(^)(bool succeed))complete;

-(void)updateUserAvatar:(NSData*)avatar complete:(void(^)(bool succeed))complete;

-(NSString*)currentAccountID;

-(void)updateDeviceToken:(NSString*)token;

-(void)loginWithThirdAccountID:(NSString*)accountID andThirdPartyType:(NSString*)type succeed:(void(^)(UserAccount *account))succeed;

-(void)registrationThirdPartyAccountUserInfo:(NSDictionary*)userInfo succeed:(void(^)(UserAccount *account,int code))succeed;

-(void)loginWithInfo:(NSDictionary*)info succeed:(void(^)(UserAccount *account))succeed;


-(void)getUserProfile:(NSString*)userId succeed:(void(^)(UserAccount*account))complete;

@end
