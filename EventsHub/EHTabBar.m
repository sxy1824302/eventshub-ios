//
//  EHTabBar.m
//  EventsHub
//
//  Created by alexander on 12/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EHTabBar.h"
#import "UIHelper.h"

@interface EHTabBar ()
@property (nonatomic)IBInspectable NSInteger numberOfButton;
@end


@implementation EHTabBar{
    UIBezierPath *_topShadow;
    UIBezierPath *_separator;
    

    
    NSArray *_titles;
}


-(instancetype)initWithFrame:(CGRect)frame{//IB
    
    frame = CGRectMake(frame.origin.x, frame.origin.y, [UIHelper screenWidth], GuidlineMetric(98));
    
    self = [super initWithFrame:frame];
    
    [self initializeCustomDrawingProperties];
    
    return self;
}
-(void)setNumberOfButton:(NSInteger)numberOfButton{
    _numberOfButton = numberOfButton;
    
    if(!_titles&&_buttons){
        
        [self needReloadLayoutWithTitles:@[@"按钮1",@"很长的一个按钮2啊，你觉得咋样？"]];
    }
    
        
}

-(void)prepareForInterfaceBuilder{
  
    [self initializeCustomDrawingProperties];
    
    self.numberOfButton = 1;
    
}

-(void)awakeFromNib{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, [UIHelper screenWidth], GuidlineMetric(98));
    
    
    [self initializeCustomDrawingProperties];
}

-(void)initializeCustomDrawingProperties{
    
    _buttons = [NSMutableArray array];
    self.fontSize = GuidlineMetric(32);
    self.backgroundColor = [UIHelper whiteBackgroundColor];
 
    _topShadow = [[UIBezierPath alloc] init];
    [_topShadow moveToPoint:CGPointZero];
    [_topShadow addLineToPoint:CGPointMake(self.frame.size.width, 0)];
    _topShadow.lineWidth=GuidlineMetric(2);
    
}

-(void)needReloadLayoutWithTitles:(NSArray*)titles{
    
    _titles = titles;
    
    _numberOfButton = titles.count;
    
    [_buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
   
    [_buttons removeAllObjects];
    
    UIColor *color = _warning?[UIHelper orangeColor]:[UIHelper blueThemeColor];
    if (_numberOfButton > 1) {
        
        UIButton*button1 = [UIButton buttonWithType:UIButtonTypeSystem];
        button1.translatesAutoresizingMaskIntoConstraints = NO;
        [button1 setAttributedTitle:[[NSAttributedString alloc] initWithString:titles[0] attributes:@{
                                                                                                    NSFontAttributeName:[UIFont systemFontOfSize:self.fontSize],
                                                                                                    NSForegroundColorAttributeName:color}] forState:UIControlStateNormal];
        
        [button1 setAttributedTitle:[[NSAttributedString alloc] initWithString:titles[0] attributes:@{
                                                                                                      NSFontAttributeName:[UIFont systemFontOfSize:self.fontSize],
                                                                                                      NSForegroundColorAttributeName:[UIHelper ghostColor]}] forState:UIControlStateDisabled];
        
        button1.tag = 0;
        [button1 addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button1];
        [_buttons addObject:button1];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button1 attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:-10]];
  
        _separator = [[UIBezierPath alloc] init];
        [_separator moveToPoint:CGPointMake(CGRectGetMidX(self.bounds), (GuidlineMetric(98)-GuidlineMetric(40))*0.5)];
        [_separator addLineToPoint:CGPointMake(CGRectGetMidX(self.bounds),  (GuidlineMetric(98)+GuidlineMetric(40))*0.5)];
        _separator.lineWidth=GuidlineMetric(2);
        

        UIButton*button2 = [UIButton buttonWithType:UIButtonTypeSystem];
        button2.translatesAutoresizingMaskIntoConstraints = NO;
        [button2 setAttributedTitle:[[NSAttributedString alloc] initWithString:titles[1] attributes:@{
                                                                                                    NSFontAttributeName:[UIFont systemFontOfSize:self.fontSize],
                                                                                                    NSForegroundColorAttributeName:color}] forState:UIControlStateNormal];
        [self addSubview:button2];
        [button2 addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
         button2.tag = 1;
        [_buttons addObject:button2];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button2 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button2 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button2 attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button2 attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-10]];
        
        
        
    }else if (_numberOfButton==1){
        
        _separator = nil;
        
        UIButton*button = [UIButton buttonWithType:UIButtonTypeSystem];
        
        button.translatesAutoresizingMaskIntoConstraints = NO;
        
        [button setAttributedTitle:[[NSAttributedString alloc] initWithString:titles[0] attributes:@{
                                                                                                   NSFontAttributeName:[UIFont systemFontOfSize:self.fontSize],
                                                                                                   NSForegroundColorAttributeName:color}] forState:UIControlStateNormal];
        [self addSubview:button];
        [button addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
         button.tag = 0;
        [_buttons addObject:button];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:10]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-10]];
        
        
    }
    
    
}
-(void)clicked:(UIButton*)button{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(tabBarDidClickedButtonAtIndex:)]) {
        [self.delegate tabBarDidClickedButtonAtIndex:button.tag];
    }
}

-(void)applyNewTitles:(NSArray*)titles{
    
    assert(titles.count>0&&titles.count<=2);
    
    [self needReloadLayoutWithTitles:titles];
    
}

- (void)drawRect:(CGRect)rect {
    

    [[UIHelper ghostColor] setStroke];
    [_topShadow stroke];
    
    
    [[UIHelper blueThemeColor] setStroke];
    [_separator stroke];
}


@end
