//
//  NetworkManager.h
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//


#import <AFNetworking.h>
#import "Config.h"
#import "EHHTTPSessionManager.h"
#import <UIImageView+WebCache.h>

@class NetworkManager;

typedef void(^FailedHandler)(NSError *error, NSDictionary *responseObject);

typedef void(^SuccessHandler)(void);
typedef void(^NetworkActionBlock)(NetworkManager *networkManager,SuccessHandler successed, FailedHandler failed);

#define CURRENT_LOCALE_IDENTIFIER [[NSLocale currentLocale] localeIdentifier]


#define kDefaultLocale CURRENT_LOCALE_IDENTIFIER







@interface NetworkManager : NSObject

@property (nonatomic)EHHTTPSessionManager *JSONEncodeHttpRequestManager;
@property (nonatomic)EHHTTPSessionManager *fileHttpRequestManager;

@property (nonatomic)AFHTTPSessionManager *downloadHttpRequestManager;
@property (nonatomic)AFHTTPRequestOperationManager *generalHttpRequestManager;

@property (nonatomic)AFNetworkReachabilityStatus networkStatus;


+ (id)sharedNetworkManager;

- (void)perform:(NetworkActionBlock)action;


-(void)downloadImage:(NSString*)iconURL image:(void(^)(UIImage *image))complete;
@end
