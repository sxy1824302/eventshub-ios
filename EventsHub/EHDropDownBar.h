//
//  EHDropDownBar.h
//  EventsHub
//
//  Created by alexander on 1/6/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EHDropDownBarDelegate <NSObject>

-(void)menuDidClickedButtonAtIndex:(NSInteger)index withTitle:(NSString*)title;

@end

IB_DESIGNABLE
@interface EHDropDownBar : UIView
@property (nonatomic,weak)id<EHDropDownBarDelegate>delegate;
@property (nonatomic)NSInteger fontSize;
@property(nonatomic,readonly)NSArray *titles;

-(void)applyNewTitles:(NSArray*)titles;



-(void)selectDefault;
@end
