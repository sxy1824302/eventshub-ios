//
//  WechatUserInfo.h
//  EventsHub
//
//  Created by alexander on 2/4/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "JSONModel.h"


@interface WechatUserInfo : JSONModel
@property (nonatomic)NSString*openid;
@property (nonatomic)NSString*nickname;
@property (nonatomic)NSNumber*sex;
@property (nonatomic)NSString*province;
@property (nonatomic)NSString*city;
@property (nonatomic)NSString*country;
@property (nonatomic)NSString*headimgurl;
@property (nonatomic)NSArray*privilege;
@property (nonatomic)NSString*unionid;
@end
