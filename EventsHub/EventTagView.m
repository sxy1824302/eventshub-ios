//
//  EHTagStick.m
//  EventsHub
//
//  Created by apple on 15/1/4.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import "EventTagView.h"

#import <FAKFontAwesome.h>

@implementation EventTagView{
    

    NSDictionary* _tagDic;
    TagSelectedCallBack _tagSelectedAction;
    CancelCallBack _cancelAction;
    
    
    UILabel* _cancelLabel;
}


-(instancetype)initWithTag:(Tag*)tag{
    CGRect frame = CGRectMake(0, 0,kMinimalFontSize, kDefaultTagHeight);
    self = [super initWithFrame:frame];
    if (self) {
        
        [self load];
        
        [self setEventTag:tag andCallBack:nil];
        
   
    }
    return self;
}
-(void)awakeFromNib{
    [self load];
}

-(void)load{
    self.layer.cornerRadius = 20;
 
    _tagNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 18, kDefaultTagHeight)];

    _tagNameLabel.backgroundColor = [UIColor clearColor];
    _tagNameLabel.textAlignment = NSTextAlignmentCenter;
    
    
    _cancelLabel = [[UILabel alloc] init];
    FAKFontAwesome*as = [FAKFontAwesome timesIconWithSize:GuidlineMetric(32)];
    [as addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    _cancelLabel.attributedText = as.attributedString;
    _cancelLabel.backgroundColor = [UIColor clearColor];
    _cancelLabel.textAlignment = NSTextAlignmentCenter;
    [_cancelLabel sizeToFit];
    [self addSubview:_cancelLabel];
    
    _cancelLabel.hidden = YES;
    
    [self addSubview:_tagNameLabel];
    
   
}

-(void)layoutSubviews{
    
    [super layoutSubviews];

    _tagNameLabel.bounds = CGRectMake(0, 0, _tagNameLabel.attributedText.size.width, kDefaultTagHeight);
    
    
    if (!_cancelLabel.hidden) {
        
        _tagNameLabel.center = CGPointMake(CGRectGetMidX(self.bounds)-6, CGRectGetMidY(self.bounds));
        
        _cancelLabel.center = CGPointMake(CGRectGetMaxX(self.bounds)-GuidlineMetric(32), CGRectGetMidY(self.bounds));

    }else{
        
        _tagNameLabel.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
        
    }
}

-(void)showCancel{
    self.tagNameLabel.textAlignment = NSTextAlignmentLeft;
    _cancelLabel.hidden = NO;
    [self setNeedsLayout];
}


-(UIColor*)tagColorWith:(NSString*)tagColorKey {
    NSDictionary* colorsDic = @{@"ms":@"ffb245",
                                @"yy":@"3aa3dc",
                                @"dy":@"4dacff",
                                @"ly":@"f5ba46",
                                @"ds":@"33cb98",
                                @"sy":@"98bd5d",
                                @"yd":@"dad710",
                                @"pd":@"e762df",
                                @"xx":@"b4c36e",
                                @"xj":@"fe0000",
                                @"jy":@"eedd60",
                                @"wy":@"93b9c4",
                                @"jz":@"cf7733",
                                @"zw":@"0fa15b",
                                @"dw":@"b48444",
                                @"qt":@"999999"
                                };
    NSString* colorString = [NSString stringWithFormat:@"#%@",[colorsDic objectForKey:tagColorKey]];
    return [UIColor colorFromHexString:colorString];
}


-(void)setEventTag:(Tag *)eventTag andCallBack:(TagSelectedCallBack)action{
    _eventTag = eventTag;

    _tagNameLabel.attributedText = [EventTagView attributeName:_eventTag.tag];
    self.backgroundColor = [self tagColorWith:_eventTag.pinyin];
    _tagSelectedAction = action;
    
    self.frame = CGRectMake(0, 0,16*2+self.tagNameLabel.attributedText.size.width, kDefaultTagHeight);
}

+(NSAttributedString*)attributeName:(NSString*)name{
    return [[NSAttributedString alloc] initWithString:name attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIHelper defaultFontWithSize:kDefaultTagFontSize]}];
}


//显示取消按钮
-(void)tagStickWithCancel:(CancelCallBack)action{
    CGRect frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width+10, self.frame.size.height);
    [self setFrame:frame];
    
    [self showCancel];
    _cancelAction = action;
}

#pragma mark - Touch

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    _tagNameLabel.textColor = [UIColor darkGrayColor];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    _tagNameLabel.textColor = [UIColor whiteColor];
    if (_tagSelectedAction) {
        _tagSelectedAction(_tagDic);
    }
    if (_cancelAction) {
        _cancelAction();
    }
}

@end
