//
//  EventPostersTableViewCell.m
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventPostersTableViewCell.h"
#import <UIButton+WebCache.h>
#import "NetworkManager.h"
#import "PhotoThumbnailView.h"
#import "UIHelper.h"
@implementation EventPostersTableViewCell{
    NSMutableArray*_imageButtons;
}
- (void)addPhotos:(id)sender {
    [_delegate addPhotoClickedWithButton:sender];
}
- (void)awakeFromNib {
    
    self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addButton setImage:[UIImage imageNamed:@"btn-create-addphoto-normal"] forState:UIControlStateNormal];
    [self.addButton setImage:[UIImage imageNamed:@"btn-create-addphoto-pressed"] forState:UIControlStateHighlighted];
    [self.addButton addTarget:self action:@selector(addPhotos:) forControlEvents:UIControlEventTouchUpInside];
    [_addButton sizeToFit];
    
    [self.contentView addSubview:_addButton];
    
    
    _imageButtons =[NSMutableArray array];
    
    
    for (int i = 0; i<3; i++) {
        PhotoThumbnailView*button = [[PhotoThumbnailView alloc] initWithFrame: _addButton.bounds];
        
        [button addTarget:self action:@selector(viewImage:) forControlEvents:UIControlEventTouchUpInside];
   
        button.autoresizingMask = _addButton.autoresizingMask;
        
        button.tag = i;
        
        [_imageButtons addObject:button];
    }
}
-(void)viewImage:(UIButton*)btn{
    [_delegate imageSelectedAtIndex:btn.tag];
}
-(NSArray*)displayedImages{
    NSMutableArray*array =[NSMutableArray array];
    
    [_imageButtons enumerateObjectsUsingBlock:^(PhotoThumbnailView *obj, NSUInteger idx, BOOL *stop) {
       
        if (obj.superview) {
            
            [array addObject:obj.thumbnailImageView.image];
        }
        
    }];
    
    return array;
}
-(void)reloadPhotoViewsWithImageAndURL:(NSArray*)posters{
    
    CGFloat margin = 16;
    
    __block CGFloat width = 0;
        DDLogVerbose(@"loading %@",posters);
    [_imageButtons enumerateObjectsUsingBlock:^(PhotoThumbnailView* btn, NSUInteger idx, BOOL *stop) {
       
        if (idx<posters.count) {
            
             btn.hidden =NO;
            
            id obj = posters[idx];
            
            if([obj isKindOfClass:[UIImage class]]){
                
                [btn.thumbnailImageView setImage:obj];
                
                
            }else if ([obj isKindOfClass:[NSString class]]){
                
                NSString* urlString = [NSString stringWithFormat:@"%@/thumbnails/%@/%i.jpg",HostAPI,obj,(int)(btn.frame.size.width*[UIHelper ImageThumbnailScale])];
                
                [btn loadImageURL:urlString];
                
            }else if ([obj isKindOfClass:[NSNull class]]){
                
                [btn emptyLoading];
                
            }
            
            if (btn.superview) {
                [btn removeFromSuperview];
            }
            
            btn.center =  CGPointMake(margin+CGRectGetWidth(btn.frame)*.5+(CGRectGetWidth(btn.frame)+margin)*idx,  CGRectGetHeight(self.contentView.frame)-(CGRectGetHeight(btn.frame)*0.5+margin));
            
            [self.contentView addSubview:btn];
            
            
            width =btn.frame.size.width;
            
        }else{
            
            btn.hidden =YES;
        }
        
    }];
    

    
    if (posters.count>=3||!self.isMine) {
        
        _addButton.hidden = YES;
        
    }else{
        _addButton.hidden = NO;
        
        _addButton.center =  CGPointMake(margin+(_addButton.frame.size.width)*.5+(width+margin)*posters.count,  CGRectGetHeight(self.contentView.frame)-(CGRectGetHeight(_addButton.frame)*0.5+margin));
        
        
    }
    
}



@end
