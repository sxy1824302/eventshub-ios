//
//  UserProfileInfoCell.m
//  EventsHub
//
//  Created by alexander on 2/5/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "UserProfileInfoCell.h"
#import "UIHelper.h"
@implementation UserProfileInfoCell
-(void)awakeFromNib{
    _seperatorLine.backgroundColor = [UIHelper ghostColor];
    
    self.inputLabel.userInteractionEnabled = NO;
    
    self.inputLabel.floatingLabel.font = [UIHelper defaultFontWithSize:kMinimalFontSize];
    
    
    self.inputLabel.font = [UIHelper defaultFontWithSize:16];
}
@end
