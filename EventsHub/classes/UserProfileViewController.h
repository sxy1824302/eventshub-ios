//
//  UserProfileViewController.h
//  EventsHub
//
//  Created by alexander on 2/5/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseUIViewController.h"

#import "ProfileConfig.h"


@interface UserProfileViewController :BaseCollectionViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
