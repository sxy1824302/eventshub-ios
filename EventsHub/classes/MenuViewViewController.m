//
//  MenuViewViewController.m
//  EventsHub
//
//  Created by 亮 姚 on 14-11-12.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "MenuViewViewController.h"
#import "HomeViewController.h"
#import "AccountManager.h"
#import "EventHubRootViewController.h"
#import "NetworkManager.h"
#import "UIHelper.h"
#import "MenuTableViewCell.h"
#import "ProfileMenuTableViewCell.h"
#import "LocationManager.h"
#import "UIHelper.h"
#import "MessageCenter.h"
@interface MenuViewViewController ()
@property (weak, nonatomic) IBOutlet UIButton *settingButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *cityButton;

@end

@implementation MenuViewViewController{
    NSArray* _menuArray;
    
    NSArray *_icons;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    self.tableView.tableFooterView = [UIView new];
    
    FAKFontAwesome *homea = [FAKFontAwesome homeIconWithSize:20];
    [homea addAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIImage*home = [homea imageWithSize:kIconSizeNormal];
 
    FAKFontAwesome *minea = [FAKFontAwesome childIconWithSize:20];
    [minea addAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIImage*mine = [minea imageWithSize:kIconSizeNormal];
   
    FAKFontAwesome *creata = [FAKFontAwesome plusCircleIconWithSize:20];
    [creata addAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIImage*create = [creata imageWithSize:kIconSizeNormal];

    _menuArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"主页", nil),NSLocalizedString(@"我的",nil),NSLocalizedString(@"创建",nil), nil];
    _icons = @[home,mine,create];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:User_LoginNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self reloadMenu];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:User_InfoUpdatedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self reloadMenu];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:User_LogoutNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self reloadMenu];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:EventCityUpdatedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {

        [self reloadLocation];
    }];
    
    
    
    FAKFontAwesome *geara = [FAKFontAwesome gearIconWithSize:kIconFontSize];
    [geara addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    [_settingButton setAttributedTitle:geara.attributedString forState:UIControlStateNormal];
 
    
    [self reloadLocation];
    
    [self.tableView setSeparatorColor:[[UIHelper darkColor] colorWithAlphaComponent:0.2]];
}

-(void)reloadLocation{
    FAKFontAwesome *loca = [FAKFontAwesome mapMarkerIconWithSize:kIconFontSize];
    [loca addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    
    NSMutableAttributedString*s = [[NSMutableAttributedString alloc] initWithAttributedString:loca.attributedString];
    
    NSString*locationName = [[LocationManager sharedLocationManager] eventCity];
    
    if (locationName.length<1) {
         locationName = NSLocalizedString(@"地球上", nil);
    }
    
    [s appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",locationName] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIHelper defaultFontWithSize:kIconFontSize]}]];
    
    [_cityButton setAttributedTitle:s forState:UIControlStateNormal];
 
}

-(void)reloadMenu{
    [self.tableView reloadData];
}

- (IBAction)showSettingAction:(id)sender {
    [self performSegueWithIdentifier:@"SettingsSegue" sender:self];
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }
    return [_menuArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    UITableViewCell*acell;
    if (indexPath.section==0) {
        ProfileMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileMenuCell"];;
        if (![[AccountManager sharedAccountManager] checkUserSigned]) {
            
            cell.titleLabel.text = NSLocalizedString(@"注册登录", nil);
            [cell.iconView reset];
        }else{
            
            NSString*name= [[[AccountManager sharedAccountManager] userAccount] username]?:[[[AccountManager sharedAccountManager] userAccount] _id];
            
            cell.titleLabel.text = name;
            cell.iconView.userInteractionEnabled = NO;
            [cell.iconView loadAvatarWithURLComponents:[[[AccountManager sharedAccountManager] userAccount] avatar_path]];
        }
        
        acell = cell;
    }else{
        MenuTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
        
        cell.titleLabel.text = [_menuArray objectAtIndex:indexPath.row];
        
        cell.iconView.image = _icons[indexPath.row];
        
        acell = cell;
    }
    
    return acell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 80;
    }
    return 55;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];//fucking ios reset problem
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    if (indexPath.section == 0) {
        
        if ([[AccountManager sharedAccountManager] checkUserSigned]) {
            
            [self performSegueWithIdentifier:@"ProfileSegue" sender:self];
            
        }else{
            
             [self performSegueWithIdentifier:@"AuthNeedSegue" sender:self];
        }
    }else{
        
        if (indexPath.row==0) {
              [self performSegueWithIdentifier:@"MainPageSegue" sender:self];
        }
        else if (![[AccountManager sharedAccountManager] checkUserSigned]) {
            [self performSegueWithIdentifier:@"AuthNeedSegue" sender:self];
            return;
        }else{
            switch (indexPath.row) {
                case 1:
                    [self performSegueWithIdentifier:@"MyEventsSegue" sender:self];
                    break;
                case 2:
                    if ([[LocationManager sharedLocationManager] currentUserLocation]) {
                        [self performSegueWithIdentifier:@"CreateEventSegue" sender:self];
                    }else{
                        [MessageCenter showMessage:NSLocalizedString(@"创建活动需要开定位功能", nil) withType:MessageTypeError inViewController:self.sideMenuViewController];
                        [tableView deselectRowAtIndexPath:indexPath animated:YES];
                        return;
                    }
                    break;
                    
                default:
                    break;
                    
            }
            [self.sideMenuViewController hideMenuViewController];
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }

    }
}
-(void)showCreatePage{
    [self performSegueWithIdentifier:@"CreateEventSegue" sender:self];
}
-(void)showMainPage{
    [self performSegueWithIdentifier:@"MainPageSegue" sender:self];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
       [self performSegueWithIdentifier:@"SignInMenuSegue" sender:self];
    }
}



@end



@implementation ProfileMenuSegue

- (void)perform{
    
    id controller =  [[(UIViewController*)self.sourceViewController storyboard] instantiateViewControllerWithIdentifier:@"ProfileNavi"];
    
    [[(UIViewController*)self.sourceViewController sideMenuViewController] setContentViewController:controller animated:YES];
      [[(UIViewController*)self.sourceViewController sideMenuViewController] hideMenuViewController];
}


@end
@implementation AuthNeedSegue

- (void)perform{
    UINavigationController*nvi = (UINavigationController*)[[(UIViewController*)self.sourceViewController sideMenuViewController] contentViewController];
    HomeViewController* controller ;
    if (nvi&&[nvi.viewControllers[0] isKindOfClass:[HomeViewController class]]) {
        controller =nvi.viewControllers[0] ;
    }else{
        UINavigationController* mainNavi = [[(UIViewController*)self.sourceViewController storyboard] instantiateViewControllerWithIdentifier:@"MainNavi"];
        controller = mainNavi.viewControllers[0];

        [[(UIViewController*)self.sourceViewController sideMenuViewController] setContentViewController:mainNavi animated:YES];
    }
    
    controller.needAuth = YES;
    
    [[(UIViewController*)self.sourceViewController sideMenuViewController] hideMenuViewController];
    
    [controller viewDidAppear:YES];
}


@end
@implementation MainPageSegue

- (void)perform{
    
    id controller =  [[(UIViewController*)self.sourceViewController storyboard] instantiateViewControllerWithIdentifier:@"MainNavi"];
    
    [[(UIViewController*)self.sourceViewController sideMenuViewController] setContentViewController:controller animated:YES];
      [[(UIViewController*)self.sourceViewController sideMenuViewController] hideMenuViewController];
}


@end

@implementation SettingsSegue

- (void)perform{
    
    id controller =  [[(UIViewController*)self.sourceViewController storyboard] instantiateViewControllerWithIdentifier:@"SettingsNavi"];
    
    [[(UIViewController*)self.sourceViewController sideMenuViewController] setContentViewController:controller animated:YES];
    [[(UIViewController*)self.sourceViewController sideMenuViewController] hideMenuViewController];
}


@end

@implementation CreateEventSegue

- (void)perform{
    
    id controller =  [[(UIViewController*)self.sourceViewController storyboard] instantiateViewControllerWithIdentifier:@"CreateEventNavi"];
    
    [[(UIViewController*)self.sourceViewController sideMenuViewController] setContentViewController:controller animated:YES];
    [[(UIViewController*)self.sourceViewController sideMenuViewController] hideMenuViewController];
}


@end

@implementation MyEventsSegue

- (void)perform{
    
    id controller =  [[(UIViewController*)self.sourceViewController storyboard] instantiateViewControllerWithIdentifier:@"MyEventsNavi"];
    
    [[(UIViewController*)self.sourceViewController sideMenuViewController] setContentViewController:controller animated:YES];
    [[(UIViewController*)self.sourceViewController sideMenuViewController] hideMenuViewController];
}

@end






