//
//  OtherProfileViewController.h
//  EventsHub
//
//  Created by alexander on 2/5/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "BaseUIViewController.h"
#import "ProfileConfig.h"


@class UserAccount;


@interface OtherProfileViewController : BaseCollectionViewController
@property (nonatomic)UserAccount *account;
@property (nonatomic)NSString*userID;
@end
