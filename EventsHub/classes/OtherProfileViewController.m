//
//  OtherProfileViewController.m
//  EventsHub
//
//  Created by alexander on 2/5/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "OtherProfileViewController.h"
#import "AccountManager.h"
#import <CSStickyHeaderFlowLayout.h>
#import "UserProfileInfoCell.h"
#import <FAKFontAwesome.h>
#import "NetworkManager.h"
#import "ProfileIconView.h"
#import "MessageCenter.h"
#import "CircleButton.h"
#import "UIHelper.h"
#import "ProfileHeaderView.h"
#import <RESideMenu.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "UserAccount.h"
#import "UserEventsViewController.h"
@interface OtherProfileViewController ()<UICollectionViewDelegateFlowLayout>{
      
    ProfileHeaderView*_profileHeaderView;
}

@end

@implementation OtherProfileViewController

-(void)reloadUI{
    NSString* avatar_path = _account.avatar_path;
    
    [_profileHeaderView.profileIconView loadAvatarWithURLComponents:avatar_path];
    
    self.navigationItem.title =  _account.username;
    
    [self.collectionView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if (!_account) {
        [[AccountManager sharedAccountManager] getUserProfile:_userID succeed:^(UserAccount *account) {
            _account = account;
            
            [self reloadUI];
        }];
        
    }
    
    [self reloadUI];
    
    self.navigationItem.title = NSLocalizedString(@"GetUserInfo", nil);
    
    
    CSStickyHeaderFlowLayout *layout = (id)self.collectionViewLayout;
    
    CGFloat headerSize = 203;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(CGRectGetWidth(self.view.frame), headerSize);
        
        // Setting the minimum size equal to the reference size results
        // in disabled parallax effect and pushes up while scrolls
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(CGRectGetWidth(self.view.frame), headerSize);
    }
    
    // Also insets the scroll indicator so it appears below the search bar
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(headerSize, 0, 0, 0);
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ProfileHeaderView" bundle:nil]forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader withReuseIdentifier:@"header"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return OtherUserFieldTagCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UserProfileInfoCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InfoCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case OtherUserFieldTagName:
            [cell.inputLabel setPlaceholder:NSLocalizedString(@"昵称", nil) floatingTitle:NSLocalizedString(@"昵称", nil)];
            cell.inputLabel.text = _account.username;
        
            break;
//        case OtherUserFieldTagPhone:
//            [cell.inputLabel setPlaceholder:NSLocalizedString(@"电话号码", nil) floatingTitle:NSLocalizedString(@"电话号码", nil)];
//     
//            if (!_account.phone||[_account.phone length]<1) {
//                cell.inputLabel.text = NSLocalizedString(@"未设置", nil);
//            }else{
//                cell.inputLabel.text = _account.phone;
//            }
//            
//            break;
//        case OtherUserFieldTagEmail:
//            [cell.inputLabel setPlaceholder:NSLocalizedString(@"Email", nil) floatingTitle:NSLocalizedString(@"Email", nil)];
//            cell.inputLabel.tag = OtherUserFieldTagEmail;
//            if (!_account.email||[_account.email length]<1) {
//                cell.inputLabel.text = NSLocalizedString(@"未设置", nil);
//            }else{
//                cell.inputLabel.text = _account.email;
//            }
//            
//            break;
        case OtherUserFieldTagCreatedEventsCount:
            [cell.inputLabel setPlaceholder:NSLocalizedString(@"举办过", nil) floatingTitle:NSLocalizedString(@"举办过", nil)];
      
            cell.inputLabel.text = [NSString stringWithFormat:@"%i 次活动",[_account.created_events_count integerValue]];
            
            break;
        default:
            break;
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==OtherUserFieldTagCreatedEventsCount) {
        [self performSegueWithIdentifier:@"UserEventsSegue" sender:self];
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewFlowLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.view.frame.size.width, collectionViewLayout.itemSize.height);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        UICollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                            withReuseIdentifier:@"header"
                                                                                   forIndexPath:indexPath];
        _profileHeaderView = (ProfileHeaderView*)cell;
        
        return cell;
    }
    return nil;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"UserEventsSegue"]) {
        
        UserEventsViewController*rvc =segue.destinationViewController;
        
        rvc.user = _account;
        
    }
    
}

@end
