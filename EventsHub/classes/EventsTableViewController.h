//
//  EventsTableViewController.h
//  EventsHub
//
//  Created by alexander on 2/13/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "BaseUIViewController.h"

@class Event;

@protocol EventsTableViewControllerDelegate <NSObject>


-(void)didSelectEvent:(Event*)event;

-(void)shouldGotoCreateEvent;

@end

@interface EventsTableViewController : BaseTableViewController

@property (nonatomic,weak)id<EventsTableViewControllerDelegate>eventDelegate;



@property (nonatomic) NSDate* fromIndexDate;

@property (nonatomic) NSDate* startDate;
@property (nonatomic) NSDate* endDate;


-(void)loadEventsFrom:(NSDate*)from end:(NSDate*)end;
@end
