//
//  ProfileMenuTableViewCell.h
//  EventsHub
//
//  Created by alexander on 1/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileIconView.h"
@interface ProfileMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ProfileIconView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
