//
//  POIViewController.h
//  EventsHub
//
//  Created by 亮 姚 on 14-12-12.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"
#import "BMKPoiSearch.h"

@class Address;

typedef void(^SelectedBloack)(Address *address, NSString*poi);

@class  CreateActivityViewController;


@interface POIViewController : UIViewController<BMKPoiSearchDelegate,BMKGeoCodeSearchDelegate,BMKMapViewDelegate,BMKLocationServiceDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>



@property (weak, nonatomic) IBOutlet BMKMapView *mapView;

@property (weak, nonatomic) IBOutlet UISearchBar *POISearchBar;



@property (nonatomic,strong)SelectedBloack selectedBlock;

@end
