//
//  EventDetailViewController.h
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EHTabBar.h"
#import <MBProgressHUD.h>
#import "BaseUIViewController.h"
#import "EventDetailDataSource.h"


@class Event;



@interface EventDetailViewController : BaseUIViewController<EHTabBarTappedDelegate,UIActionSheetDelegate,PosterImageOperationDelegate>
@property(nonatomic,weak) EventPostersTableViewCell *posterCell;
@property(nonatomic)EventDetailDataSource*dataSource;
@property (weak, nonatomic) IBOutlet UITableView *tableview;


-(void)gotoComments;
@end
