//
//  BOOLTableViewCell.m
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "BOOLTableViewCell.h"
#import "UIHelper.h"
@implementation BOOLTableViewCell

- (void)awakeFromNib {
    self.nameLabel.font = [UIHelper defaultFontWithSize:GuidlineMetric(32)];
    self.nameLabel.textColor = [UIHelper stoneColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)change:(id)sender {
    
    [_delegate cell:self didChangedValue:@(self.swich.on)];
}

@end
