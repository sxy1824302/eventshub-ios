//
//  CreateNormalTableViewCell.h
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^TextChangeBlock)(id observer, id object, NSDictionary *change);

#import "ValueChangedDelegate.h"
@interface CreateNormalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic,weak)id<ValueChangedDelegate>delegate;

-(void)setReuiredName:(NSString*)name;

-(void)overSeeTextChangeWitch:(TextChangeBlock*)changeBlock;
@end
