//
//  UserEventsViewController.h
//  EventsHub
//
//  Created by alexander on 3/23/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "BaseUIViewController.h"
#import "UserAccount.h"
@interface UserEventsViewController : BaseTableViewController
@property (nonatomic,weak)UserAccount*user;
@end
