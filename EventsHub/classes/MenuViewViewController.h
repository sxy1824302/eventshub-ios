//
//  MenuViewViewController.h
//  EventsHub
//
//  Created by 亮 姚 on 14-11-12.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"


@interface MenuViewViewController : UIViewController<RESideMenuDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

-(void)showMainPage;
-(void)showCreatePage;
@end


@interface ProfileMenuSegue : UIStoryboardSegue

@end


@interface MainPageSegue : UIStoryboardSegue

@end


@interface SettingsSegue : UIStoryboardSegue

@end

@interface CreateEventSegue : UIStoryboardSegue

@end

@interface MyEventsSegue : UIStoryboardSegue

@end
@interface AuthNeedSegue : UIStoryboardSegue

@end





