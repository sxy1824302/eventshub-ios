//
//  LocationTableViewCell.h
//  EventsHub
//
//  Created by alexander on 3/3/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JVFloatLabeledTextField.h>
@interface LocationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end
