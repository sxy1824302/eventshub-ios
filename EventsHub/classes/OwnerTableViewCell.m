//
//  OwnerTableViewCell.m
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "OwnerTableViewCell.h"
#import "UIHelper.h"
@implementation OwnerTableViewCell

- (void)awakeFromNib {
    self.floatingLabel.userInteractionEnabled = NO;
    
    self.floatingLabel.floatingLabel.font = [UIHelper defaultFontWithSize:kMinimalFontSize];
    
    
    self.floatingLabel.font = [UIHelper defaultFontWithSize:16];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
