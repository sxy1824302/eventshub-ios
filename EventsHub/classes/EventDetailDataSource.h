//
//  EventDetailDataSource.h
//  EventsHub
//
//  Created by alexander on 4/2/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OwnerTableViewCell.h"
#import "EventInfoTableViewCell.h"
#import "EventPostersTableViewCell.h"
#import "LocationTableViewCell.h"
#import "UIHelper.h"

@import UIKit;
@class Event,EventDetailViewController;

typedef NS_ENUM(NSInteger, CellTag) {
    CellTagAddress,
    CellTagOwner,
    CellTagParticipants,
    CellTagEventPoster,
    CellTagDetail,
    CellTagCount,
};

@interface EventDetailDataSource : NSObject<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic)BOOL liked;

@property (nonatomic,weak)EventDetailViewController*viewController;

@property (nonatomic)Event*event;


-(void)reloadComments;


- (instancetype)initWithViewController:(id)vc;

-(void)initializeData;

@end
