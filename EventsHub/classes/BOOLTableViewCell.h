//
//  BOOLTableViewCell.h
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ValueChangedDelegate.h"



@interface BOOLTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UISwitch *swich;
@property (nonatomic,weak)id<ValueChangedDelegate>delegate;


@end
