//
//  EventPostersTableViewCell.h
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PosterImageOperationDelegate <NSObject>
- (void)addPhotoClickedWithButton:(UIButton*)btn;
- (void)imageSelectedAtIndex:(NSInteger)idx;
@end

typedef void(^ShowImageCompleted)(NSArray* imageArray);

@interface EventPostersTableViewCell : UITableViewCell
@property (nonatomic)UIButton*addButton;
@property (nonatomic,weak)id<PosterImageOperationDelegate> delegate;

@property (nonatomic)BOOL isMine;

- (NSArray*)displayedImages;

-(void)reloadPhotoViewsWithImageAndURL:(NSArray*)posters;

@end
