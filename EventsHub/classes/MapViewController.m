//
//  MapViewController.m
//  EventsHub
//
//  Created by alexander on 3/3/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "MapViewController.h"
#import "BMKMapView.h"
#import "BMKPointAnnotation.h"
#import "Event.h"
#import "BMKPinAnnotationView.h"
#import "BMKGeometry.h"
#import "LocationManager.h"
@interface MapViewController ()<BMKMapViewDelegate,BMKLocationServiceDelegate>{
    BMKLocationService*_locService;
    BOOL _changedRegion;
}
@property (weak, nonatomic) IBOutlet BMKMapView *mapView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"活动位置", nil);
    
    _mapView.frame = CGRectMake(0,0, self.view.frame.size.width,  _mapView.frame.size.height);//baidu bug
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
    _mapView.isSelectedAnnotationViewFront = YES;
    [_mapView setZoomLevel:13];
    
    _locService = [[BMKLocationService alloc]init];
    _locService.delegate = self;
    [_locService startUserLocationService];
    
    
    
    BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
    CLLocationCoordinate2D coor;
    coor.latitude = _event.address.geopoint.latitude.doubleValue;
    coor.longitude = _event.address.geopoint.longitude.doubleValue;
    annotation.coordinate = coor;
    annotation.title = _event.pio;
    [_mapView addAnnotation:annotation];
    

    
    BMKCoordinateRegion viewRegion = BMKCoordinateRegionMake(coor, BMKCoordinateSpanMake(0.02f,0.02f));
    BMKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
    [_mapView setRegion:adjustedRegion animated:YES];
    

    
    // Do any additional setup after loading the view.
  
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mapView viewWillAppear];
    [_locService startUserLocationService];
    _mapView.delegate = self; 
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_mapView viewWillDisappear];
    [_locService stopUserLocationService];
    _mapView.delegate = nil;
}
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
   [_mapView updateLocationData:userLocation];
    
    if (!_changedRegion) {
         Location*l = [Location new];
        l.longitude = @(userLocation.location.coordinate.longitude);
        l.latitude = @(userLocation.location.coordinate.latitude);
        
        [self focus:@[_event.address.geopoint,l]];
        
    }
    
    
}

-(void)focus:(NSArray*)locations{
    __block CLLocationDegrees minLat;
    __block CLLocationDegrees maxLat;
    __block CLLocationDegrees minLon;
    __block CLLocationDegrees maxLon;
    
    [locations enumerateObjectsUsingBlock:^(Location* obj, NSUInteger idx, BOOL *stop) {
        if (idx==0) {
       
            minLat = obj.latitude.doubleValue;
            maxLat = obj.latitude.doubleValue;
            minLon = obj.longitude.doubleValue;
            maxLon = obj.longitude.doubleValue;
        }else{
      
            minLat = MIN(minLat, obj.latitude.doubleValue);
            maxLat = MAX(maxLat, obj.latitude.doubleValue);
            minLon = MIN(minLon, obj.longitude.doubleValue);
            maxLon = MAX(maxLon, obj.longitude.doubleValue);
        }
    }];
    
    

        CLLocationCoordinate2D centCoor;
        centCoor.latitude = (CLLocationDegrees)((maxLat+minLat) * 0.5f);
        centCoor.longitude = (CLLocationDegrees)((maxLon+minLon) * 0.5f);
        BMKCoordinateSpan span;
 
        span.latitudeDelta = maxLat - minLat;
        span.longitudeDelta = maxLon - minLon;
    
        BMKCoordinateRegion region = BMKCoordinateRegionMake(centCoor, span);
  
        CGRect fitRect = [_mapView convertRegion:region toRectToView:_mapView];
 
        BMKMapRect fitMapRect = [_mapView convertRect:fitRect toMapRectFromView:_mapView];
  
        [_mapView setVisibleMapRect:fitMapRect animated:YES];
    
}

    
// Override
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
        newAnnotationView.pinColor = BMKPinAnnotationColorPurple;
        newAnnotationView.animatesDrop = YES;// 设置该标注点动画显示
   
        return newAnnotationView;
    }
    return nil;
}
- (void)mapView:(BMKMapView *)mapView didAddAnnotationViews:(NSArray *)views{
    [views enumerateObjectsUsingBlock:^(id annotation, NSUInteger idx, BOOL *stop) {
        if ([annotation isKindOfClass:[BMKPinAnnotationView class]]) {
            BMKPinAnnotationView *newAnnotationView =annotation;
   
            [newAnnotationView setSelected:YES animated:YES];
        
        }
    }];

}
-(void)mapViewDidFinishLoading:(BMKMapView *)mapView{
    
}


-(void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
  
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
