//
//  UserProfileInfoCell.h
//  EventsHub
//
//  Created by alexander on 2/5/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JVFloatLabeledTextField.h>
@interface UserProfileInfoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *inputLabel;

@property (weak, nonatomic) IBOutlet UIView *seperatorLine;
@end
