//
//  UserEventsViewController.m
//  EventsHub
//
//  Created by alexander on 3/23/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "UserEventsViewController.h"
#import <MBProgressHUD.h>
#import "EventTableViewCell.h"
#import "EventDetailViewController.h"
#import "EventsManager.h"
#import "UserAccount.h"
@interface UserEventsViewController (){
    NSArray*_userEvents;
    
    Event*_tempEvent;
}

@end

@implementation UserEventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@的活动",_user.username];
    
    self.tableView.tableFooterView = [UIView new];
    
    [self reloadEvents];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)reloadEvents{

    MBProgressHUD*hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"LookingForEvents", nil);
    
    [[EventsManager sharedEventsManager] getUserEvents:_user._id complete:^(NSArray *events, BOOL success) {
        
        _userEvents = events;
        
        [self.tableView reloadData];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];

    
    
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_userEvents count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EventTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"EventsTableViewCell"];
    
    Event* showEvent = [_userEvents objectAtIndex:indexPath.row];
    cell.event = showEvent;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _tempEvent= [_userEvents objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"DetailSegue" sender:self];
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"DetailSegue"]) {
        EventDetailViewController*dvc = segue.destinationViewController;
        dvc.dataSource.event = _tempEvent;
    }
}


@end
