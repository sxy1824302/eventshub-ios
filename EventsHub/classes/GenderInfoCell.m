//
//  GenderInfoCell.m
//  EventsHub
//
//  Created by alexander on 4/1/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "GenderInfoCell.h"

@implementation GenderInfoCell
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.genderSelection.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.genderSelection addTarget:self action:@selector(changeGender:) forControlEvents:UIControlEventValueChanged];
}

-(void)changeGender:(UISegmentedControl*)seg{
    
    switch (seg.selectedSegmentIndex) {
        case 0:
            self.gender = @"M";
            break;
        case 1:
            self.gender = @"F";
            break;
        case 2:
            self.gender = @"U";
            break;
        default:
            break;
    }
    
    [self.delgeate valueDidChanged:seg.selectedSegmentIndex ofCell:self];
}

-(void)setGender:(NSString *)gender{
    _gender = gender;
    
    if ([gender isEqualToString:@"M"]) {
        self.inputLabel.text = NSLocalizedString(@"男", nil);
        self.genderSelection.selectedSegmentIndex = 0;
    }else if ([gender isEqualToString:@"F"]) {
        self.genderSelection.selectedSegmentIndex = 1;
        self.inputLabel.text = NSLocalizedString(@"女", nil);
    }else{
        self.genderSelection.selectedSegmentIndex = 2;
        self.inputLabel.text = NSLocalizedString(@"秘密", nil);
    }
}
@end
