//
//  CreateEventTableViewController.h
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseUIViewController.h"
#import "EHTabBar.h"


typedef NS_ENUM(NSInteger, EventEditingMode) {
    EventEditingModeCreateNew,
    EventEditingModeEdit,
    EventEditingModeOldCreateNew,
};

@class Event;
@interface CreateEventTableViewController : BaseUIViewController<UIActionSheetDelegate>
@property (nonatomic)EventEditingMode editingMode;
@property (weak, nonatomic) IBOutlet EHTabBar *tabbar;
-(void)loadViewControllerWithEvent:(Event*)showEvent;

@end
