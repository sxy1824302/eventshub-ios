//
//  OwnerTableViewCell.h
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface OwnerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *floatingLabel;

@end
