//
//  HomeViewController.m
//  EventsHub
//
//  Created by 亮 姚 on 14-11-12.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "HomeViewController.h"
#import "EventTableViewCell.h"
#import "EventDetailViewController.h"
#import "RecomendationView.h"
#import "AccountManager.h"
#import "EventsManager.h"
#import "NSDate+Helpers.h"
#import "Event+Display.h"
#import <iCarousel.h>
#import "LocationManager.h"
#import <FAKFontAwesome.h>
#import "UIHelper.h"
#import "QuickSignInViewController.h"
#import "MessageCenter.h"
#import "EventsTableViewController.h"
#import "MenuViewViewController.h"
typedef NS_ENUM(NSInteger, RangeOfDate){
    All = 0,
    ThisWeek,
    NextWeek,
    NextMonth
};



@interface HomeViewController ()<iCarouselDelegate,iCarouselDataSource,EventsTableViewControllerDelegate>{
    NSArray *_recommendations;
    __weak Event *_tempEvent;
    __weak EventsTableViewController*_eventsTableViewController;
}

@end



@implementation HomeViewController{
    __weak IBOutlet iCarousel* _recommendationView;;
    
    NSMutableArray* _eventsArray;

    RangeOfDate _selectedDateRange;
    
    NSTimer*_moveTimer;
}

-(void)didSelectEvent:(Event *)event{
    _tempEvent = event;
    
    [self performSegueWithIdentifier:@"DetailSegue" sender:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _recommendationView.type = iCarouselTypeLinear;
    
    _recommendationView.pagingEnabled = YES;
    
    _eventsArray = [[NSMutableArray alloc] initWithCapacity:0];

    [self redloadRecommends];
    
#warning 1.1
 
    //[self setDropDownMenuTitles:@[NSLocalizedString(@"全部", nil),NSLocalizedString(@"本周", nil),NSLocalizedString(@"下周", nil),NSLocalizedString(@"下个月", nil)]];
    
    UILabel*lable=  [[UILabel alloc] init];
    lable.text = NSLocalizedString(@"全部", nil);
    lable.textColor = [UIHelper blueThemeColor];
    [lable sizeToFit];
    self.navigationItem.titleView =lable;
    // 1.1

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityChanged:) name:UserLocationCityUpdatedNotification object:nil];
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAuthed:) name:User_LoginNotification object:nil];
    
    
    
}


-(void)userAuthed:(NSNotification*)noti{
    _needAuth = NO;
}

-(void)cityChanged:(NSNotification*)noti{
    [self reloadEvents];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [LocationManager sharedLocationManager];
    
    if (_needAuth&&![[AccountManager sharedAccountManager] userAccount]) {
        [self showInAppAuthView];
        _needAuth = NO;
    }else{
        [self reloadEvents];
    }
}

-(void)move:(NSTimer*)timer{
    [_recommendationView scrollByNumberOfItems:1 duration:0.4];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self redloadRecommends];
    
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if(_moveTimer){
        [_moveTimer invalidate];
        _moveTimer = nil;
    }
}


-(void)menuChangedWithIndex:(NSInteger)idx{
    
    _selectedDateRange = idx;
    
    [self reloadEvents];
    
    [self hideTopMenu];
}
//获取置顶大图
-(void)redloadRecommends{
    
    NSMutableDictionary*dict = @{}.mutableCopy;
    NSString*city = [[LocationManager sharedLocationManager] eventCity];
    if (city) {
        [dict setObject:city forKey:@"city"];
    }
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:@"/recommends" parameters:dict success:^(NSURLSessionDataTask *task, id responseObject) {
            
            if ([responseObject count]>0) {
                NSError *error;
                _recommendations = [Event arrayOfModelsFromDictionaries:responseObject[@"events"] error:&error];
                [_recommendationView reloadData];
                
                if(_recommendations.count>1){
                    [_moveTimer invalidate];
                    _recommendationView.scrollEnabled = YES;
                    _moveTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(move:) userInfo:nil repeats:YES];
                }else{
                    _recommendationView.scrollEnabled = NO;
                }
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);;
            
        }];
    }];
    
}

-(void)reloadEvents{
    NSDate*_startDate;
    NSDate*_endDate;
    
    switch (_selectedDateRange) {
        case All:{
            _startDate = [NSDate date] ;
            _endDate = nil;
            break;
        }
        case ThisWeek:{
            _startDate = [NSDate date] ;
            _endDate = [[NSDate date] endOfWeek];
            break;
        }
        case NextWeek:
            _startDate = [[NSDate date] endOfWeek];
            _endDate = [[NSDate date] endOfNextWeek];
            break;
            
        case NextMonth:
            _startDate = [[NSDate date] endOfMonth];
            _endDate =[[NSDate date] endOfNextMonth];
            break;
        default:
            break;
    }
    
    [_eventsTableViewController loadEventsFrom:_startDate end:_endDate];
}

-(void)shouldGotoCreateEvent{
    if ([[LocationManager sharedLocationManager] currentUserLocation]) {
        [(MenuViewViewController*)self.sideMenuViewController.leftMenuViewController showCreatePage];
    }else{
        [MessageCenter showMessage:NSLocalizedString(@"创建活动需要开定位功能", nil) withType:MessageTypeError inViewController:self.sideMenuViewController];
    }
    
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return MAX([_recommendations count], 1);
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(RecomendationView *)view
{
    if (!view) {
        view =[[UINib nibWithNibName:@"RecomendationView" bundle:nil] instantiateWithOwner:nil options:nil][0];
        
        view.frame = _recommendationView.bounds;
        
    }
    [view.imageView setImage:[UIImage imageNamed:@"img-banner-default"]];
    
    
    
    if(_recommendations.count){
        Event *event = _recommendations[index];
        
        
        NSString* urlString;
        
        if ([event.posters count]>0) {
            
            urlString = [NSString stringWithFormat:@"%@/thumbnails/%@/%i.jpg",HostAPI,event.posters.firstObject,(int)(view.imageView.frame.size.width*[UIHelper ImageThumbnailScale])];
            
            [view.imageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"img-banner-default"]];
            
            view.imageView.contentMode = UIViewContentModeScaleAspectFill;

        }
        view.titleLabel.text = event.title;
        
        view.dateLabel.text = [event startAndEndDisplayString];
    }else{
        
        view.titleLabel.text = @"暂时还没有活动上榜哦";
        
        view.dateLabel.text = @"";
    }


    view.layer.masksToBounds = YES;
    

    
    return view;
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    
    if (option==iCarouselOptionWrap) {
        return 1;
    }
    return value;
}
-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    if (_recommendations.count>0) {
        [self didSelectEvent:_recommendations[index]];
        
    }
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     
     if ([segue.identifier isEqualToString:@"DetailSegue"]) {
         EventDetailViewController*dvc = segue.destinationViewController;
         
         dvc.dataSource.event = _tempEvent;
     }else{
         
         EventsTableViewController*tvc = segue.destinationViewController;
         tvc.eventDelegate = self;
         _eventsTableViewController = tvc;
     }
 }


@end
