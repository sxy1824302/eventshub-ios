//
//  DescriptionAndPhotoTableViewCell.m
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "DescriptionAndPhotoTableViewCell.h"
#import "UIHelper.h"
#import "NetworkManager.h"
#import "PhotoThumbnailView.h"
@implementation DescriptionAndPhotoTableViewCell{
    NSMutableArray*_imageButtons;
    
    UIButton* _addButton;
}

- (void)awakeFromNib {
    _textView.placeholderTextColor = [UIColor lightGrayColor];
    _textView.font = [UIHelper defaultFontWithSize:GuidlineMetric(32)];
    
    
    _imageButtons =[NSMutableArray array];
    
    _addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_addButton setImage:[UIImage imageNamed:@"btn-create-addphoto-normal"] forState:UIControlStateNormal];
    [_addButton setImage:[UIImage imageNamed:@"btn-create-addphoto-pressed"] forState:UIControlStateHighlighted];
    [_addButton addTarget:self action:@selector(addPhotos:) forControlEvents:UIControlEventTouchUpInside];
    [_addButton sizeToFit];
    [self.contentView addSubview:_addButton];
    
    
    for (int i = 0; i<3; i++) {
        PhotoThumbnailView*button = [[PhotoThumbnailView alloc] initWithFrame:_addButton.bounds];
        
        
        [button addTarget:self action:@selector(viewImage:) forControlEvents:UIControlEventTouchUpInside];
        
        button.autoresizingMask = _addButton.autoresizingMask;
        
        button.tag = i;
        
        [_imageButtons addObject:button];
    }
   
}
-(void)viewImage:(UIButton*)btn{
    [_delegate imageSelectedAtIndex:btn.tag];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)addPhotos:(id)sender {
    [_delegate addPhotoClickedWithButton:sender];
}

-(void)reloadPhotoViewsWithImageAndURL:(NSArray*)images{
    
    
    CGFloat margin = 16;
    
    __block CGFloat width = 0;
    
    [_imageButtons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIButton*btn = _imageButtons[idx];
        if (btn.superview) {
            [btn removeFromSuperview];
        }
    }];
    
    [images enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        PhotoThumbnailView*btn = _imageButtons[idx];
        
        
        if([obj isKindOfClass:[UIImage class]]){
            
            [btn.thumbnailImageView setImage:obj];
            
            
        }else if ([obj isKindOfClass:[NSString class]]){
            
            NSString* urlString = [NSString stringWithFormat:@"%@/thumbnails/%@/%i.jpg",HostAPI,obj,(int)(btn.frame.size.width*[UIHelper ImageThumbnailScale])];
            
            [btn loadImageURL:urlString];
        }
        
        if (btn.superview) {
            [btn removeFromSuperview];
        }
        
        btn.center =  CGPointMake(margin+CGRectGetWidth(btn.frame)*.5+(CGRectGetWidth(btn.frame)+margin)*idx,  CGRectGetHeight(self.contentView.frame)-(CGRectGetHeight(btn.frame)*0.5+margin));
        
        [self.contentView addSubview:btn];
        
        width =btn.frame.size.width;
    }];
    
    
    if (images.count>=3) {
        
        _addButton.hidden = YES;
        
    }else{
        _addButton.hidden = NO;
        _addButton.center =  CGPointMake(margin+(_addButton.frame.size.width)*.5+(width+margin)*images.count,  CGRectGetHeight(self.contentView.frame)-(CGRectGetHeight(_addButton.frame)*0.5+margin));
       
    }

}

@end
