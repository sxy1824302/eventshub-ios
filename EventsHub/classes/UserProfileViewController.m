//
//  UserProfileViewController.m
//  EventsHub
//
//  Created by alexander on 2/5/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "UserProfileViewController.h"
#import <CSStickyHeaderFlowLayout.h>
#import "UserProfileInfoCell.h"
#import "AccountManager.h"
#import <FAKFontAwesome.h>
#import "NetworkManager.h"
#import "ProfileIconView.h"
#import "MessageCenter.h"
#import "CircleButton.h"
#import "UIHelper.h"
#import "ProfileHeaderView.h"
#import <RESideMenu.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "GenderInfoCell.h"
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
#import "UIHelper.h"

#define PhotoActionTag 111

#define EmailRegex @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)"

typedef void(^CellTextfeildEditBlock)(BOOL isEdit);

@interface UserProfileViewController ()<UITextFieldDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegateFlowLayout
,GenderInfoCellValueChangedDelegate>{
    CellTextfeildEditBlock _textFeildEditBlock;
    BOOL _isUerEdit;
    

    BOOL _hasEditImage;
    BOOL _hasEdited;
    UIImage* _editImage;
    CircleButton* _editIconButton;
    BOOL _infoEdit;
    
    ProfileHeaderView*_profileHeaderView;
    
    
    NSString* _editedUserName;
    NSString* _editedPhone;
    NSString* _editedEmail;
    
    NSString* _editedGender;

}

@end

@implementation UserProfileViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isUerEdit = NO;
    _infoEdit = NO;
    _hasEditImage = NO;
    
    self.photoPicker.allowEdit =YES;
    

    self.navigationItem.title = NSLocalizedString(@"关于我", nil);
    

    [[NSNotificationCenter defaultCenter] addObserverForName:User_InfoUpdatedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self reloadProfile];
    }];
    
    self.collectionView.backgroundColor = [UIHelper whiteBackgroundColor];
    
    
    [self setBarButtonItems];
    
    CSStickyHeaderFlowLayout *layout = (id)self.collectionViewLayout;
    
    CGFloat headerSize = 203;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(CGRectGetWidth(self.view.frame), headerSize);
        
        // Setting the minimum size equal to the reference size results
        // in disabled parallax effect and pushes up while scrolls
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(CGRectGetWidth(self.view.frame), headerSize);
    }
    
    // Also insets the scroll indicator so it appears below the search bar
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(headerSize, 0, 0, 0);
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ProfileHeaderView" bundle:nil]forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader withReuseIdentifier:@"header"];
    

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)getPhotoAction{
    if (_isUerEdit) {
        [self.photoPicker showOnView:_editIconButton withMaxCount:1];
    }
}

-(void)setBarButtonItems{
    
    
    if (_isUerEdit) {
        FAKFontAwesome* check = [FAKFontAwesome checkIconWithSize:kIconFontSize];
        [check addAttribute:NSForegroundColorAttributeName value: [UIHelper blueThemeColor]];
        UIButton*button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(doneEditAction:) forControlEvents:UIControlEventTouchUpInside];
        [button setAttributedTitle:check.attributedString forState:UIControlStateNormal];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [button sizeToFit];
        UIBarButtonItem* checkItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        FAKFontAwesome* cancel = [FAKFontAwesome closeIconWithSize:kIconFontSize];
        [cancel addAttribute:NSForegroundColorAttributeName value: [UIHelper blueThemeColor]];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(cancelEditAction:) forControlEvents:UIControlEventTouchUpInside];
        [button setAttributedTitle:cancel.attributedString forState:UIControlStateNormal];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [button sizeToFit];
        UIBarButtonItem* cancelItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        self.navigationItem.leftBarButtonItem = cancelItem;
        self.navigationItem.rightBarButtonItem = checkItem;
        
        self.navigationItem.title = NSLocalizedString(@"编辑", nil);
    }else{
        
        FAKFontAwesome* edit = [FAKFontAwesome editIconWithSize:kIconFontSize];
        [edit addAttribute:NSForegroundColorAttributeName value: [UIHelper blueThemeColor]];
        
        UIButton*button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(editProfile:) forControlEvents:UIControlEventTouchUpInside];
        [button setAttributedTitle:edit.attributedString forState:UIControlStateNormal];
        
        [edit addAttribute:NSForegroundColorAttributeName value:[UIHelper greyColor]];
        [button setAttributedTitle:edit.attributedString forState:UIControlStateDisabled];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [button sizeToFit];
        UIBarButtonItem *edititem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = self.menuBarButton;
        self.navigationItem.rightBarButtonItem = edititem;
        
        self.navigationItem.title = NSLocalizedString(@"关于我", nil);
    }
}

-(void)showMenu:(UIBarButtonItem*)item{
    [self.sideMenuViewController presentLeftMenuViewController];
}

-(void)reloadProfile{
    UserAccount *account = [[AccountManager sharedAccountManager] userAccount];
    
    _editedUserName = account.username;
    
    _editedPhone = account.phone;
    
    NSString* avatar_path = account.avatar_path;
    
    if (_profileHeaderView && avatar_path) {
        [_profileHeaderView.profileIconView loadAvatarWithURLComponents:avatar_path placeHolder:_editImage];
        _editImage = _profileHeaderView.profileIconView.imageView.image;
    }
    
    [self.collectionView reloadData];
}

-(void)editProfile:(UIBarButtonItem*)sender{
    _isUerEdit = YES;
    _editIconButton.hidden = NO;
    [self setBarButtonItems];
    [self.collectionView reloadData];
}

-(void)cancelEditAction:(UIBarButtonItem*)sendr{
    _isUerEdit = NO;
    _editIconButton.hidden = YES;
    [self setBarButtonItems];
    [self.collectionView reloadData];
}

-(BOOL)checkParametersRight{
    
    
    if (_editedPhone) {
        NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
        NSError *anError = nil;
        NBPhoneNumber *myNumber = [phoneUtil parse:_editedPhone defaultRegion:@"CN" error:&anError];
        if (anError == nil) {
            if (![phoneUtil isValidNumber:myNumber]) {
                
                [MessageCenter showMessage:NSLocalizedString(@"请输入正确的联系电话", nil) withType:MessageTypeError inViewController:self];
                return NO;
            }
        }
    }

    
    NSString* regularString = EmailRegex;
    if (_editedEmail) {
        NSRange r = [_editedEmail rangeOfString:regularString options:NSRegularExpressionSearch];
        if (r.location == NSNotFound) {
            
            [MessageCenter showMessage:NSLocalizedString(@"请输入正确的邮箱", nil) withType:MessageTypeError inViewController:self];
            return NO;
        }
    }
    
    return YES;
}

-(void)doneEditAction:(UIBarButtonItem*)sender{
    if ([self checkParametersRight]) {
        _isUerEdit = NO;
        [self.collectionView reloadData];
        
        _isUerEdit = NO;
        _editIconButton.hidden = YES;
        [self setBarButtonItems];
        
        if (_infoEdit) {
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            _infoEdit = NO;
            
            UserAccount *account = [UserAccount new];
            
            if (_editedUserName&&[_editedUserName length]>0) {
                account.username = _editedUserName;
            }
            if (_editedPhone) {
                account.phone = _editedPhone;
                
            }
            if (_editedEmail) {
                account.email = _editedEmail;
                
            }
            
            if (_editedGender) {
                account.gender = _editedGender;
            }
            
            
            [[AccountManager sharedAccountManager] updateUserInfo:account.toDictionary complete:^(bool succeed) {
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                if (succeed) {
                    _editedUserName = nil;
                    _editedPhone = nil;
                    _editedEmail = nil;
                    [MessageCenter showMessage:NSLocalizedString(@"信息更新成功", nil) withType:MessageTypeSuccess inViewController:self];
                }
            }];
        }
        
        
        if (_hasEditImage) {
            
            _hasEditImage = NO;
            self.navigationItem.leftBarButtonItem.enabled = NO;
            self.navigationItem.rightBarButtonItem.enabled = NO;
            [_profileHeaderView.profileIconView setUpdating:YES];
            
            [[AccountManager sharedAccountManager] updateUserAvatar:UIImagePNGRepresentation(_editImage) complete:^(bool succeed) {
                [_profileHeaderView.profileIconView setUpdating:NO];
                self.navigationItem.leftBarButtonItem.enabled = YES;
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }];
        }
        [self.collectionView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    switch (textField.tag) {
            
        case FieldTagPhone:{
            if ([textField.text isEqualToString:NSLocalizedString(@"未设置", nil)]) {
                textField.text = @"";
            }
            break;
        }
        case FieldTagEmail:{
            if ([textField.text isEqualToString:NSLocalizedString(@"未设置", nil)]) {
                textField.text = @"";
            }
            break;
        }
        default:
            break;
    }

}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
        case FieldTagName:{
            if (![_editedUserName isEqualToString:textField.text]) {
                _editedUserName = textField.text;

            }
            break;
        }
        case FieldTagPhone:{
            if (![_editedPhone isEqualToString:textField.text]) {
                _editedPhone = textField.text;
      
            }
            break;
        }
        case FieldTagEmail:{
            if (![_editedEmail isEqualToString:textField.text]) {
                _editedEmail = textField.text;
               
            }
            break;
        }
        default:
            break;
    }
    _infoEdit = YES;
}
#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return FieldTagCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UserProfileInfoCell *cell;
    UserAccount *account = [[AccountManager sharedAccountManager] userAccount] ;
    



    switch (indexPath.row) {
        case FieldTagName:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InfoCell" forIndexPath:indexPath];
            
            [cell.inputLabel setPlaceholder:NSLocalizedString(@"昵称", nil) floatingTitle:NSLocalizedString(@"昵称", nil)];
            cell.inputLabel.text = account.username;
            cell.inputLabel.tag = FieldTagName;
            cell.inputLabel.keyboardType = UIKeyboardTypeDefault;
            cell.inputLabel.userInteractionEnabled = _isUerEdit;
            break;
        case FieldTagPhone:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InfoCell" forIndexPath:indexPath];
            
            [cell.inputLabel setPlaceholder:NSLocalizedString(@"电话号码", nil) floatingTitle:NSLocalizedString(@"电话号码", nil)];
            cell.inputLabel.tag = FieldTagPhone;
            cell.inputLabel.keyboardType = UIKeyboardTypePhonePad;
            if (!account.phone||[account.phone length]<1) {
                cell.inputLabel.text = NSLocalizedString(@"未设置", nil);
            }else{
                cell.inputLabel.text = account.phone;
            }
            cell.inputLabel.userInteractionEnabled = _isUerEdit;
            break;
        case FieldTagEmail:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InfoCell" forIndexPath:indexPath];
            
            [cell.inputLabel setPlaceholder:NSLocalizedString(@"Email", nil) floatingTitle:NSLocalizedString(@"Email", nil)];
            cell.inputLabel.tag = FieldTagEmail;
            cell.inputLabel.keyboardType = UIKeyboardTypeDefault;
            if ([account.email length]<1) {
                cell.inputLabel.text = NSLocalizedString(@"未设置", nil);
            }else{
                cell.inputLabel.text = account.email;
            }
            cell.inputLabel.userInteractionEnabled = _isUerEdit;
            break;
        case FieldTagGender:{
            
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GenderInfoCell" forIndexPath:indexPath];
            
            GenderInfoCell*acell = (GenderInfoCell*)cell;
            
            [cell.inputLabel setPlaceholder:NSLocalizedString(@"性别", nil) floatingTitle:NSLocalizedString(@"性别", nil)];
            acell.gender = account.gender;
            
            cell.tintColor = [UIHelper blueThemeColor];
            
            acell.delgeate = self;
            
            if (_isUerEdit) {
                acell.genderSelection.hidden = NO;
            }else{
                acell.genderSelection.hidden = YES;
            }
            
            break;
        }
        default:
            break;
    }
    

    cell.inputLabel.delegate = self;
    
    if (_isUerEdit) {
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }else{
        cell.contentView.backgroundColor = [UIHelper whiteBackgroundColor];
    }
    
    
    return cell;
}
-(void)valueDidChanged:(NSInteger)value ofCell:(GenderInfoCell *)cell{
    _infoEdit = YES;
    _editedGender = cell.gender;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        UICollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                            withReuseIdentifier:@"header"
                                                                                   forIndexPath:indexPath];
        
        if (!_profileHeaderView) {
            
            _profileHeaderView = (ProfileHeaderView*)cell;
            UserAccount *account = [[AccountManager sharedAccountManager] userAccount];
            
            NSString* avatar_path = account.avatar_path;
            
            [_profileHeaderView.profileIconView loadAvatarWithURLComponents:avatar_path placeHolder:_editImage];
            
            _editImage = _profileHeaderView.profileIconView.imageView.image;
        }
    
        if (!_editIconButton) {
            _editIconButton = [CircleButton buttonWithType:UIButtonTypeCustom];
            [_editIconButton setImage:[UIImage imageNamed:@"btn-avatar-edit-normal"] forState:UIControlStateNormal];
            [_editIconButton setImage:[UIImage imageNamed:@"btn-avatar-edit-pressed"] forState:UIControlStateNormal];
            [_editIconButton sizeToFit];
            [_editIconButton addTarget:self action:@selector(getPhotoAction) forControlEvents:UIControlEventTouchUpInside];
            _editIconButton.hidden = YES;
            [_editIconButton initializePayload];
            [_profileHeaderView.profileIconView addSubview:_editIconButton];
        }

        return cell;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewFlowLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.view.frame.size.width, collectionViewLayout.itemSize.height);
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == PhotoActionTag) {


        switch (buttonIndex) {
            case 0:
                [self.photoPicker showAlbumPickerWithPhotoLimit:1];
                break;
            case 1:
                [self.photoPicker showCamara];
                break;
            default:
                break;
        }
 
        
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
    
    if (!image) {
        image = [info valueForKey:UIImagePickerControllerOriginalImage];
    }
    
    _editImage = image;
    _profileHeaderView.profileIconView.imageView.image = image;
    _hasEditImage = YES;
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
