//
//  CommentTableViewCell.h
//  EventsHub
//
//  Created by alexander on 4/16/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileIconView.h"

#define kAvatarSize 40
#define kMinimumHeight 60



@interface CommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ProfileIconView *profileView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@end
