//
//  ProfileHeaderView.h
//  EventsHub
//
//  Created by alexander on 2/5/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileIconView.h"
@interface ProfileHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet ProfileIconView *profileIconView;
@end
