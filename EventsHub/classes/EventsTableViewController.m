//
//  EventsTableViewController.m
//  EventsHub
//
//  Created by alexander on 2/13/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "EventsTableViewController.h"
#import "EventTableViewCell.h"
#import "NetworkManager.h"
#import "LocationManager.h"
#import "NSDate+ISO8601.h"
#import "MessageCenter.h"
#import "EventsManager.h"
#import <UIScrollView+EmptyDataSet.h>
#import "EventsDataCache.h"
@interface EventsTableViewController ()<DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>{
    UIRefreshControl*_downRefresh;
    BOOL _ignoreCity;
    
    BOOL _isUpdating;
}

@end

@implementation EventsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [UIView new];
    
    _downRefresh = [[UIRefreshControl alloc] init];
    _downRefresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"获取活动"];
    [_downRefresh addTarget:self action:@selector(RefreshViewControlEventValueChanged) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = _downRefresh;
    
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    _isUpdating = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
 
}
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text ;
    
    
    if (_isUpdating) {
        
        text =  @"正在搜索活动";
        
    }else{
        
        text =  @"当前城市还没有活动哦，快去创建活动邀请小伙伴加入吧！";
    }
  
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    if (_isUpdating) {
        
        return nil;
    }else{
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],NSForegroundColorAttributeName:[UIHelper blueThemeColor]};
        
        return [[NSAttributedString alloc] initWithString:@"创建活动" attributes:attributes];
    }

}
- (void)emptyDataSetDidTapButton:(UIScrollView *)scrollView{
    [self.eventDelegate shouldGotoCreateEvent];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[EventsDataCache sharedEventsDataCache] events] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EventTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"EventCell"];
    
    Event* showEvent = [[[EventsDataCache sharedEventsDataCache] events] objectAtIndex:indexPath.row];
    
    cell.event = showEvent;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    [self.eventDelegate didSelectEvent:[[[EventsDataCache sharedEventsDataCache] events] objectAtIndex:indexPath.row]];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)RefreshViewControlEventValueChanged{
    [self loadEventsFrom:[NSDate date] end:_endDate];
}

-(void)reloadWithStartDate:(NSDate*)date needReset:(BOOL)reset{
    
    if (![_downRefresh isRefreshing]) {
        [_downRefresh beginRefreshing];
    }
    _isUpdating = YES;
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        NSString*city = [[LocationManager sharedLocationManager] eventCity];
        
        NSMutableDictionary*dict = @{@"after":[date ISOString]}.mutableCopy;
        
        if (_endDate) {
            [dict setObject:[_endDate ISOString] forKey:@"end_at"];
        }
        
    
        [dict setObject:CURRENT_LOCALE_IDENTIFIER forKey:@"locale"];
        
        if (!_ignoreCity) {
            
            if (city) {
                [dict setObject:city forKey:@"city"];
                
            }
        }
    
        
        [networkManager.JSONEncodeHttpRequestManager GET:@"/events" parameters:dict success:^(NSURLSessionTask *task, id responseObject) {
            
            _isUpdating = NO;
            
            if ([_downRefresh isRefreshing]) {
                [_downRefresh endRefreshing];
            }
            
            [self.tableView reloadEmptyDataSet];
            
            if ([responseObject count]>0) {
                JSONModelError*error ;
                NSArray*events = [Event arrayOfModelsFromDictionaries:responseObject[@"events"] error:&error];
                if (error) {
                    failed(error,responseObject);
                    
                }else{
                    
                    if (reset) {
                        [[[EventsDataCache sharedEventsDataCache] events] removeAllObjects];
                    }
                    
                    [[EventsDataCache sharedEventsDataCache] appendEvents:events];
                   
                    if ([[EventsDataCache sharedEventsDataCache] events].count==0) {
                        
                        if (_ignoreCity) {
                            _ignoreCity = NO;
                            return ;
                        }
                        _ignoreCity = YES;
                        
                        [self reloadWithStartDate:date needReset:reset];
                        
                    }else{
                        [self.tableView reloadData];
                    }
                    
                }
                
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);;
            if ([_downRefresh isRefreshing]) {
                [_downRefresh endRefreshing];
            }
        }];
        
    }];

}
-(void)loadEventsFrom:(NSDate*)from end:(NSDate*)end{
    _startDate = from;
    _endDate = end;
    
    [self reloadWithStartDate:_startDate needReset:YES];
}

-(void)loadMore{
    
    _fromIndexDate = [NSDate dateFromISOString:[(Event*)[[[EventsDataCache sharedEventsDataCache] events] lastObject] date_end]];
    
    [self reloadWithStartDate:_fromIndexDate needReset:NO];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y + scrollView.frame.size.height >= scrollView.contentSize.height) {
        
        if(scrollView.contentOffset.y>20){
            
            [self loadMore];
            
        }
        
    }
}
@end
