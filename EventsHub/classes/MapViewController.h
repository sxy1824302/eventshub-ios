//
//  MapViewController.h
//  EventsHub
//
//  Created by alexander on 3/3/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "BaseUIViewController.h"
@class Event;

@interface MapViewController : BaseUIViewController

@property (nonatomic,weak)Event*event;

@end
