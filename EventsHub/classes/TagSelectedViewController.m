//
//  TagSelectedViewController.m
//  EventsHub
//
//  Created by apple on 15/1/4.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import "TagSelectedViewController.h"
#import "TagCollectionViewCell.h"

@interface TagSelectedViewController ()
@property (nonatomic) NSMutableArray* selectedTagsArray;

@end

@implementation TagSelectedViewController{
 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"添加标签", nil);
   
    if (!_selectedTagsArray) {
        _selectedTagsArray = [[NSMutableArray alloc] initWithCapacity:3];

    }



    self.selectedTagsView.layer.borderWidth=1;
    self.selectedTagsView.layer.borderColor = [[UIColor ghostWhiteColor] CGColor];
    
    self.selectedTagsView.backgroundColor = self.showAllTagsCollectionView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    
    FAKFontAwesome* tick = [FAKFontAwesome checkIconWithSize:20];
    UIBarButtonItem* doneItem = [[UIBarButtonItem alloc] initWithImage:[tick imageWithSize:CGSizeMake(40, 40)] style:UIBarButtonItemStyleBordered target:self action:@selector(doneAction:)];
    
    self.navigationItem.rightBarButtonItem = doneItem;
    
    [self.selectedTagsView reloadData];
}

-(void)setSelectedTagsWith:(NSArray*)array{
    if (!_selectedTagsArray) {
        _selectedTagsArray = [[NSMutableArray alloc] initWithCapacity:3];
    }
    
    for(Tag* tag in array) {
        [_selectedTagsArray addObject:tag];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)doneAction:(UIBarButtonItem*)item{
    _selectedBlock(_selectedTagsArray.copy);
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)selectedTag:(Tag*)tag{
    if ([_selectedTagsArray count]>=3) {
        [[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"TagLimitTitle", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"确定", nil) otherButtonTitles: nil] show];
        return;
    }
    if ([_selectedTagsArray indexOfObject:tag] != NSNotFound) {
        [[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"TagRepeatedWarning", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"确定", nil) otherButtonTitles: nil] show];
        return;
    }
    [_selectedTagsArray addObject:tag];
    
    [self.selectedTagsView reloadData];
}




#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView==self.showAllTagsCollectionView) {
        return [[[EventsManager sharedEventsManager] eventTags] count];
    }
    
    return _selectedTagsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TagCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"tagsCell" forIndexPath:indexPath];

    
    Tag* tag ;
    
    __weak TagSelectedViewController* weakSelf = self;
    
    if (collectionView==self.showAllTagsCollectionView) {
        
        NSArray* array = [[EventsManager sharedEventsManager] eventTags];
        
        tag= [array objectAtIndex:indexPath.row];
        
        [cell.stickView setEventTag:tag andCallBack:^(NSDictionary *tagDic) {
            
            [weakSelf selectedTag:tag];
        }];
        
    }else{
        
        tag= [_selectedTagsArray objectAtIndex:indexPath.row];
        
        [cell.stickView setEventTag:tag andCallBack:^(NSDictionary *tagDic) {
            
            
        }];
    
        [cell.stickView tagStickWithCancel:^{
            
            [weakSelf.selectedTagsArray removeObject:tag];
            
            [weakSelf.selectedTagsView reloadData];
        }];
    }
    
   
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Tag* tag ;
    
    if (collectionView==self.showAllTagsCollectionView) {
        
        NSArray* array = [[EventsManager sharedEventsManager] eventTags];
        
        tag= [array objectAtIndex:indexPath.row];
        
        CGSize size = [[EventTagView attributeName:tag.tag] size];
        
        size = CGSizeMake(ceilf(size.width),ceilf( size.height));
        
        return CGSizeMake(MAX(kDefaultTagMinWidth, size.width+(16*2)), kDefaultTagHeight);
        
    }else{
        
        tag= [_selectedTagsArray objectAtIndex:indexPath.row];
        
        CGSize size = [[EventTagView attributeName:tag.tag] size];
        
        size = CGSizeMake(ceilf(size.width),ceilf( size.height));
        
        return CGSizeMake(MAX(kDefaultTagMinWidth, size.width+(16*2))+10, kDefaultTagHeight);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 0, 0, 0);
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    if (collectionView==self.showAllTagsCollectionView) {
        return 10;
    }
    return 4;
}
@end
