//
//  POIViewController.m
//  EventsHub
//
//  Created by 亮 姚 on 14-12-12.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "POIViewController.h"
#import "AccountManager.h"
#import <CoreLocation/CLLocation.h>
#import "EventsManager.h"
#import "Address.h"
#import "Config.h"
#import "LocationManager.h"
#import "MessageCenter.h"
#import <MBProgressHUD.h>
@interface POIViewController ()

@end

@implementation POIViewController{
    BMKPoiSearch* _globalSearcher;

    BMKGeoCodeSearch* _geoCodeSearch;

    NSArray* _nearbyPOIList;
    
    
    
    NSArray *_searchResultList;
    
    
    BMKPoiInfo *_selectedPOI;
    
    BMKReverseGeoCodeResult *_resultCache;
    __weak IBOutlet UITableView *_tableView;
    
    
    BOOL _nearySearching;
}





- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.tableFooterView = [UIView new];
    
    _mapView.frame = CGRectMake( _mapView.frame.origin.x,  _mapView.frame.origin.y, self.view.frame.size.width,  _mapView.frame.size.height);//baidu bug
    
    [_mapView setZoomLevel:13];
    _mapView.isSelectedAnnotationViewFront = YES;
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;//显示定位图层
   
    
    
    _mapView.userTrackingMode = BMKUserTrackingModeFollow;
    _mapView.showsUserLocation = YES;
    
    _globalSearcher =[[BMKPoiSearch alloc]init];
    _globalSearcher.delegate = self;
    
    _geoCodeSearch = [[BMKGeoCodeSearch alloc] init];
    _geoCodeSearch.delegate = self;

    
    [[LocationManager sharedLocationManager] reloadLocateUser];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLocationUpdated:) name:UserLocationUpdatedNotification object:nil];

}
-(void)userLocationUpdated:(NSNotification*)noti{
    
    [_mapView updateLocationData:noti.object];
    
    [self nearbySearchAction];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
}
#pragma mark - BMKLocationServiceDelegate



-(void)centerActivity{
//    CGPoint centerPoint = self.mapCenterImageView.center;
//    [UIView animateWithDuration:0.5 animations:^{
//        self.mapCenterImageView.center = CGPointMake(centerPoint.x, centerPoint.y-10);
//    } completion:^(BOOL finished) {
//       [UIView animateWithDuration:0.2 animations:^{
//          self.mapCenterImageView.center = CGPointMake(centerPoint.x, centerPoint.y);
//       }];
//    }];
}

#pragma mark - BMKMapViewDelegate
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{

    [self nearbySearchAction];
    [self centerActivity];
}



-(void)nearbySearchAction{

    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    reverseGeocodeSearchOption.reverseGeoPoint = _mapView.centerCoordinate;
  
    BOOL flag = [_geoCodeSearch reverseGeoCode:reverseGeocodeSearchOption];
    
    if(flag)
    {
        _resultCache = nil;
        DDLogVerbose(@"反geo检索发送成功");
    }
    else
    {
        DDLogVerbose(@"反geo检索发送失败");
    }
    
}


#pragma mark - POI
- (void)onGetPoiResult:(BMKPoiSearch*)searcher result:(BMKPoiResult*)poiResultList errorCode:(BMKSearchErrorCode)error
{
    if (error == BMK_SEARCH_NO_ERROR) {
        
        
        [MBProgressHUD hideHUDForView:self.searchDisplayController.searchResultsTableView animated:YES];
        _searchResultList = poiResultList.poiInfoList;
        
        [self.searchDisplayController.searchResultsTableView reloadData];
        
        
        
    }
    else if (error == BMK_SEARCH_AMBIGUOUS_KEYWORD){
        [MessageCenter showMessage:NSLocalizedString(@"SearchWarning", nil) withType:MessageTypeWarning inViewController:nil];
    } else {
        DDLogVerbose(@"抱歉，未找到结果");
    }
}


#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.POISearchBar.showsCancelButton = YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    self.POISearchBar.showsCancelButton = NO;

    BMKCitySearchOption*op = [[BMKCitySearchOption alloc] init];
    op.pageIndex = 0;
    op.pageCapacity = 20;
    op.keyword = self.POISearchBar.text;
    op.city = [[LocationManager sharedLocationManager] eventCity];
    
    BOOL flag = [_globalSearcher poiSearchInCity:op];
    if(flag)
    {
        [MBProgressHUD showHUDAddedTo:self.searchDisplayController.searchResultsTableView animated:YES];
    }
    else
    {
        DDLogVerbose(@"周边检索发送失败");
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.POISearchBar.showsCancelButton = NO;
}




#pragma mark - BMKGeoCodeSearchDelegate
- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    
    if (error == BMK_SEARCH_NO_ERROR) {
        
        if (self.childViewControllers.count>0) {
            return;
        }
        
        _resultCache = result;
        
        if (_selectedPOI) {
            
            [self updateDataAndGoBack];
            
        }else{
            
            _nearbyPOIList =result.poiList;
            
            [_tableView reloadData];
            
        }
        
        
    }
    else if (error == BMK_SEARCH_AMBIGUOUS_KEYWORD){
        
        DDLogVerbose(@"起始点有歧义");
        
    } else {
        DDLogVerbose(@"抱歉，未找到结果");
    }
    
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.searchDisplayController.searchResultsTableView==tableView){
        return _searchResultList.count;
    }
    return _nearbyPOIList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell;
    
    

    BMKPoiInfo* infoForCell ;
    
    if (self.searchDisplayController.searchResultsTableView==tableView) {
       
        infoForCell = [_searchResultList objectAtIndex:indexPath.row];
        
         cell = [tableView dequeueReusableCellWithIdentifier:@"POICell"];
        if (!cell) {
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"POICell"];
        }
       
    }else{
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"POICell"];
        
        infoForCell = [_nearbyPOIList objectAtIndex:indexPath.row];
        
    }
    cell.textLabel.text = infoForCell.name;
    cell.detailTextLabel.text = infoForCell.address;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    BMKPoiInfo* infoForCell ;
    
    if (self.searchDisplayController.searchResultsTableView==tableView) {
        
        infoForCell = [_searchResultList objectAtIndex:indexPath.row];
        
    }else{
        
        infoForCell = [_nearbyPOIList objectAtIndex:indexPath.row];
    }
    if (!_selectedPOI) {
         _selectedPOI = infoForCell;
    
    }else{
        
        

        if ([_selectedPOI.uid isEqualToString:infoForCell.uid]) {
           
        }else{
            _selectedPOI = infoForCell;
           
        }
    }
    
  
    if (self.childViewControllers.count>0) {
        [self dismissViewControllerAnimated:YES completion:^{
            [self selectedAddress];
            
            
        }];
    }else{
        [self selectedAddress];
    }
    
}


- (void)selectedAddress{

    BMKPoiInfo*poi =_selectedPOI;
    
    
    if (_resultCache) {
        
        [self updateDataAndGoBack];
        
    }else{
        
        BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
        reverseGeocodeSearchOption.reverseGeoPoint = poi.pt;
        
        BOOL flag = [_geoCodeSearch reverseGeoCode:reverseGeocodeSearchOption];
        
        if (flag) {
            DDLogVerbose(@"reverseGeoCode succeed");
        }else{
            DDLogVerbose(@"reverseGeoCode failed");
        }
        
    }
}

-(void)updateDataAndGoBack{
    Address*  evenAddress = [[Address alloc] init];
    evenAddress.country = @"中国";
    
    if (!_selectedPOI) {
        evenAddress.city = _resultCache.addressDetail.city;
        
        evenAddress.district = _resultCache.addressDetail.district;
        evenAddress.street1 = _resultCache.addressDetail.streetName;
        evenAddress.street2 =  _resultCache.addressDetail.streetNumber;
        
        evenAddress.province =  _resultCache.addressDetail.province;
        
        evenAddress.geopoint = [Location geoPointWithCoordicates:_resultCache.location];
        
    }else{
        
        
        evenAddress.city = _selectedPOI.city;
        evenAddress.street1 = _selectedPOI.address;
        evenAddress.city = _selectedPOI.city;
        evenAddress.province =  _resultCache.addressDetail.province;
        evenAddress.geopoint = [Location geoPointWithCoordicates:_selectedPOI.pt];
    }

    
    
    _selectedBlock(evenAddress,_selectedPOI.name);
    _selectedBlock = nil;
    _selectedPOI = nil;
    [self.navigationController popViewControllerAnimated:YES];
}
@end
