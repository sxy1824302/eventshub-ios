//
//  DescriptionAndPhotoTableViewCell.h
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SZTextView.h>


@protocol ImageOperationDelegate <NSObject>
- (void)addPhotoClickedWithButton:(UIButton*)btn;
- (void)imageSelectedAtIndex:(NSInteger)idx;
@end

@interface DescriptionAndPhotoTableViewCell : UITableViewCell
@property (nonatomic,weak)id<ImageOperationDelegate> delegate;

@property (weak, nonatomic) IBOutlet SZTextView *textView;
//@property (weak, nonatomic) UIButton *addButton;

-(void)reloadPhotoViewsWithImageAndURL:(NSArray*)images;
@end
