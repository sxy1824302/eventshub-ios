//
//  EventsDataCache.h
//  EventsHub
//
//  Created by alexander on 3/19/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"


@interface EventsDataCache : NSObject
@property (nonatomic)NSMutableArray*events;

+ (id)sharedEventsDataCache;


-(void)appendEvents:(NSArray*)events;
@end
