//
//  JoinPopupViewController.h
//  EventsHub
//
//  Created by alexander on 2/28/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SZTextView.h>
#import <JVFloatLabeledTextField.h>
#import "UIHelper.h"
#import "EHTabBar.h"

@protocol JoinPopupViewControllerDelegate <NSObject>

-(void)performJoinWithData:(NSDictionary*)data;

@end


@interface JoinPopupViewController : UIViewController


@property (nonatomic,weak)id<JoinPopupViewControllerDelegate>delegate;

@property (nonatomic)BOOL needPhone;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet SZTextView *noteTextView;

@property (weak, nonatomic) IBOutlet EHTabBar *actionBar;



@end
