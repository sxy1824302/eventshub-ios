//
//  SettingViewController.h
//  EventsHub
//
//  Created by 亮 姚 on 14-11-18.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseUIViewController.h"


typedef NS_ENUM(NSInteger, SettingsTag) {
    SettingsTagCity,
    SettingsTagAboutUs,
    SettingsTagCleanCache,
    SettingsTagWriteReview,
    SettingsTagHelpAndContact,
    SettingsTagLogout,
    SettingsTagCount,
};

@interface SettingViewController : BaseUIViewController

@end
