//
//  GenderInfoCell.h
//  EventsHub
//
//  Created by alexander on 4/1/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JVFloatLabeledTextField.h>

@class GenderInfoCell;

@protocol GenderInfoCellValueChangedDelegate <NSObject>

-(void)valueDidChanged:(NSInteger)value ofCell:(GenderInfoCell*)cell;

@end


@interface GenderInfoCell : UICollectionViewCell

@property (nonatomic)NSString*gender;
@property (nonatomic,weak)id<GenderInfoCellValueChangedDelegate>delgeate;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *inputLabel;

@property (weak, nonatomic) IBOutlet UIView *seperatorLine;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSelection;
@end
