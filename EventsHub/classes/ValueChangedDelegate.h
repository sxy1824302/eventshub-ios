//
//  ValueChangedDelegate.h
//  EventsHub
//
//  Created by alexander on 1/13/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>




@protocol ValueChangedDelegate <NSObject>
-(void)cell:(UITableViewCell*)cell didChangedValue:(id)value;
@end
