//
//  EventInfoTableViewCell.h
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@end
