//
//  EventDetailViewController.m
//  EventsHub
//
//  Created by alexander on 12/31/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventDetailViewController.h"
#import "EventsManager.h"
#import "NetworkManager.h"
#import "EHTabBar.h"
#import "CommentsMessageViewController.h"
#import "AccountManager.h"
#import "Event+Join.h"
#import "Event+Interest.h"
#import <MMMaterialDesignSpinner.h>
#import "EventHubRootViewController.h"
#import "QuickSignInViewController.h"
#import "ParticipantsListTableViewController.h"
#import "ELCImagePickerController.h"
#import "PhotosCacheManager.h"
#import "MessageCenter.h"
#import "CreateEventTableViewController.h"
#import "SocialServices.h"
#import "OtherProfileViewController.h"
#import <UINavigationController+M13ProgressViewBar.h>
#import "JoinPopupViewController.h"

#import "MapViewController.h"
#import "ImageDataConvertor.h"


@interface EventDetailViewController ()<ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate,PhotoBrowsingDelegate,JoinPopupViewControllerDelegate>{
    UIBarButtonItem *_likeItem;
    UIBarButtonItem *_unlikeItem;
}
@property (weak, nonatomic) IBOutlet UIImageView *eventPosterImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet EHTabBar *bottomBar;

@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet UILabel *participantLabel;

@property(nonatomic)BOOL joined;
@end

typedef void(^DisabledButton)(NSInteger index,BOOL enabled);

@implementation EventDetailViewController{


    NSInteger _changeButtonIndex;
    BOOL _changeEnabled;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _dataSource = [[EventDetailDataSource alloc] initWithViewController:self];
        
       
    }
    return self;
}

-(void)awakeFromNib{
    
}

-(void)loadView{

    [super loadView];
    self.tableview.delegate = _dataSource;
    self.tableview.dataSource = _dataSource;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_dataSource initializeData];
    
    self.title = NSLocalizedString(@"详细", nil);
    
    [self.tableview registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"CommentCell"];
    
    _changeButtonIndex = 0;
    _changeEnabled = YES;
 
    self.maskView.backgroundColor = [[UIHelper darkColor] colorWithAlphaComponent:0.8];

    UITapGestureRecognizer*tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectPostser:)];
    [self.maskView addGestureRecognizer:tg];
    
    self.tableview.tableFooterView = [UIView new];

    self.tableview.backgroundColor = [UIHelper whiteBackgroundColor];
    
    _bottomBar.delegate = self;
    
    
    // TODO: button?
    FAKFontAwesome *heart = [FAKFontAwesome heartOIconWithSize:kIconFontSize];
    _likeItem = [[UIBarButtonItem alloc] initWithImage:[heart imageWithSize:kIconSizeNormal] style:UIBarButtonItemStyleBordered target:self action:@selector(interest:)];
    
    FAKFontAwesome *heart1 = [FAKFontAwesome heartIconWithSize:kIconFontSize];
    _unlikeItem = [[UIBarButtonItem alloc] initWithImage:[heart1 imageWithSize:kIconSizeNormal] style:UIBarButtonItemStyleBordered target:self action:@selector(interest:)];


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAuthed:) name:User_LoginNotification object:nil];
    
    
    [self updateTabbar];
    
    
    [self updateLikeAndJointStatus];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self reloadEvent];
    
    [_dataSource reloadComments];
}

-(void)userAuthed:(NSNotification*)noti{
    [self updateLikeAndJointStatus];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)selectPostser:(UITapGestureRecognizer*)tpg{
    if (_dataSource.event.posters.count>0) {
        
        [self imageSelectedAtIndex:0];
        
    }
}

-(void)setJoined:(BOOL)joined{
    _joined = joined;
    
    [self updateTabbar];
}


-(void)updateLikeAndJointStatus{
    
    if ([[AccountManager sharedAccountManager] checkUserSigned]) {
        
        [_dataSource.event checkInterest:^(NSDictionary *responce,BOOL success) {
            if (success) {
                int code = [[responce objectForKey:@"code"] intValue];
                 self.dataSource.liked = code;
                
                if (self.dataSource.liked) {
                    self.navigationItem.rightBarButtonItem = _unlikeItem;
                    
                }else{
                    
                    self.navigationItem.rightBarButtonItem = _likeItem;
                }
            }
            
        }];
        
        if (!IsMyEvent(_dataSource.event)) {
            
            [_dataSource.event checkJoinedWithUserID:[[AccountManager sharedAccountManager] currentAccountID] andHandle:^(NSDictionary *responce,BOOL success) {
                if (success) {
                    int code = [[responce objectForKey:@"code"] intValue];
                    self.joined = (code==1)?YES:NO;
                    
                }else{
                    
                }
                
            }];
        }
    }
}
-(void)gotoComments{
    CommentsMessageViewController*mvc = [CommentsMessageViewController new];
    mvc.event = self.dataSource.event;
    
    [self.navigationController pushViewController:mvc animated:YES];
}
-(void)reloadEvent{
    
    self.titleLabel.text = _dataSource.event.title;
    
    
    self.timeLabel.text = [_dataSource.event startAndEndDisplayString];
    
    
    self.participantLabel.text = [NSString localizedStringWithFormat:@"%@ 参加",[_dataSource.event.participant_count stringValue]];
    
    self.infoLabel.text = [NSString localizedStringWithFormat:@"%@",_dataSource.event.fee_description];
    
    NSString* urlString = @"";
    
    if ([_dataSource.event.posters count]>0) {
        
        urlString = [NSString stringWithFormat:@"%@/images/%@",HostAPI,[_dataSource.event.posters firstObject]];
        
        [self.eventPosterImageView sd_setImageWithURL:[NSURL URLWithString:urlString]  placeholderImage:[UIImage imageNamed:@"poster_default"]];
        
    }
    
    [_dataSource.event.posters enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if ([obj isKindOfClass:[NSNull class]]) {
            [self changeButtonEnabledWith:0 enaled:NO];
        }
        
    }];
    
    [self.tableview reloadData];

}

-(void)changeButtonEnabledWith:(NSInteger)index enaled:(BOOL)ena{
    self.navigationItem.leftBarButtonItem.enabled = ena;
    
    _changeButtonIndex = index;
    _changeEnabled = ena;
    
    [_bottomBar.buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx == _changeButtonIndex) {
            UIButton* button = obj;
            button.enabled = _changeEnabled;
        }
    }];
    
}

-(void)updateTabbar{
        NSString* newTitle ;
    
        if (![_dataSource.event isExpired]) {
            
            if (IsMyEvent(_dataSource.event)) {
                
                newTitle = NSLocalizedString(@"编辑", nil);
                
            }else{
                
                newTitle = self.joined?NSLocalizedString(@"退出", nil):NSLocalizedString(@"加入",nil);
            }
            
             [_bottomBar applyNewTitles:@[newTitle,NSLocalizedString(@"分享", nil)]];
            
        }else{
            
            if (IsMyEvent(_dataSource.event)) {
                
                newTitle = NSLocalizedString(@"重新发布", nil);
                
            }else{
                
                newTitle = NSLocalizedString(@"活动已结束", nil);
            }
            
            [_bottomBar applyNewTitles:@[newTitle]];
        }
    

    [_bottomBar.buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx == _changeButtonIndex) {
            UIButton* button = obj;
            button.enabled = _changeEnabled;
        }
    }];
}


#pragma mark - EHTabBarTappedDelegate
-(void)tabBarDidClickedButtonAtIndex:(NSInteger)index{
        switch (index) {
            case 0:
                if (![_dataSource.event isExpired]) {
                    if (IsMyEvent(_dataSource.event)) {
                        
                        [self editEvent];
                    }else{
                        
                        [self toggleJoinAction];
                    }
                }else{
                    if (IsMyEvent(_dataSource.event)) {
                        
                        [self createNewEventFrom];
                    }
                }
                break;
            case 1:{
                if (![_dataSource.event isExpired])
                    [self shareAction];
                break;
            }
            default:
                break;
        }
    
}



-(void)editEvent{
 
    CreateEventTableViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateEventTableViewController"];
    
    viewController.editingMode = EventEditingModeEdit;
    
    [viewController loadViewControllerWithEvent:_dataSource.event];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)createNewEventFrom{

    CreateEventTableViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateEventTableViewController"];
    
    viewController.editingMode = EventEditingModeOldCreateNew;
    
    [viewController loadViewControllerWithEvent:_dataSource.event];
    
    [self.navigationController pushViewController:viewController animated:YES];
}



- (void)interest:(UIBarButtonItem*)sender {
    
    
    
    if (![[AccountManager sharedAccountManager] checkUserSigned]) {
        
        [self showInAppAuthView];
    }else{
        
        // TODO: fix this
        MMMaterialDesignSpinner *spinnerView = [[MMMaterialDesignSpinner alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        spinnerView.lineWidth = 1;
        [spinnerView startAnimating];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:spinnerView];
        
        if (!self.dataSource.liked) {
            
            
            [_dataSource.event interest:^(NSDictionary *responce,BOOL success) {
                
                if(success){
                    
                    self.navigationItem.rightBarButtonItem = _unlikeItem;
                    
                    self.dataSource.liked = YES;
                }else{
                    
                    self.navigationItem.rightBarButtonItem = _likeItem;
                }
                
            }];
        }else{
            [_dataSource.event uninterest:^(NSDictionary *responce, BOOL success) {
                
                if (success) {
                    self.dataSource.liked = NO;
                    self.navigationItem.rightBarButtonItem = _likeItem;
                    
                }else{
                    self.navigationItem.rightBarButtonItem = _unlikeItem;
                }
                
            }];
        }
    }
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ParticipantsSegue"]) {
        ParticipantsListTableViewController* partCont = segue.destinationViewController;
        partCont.event = _dataSource.event;
    }else if ([segue.identifier isEqualToString:@"OtherProfileSegue"]){
        OtherProfileViewController*hvc =segue.destinationViewController;
        hvc.userID = _dataSource.event.owner;
    }else if ([segue.identifier isEqualToString:@"MapSegue"]){
        MapViewController*hvc =segue.destinationViewController;
        hvc.event = _dataSource.event;
        
    }
}

-(void)performJoinWithData:(NSDictionary *)data{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    hud.labelText = NSLocalizedString(@"JoiningEvent", nil);
    [_dataSource.event joinWithData:data complete:^(NSDictionary *responce, BOOL success) {
        if (success) {
            self.joined = YES;
        }
        [hud hide:YES];
    }];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.navigationController cancelProgress];
}

-(void)toggleJoinAction{
    
    if (![[AccountManager sharedAccountManager] checkUserSigned]) {
        
        [self showInAppAuthView];
        
        return;
    }
    
    
    if (!self.joined) {
        
        JoinPopupViewController *popin = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinPopupViewController"];
        
        [popin setPopinTransitionStyle:BKTPopinTransitionStyleSpringySlide];
        
        [popin setPopinTransitionDirection:BKTPopinTransitionDirectionBottom];
        
        [popin setPopinAlignment:BKTPopinAlignementOptionCentered];
        
        if ([_dataSource.event.config[@"phone_required"] boolValue]) {
            popin.needPhone = YES;
        }
        [popin setPopinOptions:BKTPopinDisableAutoDismiss];
        
        popin.delegate = self;
        
        
        [self presentPopinController:popin animated:YES completion:^{
            [popin.phoneTextField becomeFirstResponder];
        }];
        
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        
        hud.labelText = NSLocalizedString(@"QuitEvent", nil);
        [_dataSource.event deleteParticipantWithUserID:[[AccountManager sharedAccountManager] currentAccountID] andHandle:^(NSDictionary *responce, BOOL success) {
            if (success) {
                self.joined = NO;
            }
            [hud hide:YES];
        }];
    }

}

-(void)shareAction{
    UIActionSheet* shareActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    for (NSString* title in  [[SocialServices sharedSocialServices] shareTitle]) {
        [shareActionSheet addButtonWithTitle:title];
    }
    [shareActionSheet addButtonWithTitle:NSLocalizedString(@"取消", nil)];
    shareActionSheet.cancelButtonIndex = shareActionSheet.numberOfButtons - 1;
    shareActionSheet.tag = 1111;
    [shareActionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (actionSheet.tag == 1111) {
        
        NSString* shareTitle = _dataSource.event.title;
        
        NSString* shareDes ;
        
        if (_dataSource.event.eventDescription.length>50) {
     
            shareDes = [NSString stringWithFormat:@"%@...",[_dataSource.event.eventDescription substringWithRange:NSMakeRange(0, 50)]];
        }else{
            
            shareDes = _dataSource.event.eventDescription;
        }
        
     
        NSString* URLForEvent = [NSString stringWithFormat:@"%@/page/join_event/%@",HostAPI,_dataSource.event._id];
        
        UIImage* shareImage = _posterCell.displayedImages.firstObject;
        
        
        if (buttonIndex != actionSheet.numberOfButtons-1) {
            //#warning share
            switch (buttonIndex) {
                case 0:{
                    if ([WeiboSDK isWeiboAppInstalled]) {
                        WeiboService*wservice = [[SocialServices sharedSocialServices] serviceOfType:ServiceTypeWeibo];
                        
                        [wservice.manager shareWithTitle:shareTitle description:shareDes thumbNail:shareImage andLink:URLForEvent eventID:_dataSource.event._id andFinish:^(BOOL succeed) {
                            
                        }];
                    }
                    break;
                }
                case 1:{
                    
                    
                    TencentService*wservice = [[SocialServices sharedSocialServices] serviceOfType:ServiceTypeTencent];
                    
                    [wservice.manager shareToQQWithTitle:shareTitle description:shareDes thumbNail:shareImage andLink:URLForEvent andFinish:^(BOOL succeed) {
                        
                    }];
                    break;
                }
                case 2:{
                    
                    if ([WXApi isWXAppInstalled]) {
                        WechatService*wservice = [[SocialServices sharedSocialServices] serviceOfType:ServiceTypeWechat];
                        
                        [wservice.manager shareWithTitle:shareTitle description:shareDes thumbNail:shareImage andLink:URLForEvent  andFinish:^(BOOL succeed) {
                            
                            
                        }];
                    }
                    break;
                }
                case 3:{
                    
                    [MessageCenter showMessage:NSLocalizedString(@"活动链接已经成功保存到粘贴板", nil) withType:MessageTypeSuccess inViewController:nil];
                    [[UIPasteboard generalPasteboard] setPersistent:YES];
                    [[UIPasteboard generalPasteboard] setValue:URLForEvent forPasteboardType:[UIPasteboardTypeListString objectAtIndex:0]];
                    
                    break;
                }
                default:
                    break;
            }
            
        }
    }else {
        
        if (_dataSource.event.posters.count>=3) {
            return;
        }
        if (buttonIndex==actionSheet.cancelButtonIndex) {
            
        }else if (buttonIndex==0){
            
            [self.photoPicker showAlbumPickerWithPhotoLimit:3-_dataSource.event.posters.count];
        }else if (buttonIndex==1){
            
            [self.photoPicker showCamara];
            
        }
        
    }

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
    
    if (!image) {
        image = [info valueForKey:UIImagePickerControllerOriginalImage];
    }
    
    
    [self.navigationController showProgress];
    
    if ([_dataSource.event.posters isKindOfClass:[NSArray class]]) {
        _dataSource.event.posters = _dataSource.event.posters.mutableCopy;
    }
    
    [(NSMutableArray*)_dataSource.event.posters addObject:[NSNull null]];
    
    
    [self changeButtonEnabledWith:0 enaled:NO];
    
    [[EventsManager sharedEventsManager] addPosters:@[image] toEvent:_dataSource.event._id progress:^(CGFloat percentage) {
        [self.navigationController setProgress:percentage animated:YES];
    } succeed:^(NSArray* posters) {
        
        [self changeButtonEnabledWith:0 enaled:YES];
        
        [self.navigationController finishProgress];
        
        NSMutableArray*newPosters = _dataSource.event.posters.mutableCopy;
        
        
        __block NSInteger posterIndex = 0;
        
        [_dataSource.event.posters enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            if ([obj isKindOfClass:[NSNull class]]) {
                
                
                [newPosters replaceObjectAtIndex:idx withObject:posters[posterIndex]];
                
                posterIndex++;
                
            }
            
        }];
        
        _dataSource.event.posters = newPosters;
        
        
        if (posters.count>0) {
            [self.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagEventPoster inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
    }];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    [self.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagEventPoster inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    

}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{

    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    
    for (NSDictionary *dict in info) {
        
        UIImage *image = [dict objectForKey:UIImagePickerControllerOriginalImage];
        [images addObject:image];
    }
    
    
    
    [self.navigationController showProgress];
    
    if ([_dataSource.event.posters isKindOfClass:[NSArray class]]) {
        _dataSource.event.posters = _dataSource.event.posters.mutableCopy;
    }
    
    [images enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [(NSMutableArray*)_dataSource.event.posters addObject:[NSNull null]];
    }];
    

    
    [self changeButtonEnabledWith:0 enaled:NO];
    
    [[EventsManager sharedEventsManager] addPosters:images toEvent:_dataSource.event._id progress:^(CGFloat percentage) {
        [self.navigationController setProgress:percentage animated:YES];
    } succeed:^(NSArray* posters) {

        [self changeButtonEnabledWith:0 enaled:YES];
        
        [self.navigationController finishProgress];
        
        NSMutableArray*newPosters = _dataSource.event.posters.mutableCopy;
        
        
        __block NSInteger posterIndex = 0;
        
        [_dataSource.event.posters enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            if ([obj isKindOfClass:[NSNull class]]) {
                
                
                [newPosters replaceObjectAtIndex:idx withObject:posters[posterIndex]];
                
                posterIndex++;
                
            }
            
        }];
        
        _dataSource.event.posters = newPosters;
        
    
        if (posters.count>0) {
            [self.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagEventPoster inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
    }];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    

    [self.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagEventPoster inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




-(void)addPhotoClickedWithButton:(UIButton *)btn{
    if (_dataSource.event.posters.count<3) {

        [self.photoPicker showOnView:btn withMaxCount:3-_dataSource.event.posters.count];
    }
}




#pragma mark browser
-(void)imageSelectedAtIndex:(NSInteger)idx{
    
    NSMutableArray*urls = [NSMutableArray array];
    
    [_dataSource.event.posters enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString* urlString = [NSString stringWithFormat:@"%@/images/%@",HostAPI,obj];
        [urls addObject:urlString];
    }];
    
    [[PhotosCacheManager sharedPhotosCacheManager] displayImages:urls fromViewController:self showDelete:IsMyEvent(_dataSource.event)];
}

-(void)deletePhotoAtIndex:(NSInteger)idx{
    
    
    if (_dataSource.event.posters.count>0) {
        
        if ([_dataSource.event.posters isKindOfClass:[NSArray class]]) {
            _dataSource.event.posters = _dataSource.event.posters.mutableCopy;
        }
        
        
        [[EventsManager sharedEventsManager] deletePoster:_dataSource.event.posters[idx] toEvent:_dataSource.event._id succeed:^(BOOL succeed) {
            
        }];
        
        [(NSMutableArray*)_dataSource.event.posters removeObjectAtIndex:idx];
        
        
    }


    [self.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];

}
-(void)foredQuitPhotoBrowser{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
