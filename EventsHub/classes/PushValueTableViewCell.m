//
//  PushValueTableViewCell.m
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "PushValueTableViewCell.h"
#import "UIHelper.h"
#import <FontAwesomeKit.h>
@implementation PushValueTableViewCell

- (void)awakeFromNib {
    self.textLabel.font = [UIHelper defaultFontWithSize:GuidlineMetric(32)];
    self.textLabel.textColor = [UIHelper stoneColor];
    
   
    self.valid = YES;
    
    _accessoryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    
    FAKFontAwesome*fa = [FAKFontAwesome angleRightIconWithSize:20];
    [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper blueColor]];
    _accessoryLabel.attributedText = [fa attributedString];
    [_accessoryLabel sizeToFit];
}

-(void)setValid:(BOOL)valid{
    _valid = valid;
    
    if (_valid) {
         self.detailTextLabel.textColor =[UIHelper blueColor];
    }else{
         self.detailTextLabel.textColor =[UIHelper orangeColor];
    }
}
-(void)setShowArraw:(BOOL)show{
    if (show) {
        self.accessoryView=_accessoryLabel;
    }else{
        self.accessoryView = nil;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(BOOL)canBecomeFirstResponder{
    return YES;
}


// TODO: 重构 复制自 CreateNormalTableViewCell
-(void)setReuiredName:(NSString *)name{
    
    NSMutableAttributedString*string = [[NSMutableAttributedString alloc] initWithString:name attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:GuidlineMetric(32)],NSForegroundColorAttributeName:[UIHelper stoneColor]}];
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:@"*" attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:GuidlineMetric(32)],NSForegroundColorAttributeName:[UIHelper orangeColor]}]];
    
    
    self.textLabel.attributedText = string;
}

@end
