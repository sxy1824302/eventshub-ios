//
//  CreateEventTableViewController.m
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "CreateEventTableViewController.h"
#import "CreateNormalTableViewCell.h"
#import "DescriptionAndPhotoTableViewCell.h"
#import "PushValueTableViewCell.h"
#import "BOOLTableViewCell.h"
#import "Address.h"
#import "ValueChangedDelegate.h"
#import "POIViewController.h"
#import <UIViewController+MaryPopin.h>
#import "TagSelectedViewController.h"
#import "AccountManager.h"
#import <MBProgressHUD.h>
#import "MenuViewViewController.h"
#import "NSDate+ISO8601.h"
#import "MessageCenter.h"
#import "UIHelper.h"
#import "ELCImagePickerController.h"
#import "NetworkManager.h"
#import "PhotosCacheManager.h"
#import "SocialServices.h"
#import <UINavigationController+M13ProgressViewBar.h>
#import "ImageDataConvertor.h"
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>


@import AssetsLibrary;

#define kCellIdentifier @"kCellIdentifier"
#define kCellName @"kCellName"
#define kCellTag @"kCellTag"
#define kCellRequiered @"kCellRequiered"

typedef NS_ENUM(NSInteger, CellTags) {
    CellTagsTitle,
    CellTagsDescription,
    CellTagsHostPhone,
    CellTagsTags,
    CellTagsFromDate,
    CellTagsToDate,
    CellTagsAddress,
    CellTagsLimit,
    CellTagsFee,
    CellTagsNeedPhone,
    CellTagsNeedNotifyOnJoin,
};

@interface CreateEventTableViewController ()<UITableViewDataSource,UITableViewDelegate,ValueChangedDelegate,UITextFieldDelegate,UITextViewDelegate,
EHTabBarTappedDelegate,ImageOperationDelegate,UIActionSheetDelegate,ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate,PhotoBrowsingDelegate>{
    NSArray *_cellConfig;
    
    
    NSDate*_startDate;
    
    NSDate*_endDate;
    
    NSArray*_tags;
    
    BOOL _needPhoneToJoint;
    
    BOOL _needNotifyOnJoint;
    
    NSString*_fee;
    
    NSInteger _limit;
    
    NSString *_poi;
    
    Address *_address;
    
    NSString* _phone;
    
    NSString *_title;
    
    NSString*_description;

    
    
    Event* _tmpEvent;


    BOOL _validationPassed;
    
    
    
    NSMutableArray*_addedPosterImages;
    NSArray *_posterThumbnailURLS;
  
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CreateEventTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"创建", nil);
    _tabbar.delegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    
    _cellConfig = @[@{kCellName:NSLocalizedString(@"活动标题", nil),kCellIdentifier:@"NormalCell",kCellTag:@(CellTagsTitle),kCellRequiered:@(YES)},
                    @{kCellName:NSLocalizedString(@"活动介绍...", nil),kCellIdentifier:@"DescriptionPhotoCell",kCellTag:@(CellTagsDescription)},
                    @{kCellName:NSLocalizedString(@"我的电话", nil),kCellIdentifier:@"NormalCell",kCellTag:@(CellTagsHostPhone),kCellRequiered:@(YES)},
                    @{kCellName:NSLocalizedString(@"活动标签", nil),kCellIdentifier:@"PushValueCell",kCellTag:@(CellTagsTags)},
                    @{kCellName:NSLocalizedString(@"从", nil),kCellIdentifier:@"PushValueCell",kCellTag:@(CellTagsFromDate),kCellRequiered:@(YES)},
                    @{kCellName:NSLocalizedString(@"到", nil),kCellIdentifier:@"PushValueCell",kCellTag:@(CellTagsToDate),kCellRequiered:@(YES)},
                    @{kCellName:NSLocalizedString(@"地址", nil),kCellIdentifier:@"PushValueCell",kCellTag:@(CellTagsAddress),kCellRequiered:@(YES)},
                    @{kCellName:NSLocalizedString(@"人数上限", nil),kCellIdentifier:@"NormalCell",kCellTag:@(CellTagsLimit)},
                    @{kCellName:NSLocalizedString(@"活动费用", nil),kCellIdentifier:@"NormalCell",kCellTag:@(CellTagsFee)},
                    @{kCellName:NSLocalizedString(@"参加活动需要电话", nil),kCellIdentifier:@"BOOLCell",kCellTag:@(CellTagsNeedPhone)},
                    @{kCellName:NSLocalizedString(@"有人加入时通知", nil),kCellIdentifier:@"BOOLCell",kCellTag:@(CellTagsNeedNotifyOnJoin)},
                    ];
    
    
    [self setupDefault];
    
}

-(void)setupDefault{
    if (_limit==0) {
        _limit = 50;
    }
    
    [self checkDateWithMessage:NO];
  
    _addedPosterImages = [NSMutableArray arrayWithCapacity:3];
    
    if (_editingMode==EventEditingModeCreateNew){
        UserAccount *account = [[AccountManager sharedAccountManager] userAccount];
        _phone = account.phone;
    }
    
    if (_editingMode == EventEditingModeOldCreateNew) {
        _startDate = [NSDate dateWithTimeIntervalSinceNow:60*30];
        _endDate = [NSDate dateWithTimeIntervalSinceNow:60*60];
    }
    
}

-(void)setEditingMode:(EventEditingMode)editingMode{
    _editingMode = editingMode;
    
    if (_editingMode==EventEditingModeEdit) {
        
        self.navigationItem.leftBarButtonItem = nil;
        
        self.navigationItem.title = NSLocalizedString(@"编辑", nil);
    }
    
    [self updateTabBarTitle];
}

-(void)updateTabBarTitle{
    
    
    
    if (!_validationPassed) {
        
        _tabbar.warning = YES;
        
        [_tabbar applyNewTitles:@[NSLocalizedString(@"完成必填项以后才可以发布哦", nil)]];
        
    }else{
        
         _tabbar.warning = NO;
        
        if (_editingMode==EventEditingModeEdit) {
            
            [_tabbar applyNewTitles:@[NSLocalizedString(@"完成编辑", nil)]];
            
      
        }else{
            
            [_tabbar applyNewTitles:@[NSLocalizedString(@"发布活动", nil)]];
        }
        
    }
    
}


-(void)loadViewControllerWithEvent:(Event*)showEvent{

    _title = showEvent.title ;
    
    _description = showEvent.eventDescription;
    
    _phone = showEvent.contact_phone;
    
    NSArray* array = [[EventsManager sharedEventsManager] eventTags];
    NSMutableArray* showArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    
    
    for (NSString* codeString in showEvent.tags) {
        NSInteger tagid = [codeString integerValue];
 
        Tag* showTag = [array objectAtIndex:tagid-1];
        
        [showArray addObject:showTag];
    }
    _tags = showArray;
    
    _address = showEvent.address;
    
    _poi = showEvent.pio;
    
    _startDate = [NSDate dateFromISOString:showEvent.date_start];
    
    _endDate = [NSDate dateFromISOString:showEvent.date_end];
    
    _limit = [showEvent.like_count integerValue];
    
    _fee = showEvent.fee_description;
    
    _needPhoneToJoint = [[showEvent.config objectForKey:@"phone_required"] boolValue];
    
    _needNotifyOnJoint = [[showEvent.config objectForKey:@"notify_on_join"] boolValue];
    
    _posterThumbnailURLS = showEvent.posters;
    
    _tmpEvent = showEvent;
    
    [self.tableView reloadData];
}


- (void)textViewDidChange:(UITextView *)textView{
    _description = textView.text;
    [self checkRequiredField];
}
#pragma mark textf field

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
        case CellTagsTitle:{
            _title = textField.text;
            break;
        }
        case CellTagsHostPhone:{
            _phone = textField.text;
            break;
        }
        case CellTagsLimit:{
            _limit = [textField.text integerValue];
            break;
        }
        case CellTagsFee:{
            _fee = textField.text;
            break;
        }
    }
    [self checkRequiredField];
}

-(void)textLimitWarningMessage{
    if(_title.length>50){
        _validationPassed = NO;
        [MessageCenter showMessage:NSLocalizedString(@"活动标题不得超过50字。", nil) withType:MessageTypeError inViewController:self];
        return;
    }
    if(_description.length>1000){
        _validationPassed = NO;
        [MessageCenter showMessage:NSLocalizedString(@"活动描述不得超过1000字。", nil) withType:MessageTypeError inViewController:self];
        return;
    }
    if([_phone length]>50){
        _validationPassed = NO;
        [MessageCenter showMessage:NSLocalizedString(@"手机号码长度不得超过50。", nil) withType:MessageTypeError inViewController:self];
        return;
    }
    if (_limit>500) {
        _validationPassed = NO;
        [MessageCenter showMessage:NSLocalizedString(@"活动人数不得大于500，已重置为默认的50个人", nil) withType:MessageTypeError inViewController:self];
        _limit = 50;
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsLimit inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        return;
    }
    if (_limit<=0) {
        _validationPassed = NO;
        [MessageCenter showMessage:NSLocalizedString(@"活动人数不能为负数，已重置为默认的50个人", nil) withType:MessageTypeError inViewController:self];
        _limit = 50;
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsLimit inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        return;
    }
}

-(BOOL)checkPhone:(NSString*)phone{
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:phone defaultRegion:@"CN" error:&anError];
    if (anError == nil) {
        if (![phoneUtil isValidNumber:myNumber]) {
            return NO;
        }
    }
    return YES;
}

-(void)checkRequiredField{
    _validationPassed = YES;
    [self textLimitWarningMessage];
    if(_title.length<1){
        _validationPassed = NO;
    }
    if(_description.length<1){
         _validationPassed = NO;
    }
    if([_phone length]<1&&[self checkPhone:_phone]){
         _validationPassed = NO;
    }
    if(!_address){
         _validationPassed = NO;
    }
    
    [self updateTabBarTitle];
}

-(void)tabBarDidClickedButtonAtIndex:(NSInteger)index{
    if(_title.length<1){
        [MessageCenter showMessage:NSLocalizedString(@"请输入活动标题。", nil) withType:MessageTypeError inViewController:self];
        return;
    }
    if(_description.length<1){
        [MessageCenter showMessage:NSLocalizedString(@"简单介绍下活动吧。", nil) withType:MessageTypeError inViewController:self];
        return;
    }
    if([_phone length]<1){
        [MessageCenter showMessage:NSLocalizedString(@"请输入活动的联系方式。", nil) withType:MessageTypeError inViewController:self];
        return;
    }else if(![self checkPhone:_phone]){
        [MessageCenter showMessage:NSLocalizedString(@"请输入正确的联系电话", nil) withType:MessageTypeError inViewController:self];
        return;
    }
    if(_limit<=0){
        [MessageCenter showMessage:NSLocalizedString(@"PeopleCountWarning", nil) withType:MessageTypeWarning inViewController:self];
        _limit = 50;
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsLimit inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        return;
    }
    
    if(!_address){
        [MessageCenter showMessage:NSLocalizedString(@"请选择一个活动的地址", nil) withType:MessageTypeWarning inViewController:self];
        return;
    }
    
    Event*event = [Event new];
    
    NSString*quo = @"@unionOfObjects.tag_id";
    NSArray*tag_ids = [_tags valueForKeyPath: quo];
    
    event.contact_phone = _phone;
    event.fee_description = _fee;
    event.config = @{@"phone_required":@(_needPhoneToJoint) ,
                     @"notify_on_join":@(_needNotifyOnJoint)};
    event.tags = tag_ids;
    event.address = _address;
    event.pio = _poi;
    event.owner = [[AccountManager sharedAccountManager] currentAccountID];
    event.owner_name = [[[AccountManager sharedAccountManager] userAccount] username];
    event.total_participants_limit = @(_limit);
    event.date_start = [_startDate ISOString];
    event.date_end = [_endDate ISOString];
    event.title = _title;
    event.eventDescription = _description;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSMutableDictionary*para = event.toDictionary.mutableCopy;
    
    [para addEntriesFromDictionary:@{@"title":@{kDefaultLocale:_title}}];
    [para addEntriesFromDictionary: @{@"description":@{kDefaultLocale:_description}}];

    
    if (_editingMode==EventEditingModeEdit) {
        
        __weak CreateEventTableViewController* weakSelf = self;
        
        [_tmpEvent updateEventWithEventDic:para andPosters:nil succeed:^(BOOL succeed, NSString *idString) {
            if (succeed) {
                
                
                UserAccount *account = [[AccountManager sharedAccountManager] userAccount];
                if ([account.phone length]<1) {
                    account.phone = _phone;
                    [[AccountManager sharedAccountManager] updateUserInfo:account.toDictionary complete:^(bool succeed) {
                    }];
                }
                
                _tmpEvent.title = event.title;
                _tmpEvent.eventDescription = event.eventDescription;
                _tmpEvent.contact_phone = event.contact_phone;
                _tmpEvent.tags = event.tags;
                _tmpEvent.date_start = event.date_start;
                _tmpEvent.date_end = event.date_end;
                _tmpEvent.pio = event.pio;
                _tmpEvent.address = event.address;
                _tmpEvent.total_participants_limit = event.total_participants_limit;
                _tmpEvent.fee_description = event.fee_description;
                _tmpEvent.config = event.config;
                
                
                if (_addedPosterImages.count==0) {
                    [MessageCenter showMessage:@"编辑活动完成" withType:MessageTypeSuccess inViewController:nil];
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [weakSelf.navigationController cancelProgress];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }
        }];
        
        if (_addedPosterImages.count>0) {
            
            [[EventsManager sharedEventsManager] addPosters:_addedPosterImages toEvent:_tmpEvent._id progress:^(CGFloat percentage) {
                
                [weakSelf.navigationController showProgress];
                
                [weakSelf.navigationController setProgress:percentage animated:YES];
                
            } succeed:^(NSArray* addedPosters) {
                
                
                NSMutableArray* newArray = [NSMutableArray array];
                [newArray addObjectsFromArray:_tmpEvent.posters];
                [newArray addObjectsFromArray:addedPosters];
                _tmpEvent.posters = newArray;
                
                [MessageCenter showMessage:@"编辑活动完成" withType:MessageTypeSuccess inViewController:nil];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [weakSelf.navigationController cancelProgress];
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }];
        }
        

        
    }else{
        
        [[EventsManager sharedEventsManager] createEventWithEventDic:para andPosters:_addedPosterImages progress:^(CGFloat percentage) {
            [self.navigationController showProgress];
            [self.navigationController setProgress:percentage animated:YES];
            
        } succeed:^(BOOL succeed,NSString* idString) {
            
            if (succeed) {
                
                UserAccount *account = [[AccountManager sharedAccountManager] userAccount];
                if ([account.phone length]<1) {
                    account.phone = _phone;
                    [[AccountManager sharedAccountManager] updateUserInfo:account.toDictionary complete:^(bool succeed) {
                    }];
                }
                
                _tmpEvent = event;
                _tmpEvent._id = idString;
                [self shareAction];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }
}

-(void)shareAction{
    
    UIActionSheet* shareActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    for (NSString* title in [[SocialServices sharedSocialServices] shareTitle]) {
        [shareActionSheet addButtonWithTitle:title];
    }
    [shareActionSheet addButtonWithTitle:NSLocalizedString(@"取消", nil)];
    shareActionSheet.cancelButtonIndex = shareActionSheet.numberOfButtons - 1;
    shareActionSheet.tag = 1111;
    [shareActionSheet showInView:self.view];
    
}

-(void)shareFinishWith:(BOOL)success{
    NSString* shareMessage;
    MessageType finishType;
    if (success) {
        shareMessage = NSLocalizedString(@"分享成功", nil);
        finishType = MessageTypeSuccess;
    }else{
        shareMessage = NSLocalizedString(@"分享失败", nil);
        finishType = MessageTypeError;
    }
    [(MenuViewViewController*)self.sideMenuViewController.leftMenuViewController showMainPage];
    [MessageCenter showMessage:shareMessage withType:finishType inViewController:self.sideMenuViewController.rightMenuViewController];
}


# pragma mark images
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (actionSheet.tag == 1111) {
        
        NSString* shareTitle = _tmpEvent.title;
        NSString* shareDes ;
        
        if (_tmpEvent.eventDescription.length>50) {
            
            shareDes = [NSString stringWithFormat:@"%@...",[_tmpEvent.eventDescription substringWithRange:NSMakeRange(0, 50)]];
        }else{
            
            shareDes = _tmpEvent.eventDescription;
        }
        
        
        NSString* URLForEvent = [NSString stringWithFormat:@"%@/page/join_event/%@",HostAPI,_tmpEvent._id];
        //NSString* thumbURL  =  [NSString stringWithFormat:@"%@/eventshub/icons/icon_80.png",HostAPI];
        UIImage* shareImage = _addedPosterImages.firstObject;

        if (!shareImage) {
            shareImage = [UIImage imageNamed:@"AppIcon40x40@2x.png"];
        }else{
            shareImage = [ImageDataConvertor thumbnmailData:UIImageJPEGRepresentation(shareImage, 1) MIMEType:JPEGMIME];
        }
        switch (buttonIndex) {
            case 0:{
                if ([WeiboSDK isWeiboAppInstalled]) {
                    WeiboService*wservice = [[SocialServices sharedSocialServices] serviceOfType:ServiceTypeWeibo];
                    
                    [wservice.manager shareWithTitle:shareTitle description:shareDes thumbNail:shareImage andLink:URLForEvent eventID:_tmpEvent._id andFinish:^(BOOL succeed) {
                        if (succeed) {
                            [self shareFinishWith:YES];
                        }else{
                            [self shareFinishWith:NO];
                        }
                    }];
                }

                break;
            }
            case 1:{

    
                TencentService*wservice = [[SocialServices sharedSocialServices] serviceOfType:ServiceTypeTencent];
                
                
                [wservice.manager shareToQQWithTitle:shareTitle description:shareDes thumbNail:shareImage andLink:URLForEvent andFinish:^(BOOL succeed) {
                    NSLog(@"share title = %@,shareDes = %@",shareTitle,shareDes);
                    if (succeed) {
                        [self shareFinishWith:YES];
                    }else{
                        [self shareFinishWith:NO];
                    }
                }];
       
                break;
            }
            case 2:{
                if ([WXApi isWXAppInstalled]) {
                    WechatService*wservice = [[SocialServices sharedSocialServices] serviceOfType:ServiceTypeWechat];
                    
                    [wservice.manager shareWithTitle:shareTitle description:shareDes thumbNail:shareImage andLink:URLForEvent  andFinish:^(BOOL succeed) {
                        
                        
                        if (succeed) {
                            [self shareFinishWith:YES];
                        }else{
                            [self shareFinishWith:NO];
                        }
                    }];
                }
                break;
            }
            case 3:{
                [MessageCenter showMessage:NSLocalizedString(@"活动链接已经成功保存到粘贴板", nil) withType:MessageTypeSuccess inViewController:nil];
                [[UIPasteboard generalPasteboard] setPersistent:YES];
                [[UIPasteboard generalPasteboard] setValue:URLForEvent forPasteboardType:[UIPasteboardTypeListString objectAtIndex:0]];
                [(MenuViewViewController*)self.sideMenuViewController.leftMenuViewController showMainPage];
                break;
            }
        default:
            [(MenuViewViewController*)self.sideMenuViewController.leftMenuViewController showMainPage];
            break;
        }
        
    }else{
        if (buttonIndex==actionSheet.cancelButtonIndex) {
            
        }else if (buttonIndex==0){
            
            [self.photoPicker showAlbumPickerWithPhotoLimit:3-_addedPosterImages.count];
        }else if (buttonIndex==1){
            
            [self.photoPicker showCamara];
            
        }
    }
}

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
    
    if (!image) {
        image = [info valueForKey:UIImagePickerControllerOriginalImage];
    }
    
    [_addedPosterImages addObject:image];
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsDescription inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    
    for (NSDictionary *dict in info) {
        
        UIImage *image = [dict objectForKey:UIImagePickerControllerOriginalImage];
        [images addObject:image];
    }
    
    
    
    [_addedPosterImages addObjectsFromArray:images];
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsDescription inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)addPhotoClickedWithButton:(UIButton *)btn{
    
    if (_addedPosterImages.count+_posterThumbnailURLS.count<3) {
        
        [self.photoPicker showOnView:btn withMaxCount:3-_addedPosterImages.count];
    }
}
#pragma mark photo browser
-(void)imageSelectedAtIndex:(NSInteger)idx{
    
    NSMutableArray*urls = [NSMutableArray array];
    
    [_posterThumbnailURLS enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString* urlString = [NSString stringWithFormat:@"%@/images/%@",HostAPI,obj];
        [urls addObject:urlString];
    }];
    
    [[PhotosCacheManager sharedPhotosCacheManager] displayImages:[_addedPosterImages arrayByAddingObjectsFromArray:urls] fromViewController:self showDelete:YES];
}

-(void)deletePhotoAtIndex:(NSInteger)idx{
    
    if (_posterThumbnailURLS.count == 0||idx<_addedPosterImages.count) {
         [_addedPosterImages removeObjectAtIndex:idx];
        
    }else{
        idx = idx-_addedPosterImages.count;
        if ([_tmpEvent.posters isKindOfClass:[NSMutableArray class]]) {
            _tmpEvent.posters = _tmpEvent.posters.mutableCopy;
        }
        
        [[EventsManager sharedEventsManager] deletePoster:_tmpEvent.posters[idx] toEvent:_tmpEvent._id succeed:^(BOOL succeed) {
            
        }];
        
        
        [(NSMutableArray*)_tmpEvent.posters removeObjectAtIndex:idx];
        _posterThumbnailURLS = _tmpEvent.posters;

    }
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsDescription inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)foredQuitPhotoBrowser{
      [self.navigationController popViewControllerAnimated:YES];
}



-(void)cell:(UITableViewCell *)cell didChangedValue:(id)value{
    NSIndexPath*indexPath = [self.tableView indexPathForCell:cell];
    switch (indexPath.row) {

        case CellTagsNeedPhone:{
            BOOLTableViewCell*bcell = (BOOLTableViewCell*)cell;
            _needPhoneToJoint = bcell.swich.on;
            break;
            
        }
        case CellTagsNeedNotifyOnJoin:{
            BOOLTableViewCell*bcell = (BOOLTableViewCell*)cell;
            _needNotifyOnJoint = bcell.swich.on;
            break;
            
        }

    }
}

-(void)checkDateWithMessage:(BOOL)showMessage{
    if (!_startDate) {
        _startDate = [NSDate dateWithTimeIntervalSinceNow:60*30];
    }
    if (!_endDate) {
        _endDate = [_startDate dateByAddingTimeInterval:60*30];
    }
    
    if ([_endDate timeIntervalSinceDate:_startDate]<=60*30) {
        _endDate = [_startDate dateByAddingTimeInterval:60*30];
        if (showMessage) {
            [MessageCenter showMessage:NSLocalizedString(@"EventTimeWarning", nil) withType:MessageTypeWarning inViewController:self];
        }
    }

    if ([_endDate timeIntervalSinceDate:_startDate]>=60*60*24) {
        NSLog(@"%s",__func__);
        _endDate = [_startDate dateByAddingTimeInterval:60*60*24];
        if (showMessage) {
            [MessageCenter showMessage:NSLocalizedString(@"EventTimeWarning2", nil) withType:MessageTypeWarning inViewController:self];
        }
    }

    
}

-(void)dateChanged:(UIDatePicker*)picker{
    if (picker.tag==CellTagsFromDate) {
        _startDate = picker.date;
        
        [self checkDateWithMessage:NO];
        
    }else{
        
        _endDate = picker.date;
        
        [self checkDateWithMessage:YES];
    }
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsFromDate inSection:0],[NSIndexPath indexPathForRow:CellTagsToDate inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cellConfig.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_cellConfig[indexPath.row][kCellIdentifier] forIndexPath:indexPath];;
    
    
    switch ([_cellConfig[indexPath.row][kCellTag] intValue]) {
        case CellTagsTitle:{
            CreateNormalTableViewCell*ncell = (CreateNormalTableViewCell*)cell;
            [ncell setReuiredName:_cellConfig[indexPath.row][kCellName]];
            ncell.textField.text = _title;
            ncell.textField.delegate = self;
            ncell.textField.tag = CellTagsTitle;
            ncell.textField.placeholder = @"";
            ncell.textField.keyboardType = UIKeyboardTypeDefault;
            break;
            
        }
        case CellTagsDescription:{
            DescriptionAndPhotoTableViewCell*dcell = (DescriptionAndPhotoTableViewCell*)cell;
            dcell.textView.placeholder =_cellConfig[indexPath.row][kCellName];
            dcell.textView.delegate = self;
            dcell.textView.text = _description;
            dcell.delegate =self;
             [dcell reloadPhotoViewsWithImageAndURL:[_addedPosterImages arrayByAddingObjectsFromArray:_posterThumbnailURLS]];
            break;
            
        }
        case CellTagsHostPhone:{
            CreateNormalTableViewCell*ncell = (CreateNormalTableViewCell*)cell;
           [ncell setReuiredName:_cellConfig[indexPath.row][kCellName]];
            ncell.textField.text = _phone;
             ncell.textField.delegate = self;
             ncell.textField.tag = CellTagsHostPhone;
            ncell.textField.keyboardType = UIKeyboardTypePhonePad;
            break;
            
        }
        case CellTagsTags:{
            
            PushValueTableViewCell*pcell = (PushValueTableViewCell*)cell;
            
            pcell.textLabel.text =_cellConfig[indexPath.row][kCellName];
            
            [pcell setShowArraw:YES];
            
            if (_tags.count<1) {
                
                pcell.detailTextLabel.text = NSLocalizedString(@"未选择标签", nil);
                
                pcell.valid = NO;
                
            }else{
                
                NSString*quo = @"@unionOfObjects.tag";
                
                NSArray*tagTitle = [_tags valueForKeyPath: quo];
                
                pcell.detailTextLabel.text = [tagTitle componentsJoinedByString:@","];
                
                pcell.valid = YES;
            }
            break;
            
        }
        case CellTagsFromDate:{
            PushValueTableViewCell*pcell = (PushValueTableViewCell*)cell;
            [pcell setShowArraw:NO];
            pcell.value = _startDate;
            [pcell setReuiredName:_cellConfig[indexPath.row][kCellName]];
            pcell.detailTextLabel.text =[NSDateFormatter localizedStringFromDate:_startDate dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
            break;
            
        }
        case CellTagsToDate:{
            
            PushValueTableViewCell*pcell = (PushValueTableViewCell*)cell;
            [pcell setShowArraw:NO];
            pcell.value = _endDate;
            [pcell setReuiredName:_cellConfig[indexPath.row][kCellName]];
            pcell.detailTextLabel.text =[NSDateFormatter localizedStringFromDate:_endDate dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
            break;
            
        }
        case CellTagsAddress:{
            PushValueTableViewCell*pcell = (PushValueTableViewCell*)cell;
            [pcell setShowArraw:YES];
//            pcell.textLabel.text =_cellConfig[indexPath.row][kCellName];
            [pcell setReuiredName:_cellConfig[indexPath.row][kCellName]];
            pcell.detailTextLabel.text = _poi;
            if (_poi) {
                pcell.valid = YES;
            }
            break;
            
        }
        case CellTagsLimit:{
            CreateNormalTableViewCell*ncell = (CreateNormalTableViewCell*)cell;
            ncell.nameLabel.text =_cellConfig[indexPath.row][kCellName];
           
            ncell.textField.keyboardType = UIKeyboardTypeNumberPad;
            ncell.textField.placeholder = NSLocalizedString(@"默认50个人", nil);
            ncell.textField.delegate = self;
            ncell.textField.tag = CellTagsLimit;
            if (_limit==0) {
                ncell.textField.text = nil;
            }else{
                ncell.textField.text = @(_limit).stringValue;
            }
            break;
            
        }
        case CellTagsFee:{
            CreateNormalTableViewCell*ncell = (CreateNormalTableViewCell*)cell;
            ncell.nameLabel.text =_cellConfig[indexPath.row][kCellName];
            ncell.textField.text = _fee;
            ncell.textField.delegate = self;
            ncell.textField.keyboardType = UIKeyboardTypeDefault;
            ncell.textField.tag = CellTagsFee;
            
            if (_fee.length<1){
                
                 ncell.textField.placeholder= NSLocalizedString(@"免费", nil);
                
            }else{
                
                ncell.textField.text = _fee;
            }
          
            
            
            break;
            
        }
        case CellTagsNeedPhone:{
            BOOLTableViewCell*bcell = (BOOLTableViewCell*)cell;
            bcell.nameLabel.text =_cellConfig[indexPath.row][kCellName];
            bcell.swich.on = _needPhoneToJoint;
            bcell.delegate = self;
            break;
            
        }
        case CellTagsNeedNotifyOnJoin:{
            BOOLTableViewCell*bcell = (BOOLTableViewCell*)cell;
            bcell.nameLabel.text =_cellConfig[indexPath.row][kCellName];
            bcell.swich.on = _needNotifyOnJoint;
            bcell.delegate = self;
            break;
            
        }
        default:
            break;
    }
    [self checkRequiredField];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch ([_cellConfig[indexPath.row][kCellTag] intValue]) {
        case CellTagsDescription:
            return 206;
            break;
            
        default:
            return 44;
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[self.tableView cellForRowAtIndexPath:indexPath]becomeFirstResponder];
    switch (indexPath.row) {
        case CellTagsTags:{
            
            [self performSegueWithIdentifier:@"TagSelected" sender:self];
            
            break;
            
        }
        case CellTagsFromDate:{
            UIViewController*popin =[[UIViewController alloc] init];
            UIDatePicker*p = [[UIDatePicker alloc] init];
            popin.view=p;
            p.datePickerMode = UIDatePickerModeDateAndTime;
            p.minuteInterval = 30;
            p.minimumDate = [NSDate date];
            p.tag = CellTagsFromDate;
            [p addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
            popin.view.backgroundColor = [UIColor whiteColor];
            [popin setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
            
            
            [popin setPopinTransitionDirection:BKTPopinTransitionDirectionBottom];
            
            [popin setPopinAlignment:BKTPopinAlignementOptionDown];
            
            [self presentPopinController:popin animated:YES completion:^{
                
            }];
            break;
        }
        case CellTagsToDate:{
            
            UIViewController*popin =[[UIViewController alloc] init];
            UIDatePicker*p = [[UIDatePicker alloc] init];
            popin.view=p;
            p.datePickerMode = UIDatePickerModeDateAndTime;
            p.minuteInterval = 30;
            p.minimumDate = [_startDate dateByAddingTimeInterval:60*30];
            p.maximumDate = [_startDate dateByAddingTimeInterval:60*60*24];
            
            p.tag = CellTagsToDate;
            
            [p addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
            
            popin.view.backgroundColor = [UIColor whiteColor];
            
            [popin setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
            
            
            [popin setPopinTransitionDirection:BKTPopinTransitionDirectionBottom];
            
            [popin setPopinAlignment:BKTPopinAlignementOptionDown];
            
            [self presentPopinController:popin animated:YES completion:^{
                
            }];
            break;
            
        }
        case CellTagsAddress:{
            [self performSegueWithIdentifier:@"POIViewSegue" sender:self];

            break;
            
        }
 
    }
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"TagSelected"]) {
        TagSelectedViewController*svc = segue.destinationViewController;

        [svc setSelectedBlock:^(NSArray* slectedTags){
            _tags = slectedTags;
            
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsTags inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        
        if ([_tags count]>0) {
            [svc setSelectedTagsWith:_tags];
        }
       
        
    }else{
        
        
        POIViewController*poi = segue.destinationViewController;
        
        [poi setSelectedBlock:^(Address *address,NSString*poi){
            
            _address = address;
            _poi = poi;
            
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:CellTagsAddress inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
    }

}


@end
