//
//  JoinPopupViewController.m
//  EventsHub
//
//  Created by alexander on 2/28/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "JoinPopupViewController.h"
#import <UIViewController+MaryPopin.h>
#import "UIHelper.h"
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
#import "MessageCenter.h"
#import "AccountManager.h"
@interface JoinPopupViewController ()<EHTabBarTappedDelegate,UITextFieldDelegate>{
    NSTimer*_timer;

}

@end

@implementation JoinPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 220);
    
    self.view.backgroundColor = [UIHelper whiteBackgroundColor];
    
    _actionBar.delegate = self;
    
    [_actionBar applyNewTitles:@[NSLocalizedString(@"加入", nil),NSLocalizedString(@"取消", nil)]];
    
    
    
    _phoneTextField.delegate = self;
    
    if (_needPhone) {
        
        _phoneTextField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:NSLocalizedString(@"加入活动需要联系电话", nil) attributes:@{NSForegroundColorAttributeName:[UIHelper orangeColor]}];
        
        _phoneTextField.floatingLabel.text =NSLocalizedString(@"联系电话", nil);
  
        _timer =[ NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(check) userInfo:nil repeats:YES];
        
        UIButton*btn = _actionBar.buttons[0] ;
        
        btn.enabled = NO;
    }else{
        
        [_phoneTextField setPlaceholder:NSLocalizedString(@"联系电话", nil) floatingTitle:NSLocalizedString(@"联系电话", nil)];
        
    }
    
    self.phoneTextField.floatingLabel.font = [UIHelper defaultFontWithSize:kMinimalFontSize];
    
    self.phoneTextField.textColor = [UIHelper darkColor];
    
    self.phoneTextField.font = [UIHelper defaultFontWithSize:16];
    
    UserAccount *account = [[AccountManager sharedAccountManager] userAccount];
    _phoneTextField.text = account.phone;
    
    
    self.noteTextView.placeholder = @"留言";

    

   
}

-(void)check{
    
    
    if (_needPhone&&_phoneTextField.text.length<1) {
        
        UIButton*btn = _actionBar.buttons[0] ;
        
        if (btn.enabled) {
            
            [btn setEnabled:NO];
        }
    }else{
        
        UIButton*btn = _actionBar.buttons[0] ;
        
        if (!btn.enabled) {
            
            [btn setEnabled:YES];

        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)checkPhone:(NSString*)phone{
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:phone defaultRegion:@"CN" error:&anError];
    if (anError == nil) {
        if (![phoneUtil isValidNumber:myNumber]) {
            return NO;
        }
    }
    return YES;
}

-(void)tabBarDidClickedButtonAtIndex:(NSInteger)index{
    
    if (index==0) {
        if (![self checkPhone:_phoneTextField.text]) {
            [MessageCenter showMessage:NSLocalizedString(@"请输入正确的联系电话", nil) withType:MessageTypeError inViewController:nil];
            return;
        }
        UserAccount *account = [[AccountManager sharedAccountManager] userAccount];
        if ([account.phone length]<1) {
            account.phone = _phoneTextField.text;
            [[AccountManager sharedAccountManager] updateUserInfo:account.toDictionary complete:^(bool succeed) {
            }];
        }
        [self.delegate performJoinWithData:@{@"note":_noteTextView.text,@"phone":_phoneTextField.text}];
    }
    
    
    [self.presentingPopinViewController dismissCurrentPopinControllerAnimated:YES];
    
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)dealloc
{
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
