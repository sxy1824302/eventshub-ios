//
//  EventDetailDataSource.m
//  EventsHub
//
//  Created by alexander on 4/2/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "EventDetailDataSource.h"
#import "EventsManager.h"
#import "Config.h"
#import "EventDetailViewController.h"
#import "UserProfileViewController.h"
#import "OtherProfileViewController.h"
#import "Event+Comments.h"
#import "CommentTableViewCell.h"
#import <TTTTimeIntervalFormatter.h>

#define kCellTag @"CellTag"

@interface EventDetailDataSource (){
    NSMutableArray*_sections;

    NSArray*_comments;
    
}
@end


@implementation EventDetailDataSource
- (instancetype)initWithViewController:(id)vc
{
    self = [super init];
    if (self) {
        
    
        self.viewController = vc;
        
        
        _sections = [NSMutableArray array];
    }
    return self;
}

-(void)reloadComments{
    [_event getLimitedComments:2 complete:^(NSArray *comments) {
        
        _comments = comments;
  
        NSMutableArray*comm = [NSMutableArray array];
        
        if (_comments.count>0) {
            [comm addObjectsFromArray:_comments];
            
            [comm addObject:@(0)];
        }else{
            [comm addObject:@(0)];
        }
        
        [_sections replaceObjectAtIndex:1 withObject:comm];
        
        [_viewController.tableview reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

-(void)initializeData{
    
    NSMutableArray*eventInfo = [NSMutableArray array];
    
    __block BOOL hide = NO;
    
    [[self.viewController navigationController].viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if([obj isKindOfClass:[EventDetailViewController class]]&&obj!=self.viewController.navigationController.topViewController){
            hide = YES;
            *stop = YES;
        }
    }];
    
    
    for (int i = 0; i<CellTagCount; i++) {
        if (hide&&(i==CellTagOwner||i==CellTagParticipants)) {
            
        }else{
            [eventInfo addObject:@{kCellTag:@(i)}];
        }
    }
    
    [_sections addObject:eventInfo];
    
    
    
    NSMutableArray*comments = [NSMutableArray array];
    
    [comments addObject:@(0)];
    
    [_sections addObject:comments];
}



#pragma mark tableview


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        NSInteger tag = [_sections[indexPath.section][indexPath.row][kCellTag] intValue];
        
        
        if (tag==CellTagEventPoster) {
            EventPostersTableViewCell*eicell = (EventPostersTableViewCell*)cell;
            [eicell reloadPhotoViewsWithImageAndURL:self.event.posters];
            
            self.viewController.posterCell = eicell;
            
        }else if (tag==CellTagDetail){
            cell.separatorInset = UIEdgeInsetsMake(0, self.viewController.view.frame.size.width, 0, 0);// 很猥琐的隐藏那个横线
            cell.backgroundColor = [UIColor clearColor];
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _sections.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        NSInteger tag = [_sections[indexPath.section][indexPath.row][kCellTag] intValue];
        
        if (tag==CellTagOwner) {
            
            
            [self.viewController performSegueWithIdentifier:@"OtherProfileSegue" sender:self.viewController];
            
            
        }else if (tag==CellTagParticipants){
            
            
            [self.viewController performSegueWithIdentifier:@"ParticipantsSegue" sender:self.viewController];
            
        }
    }else{
        
   
        [_viewController gotoComments];
        
        
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section==0) {
        
        NSInteger tag = [_sections[indexPath.section][indexPath.row][kCellTag] intValue];
        
    
        switch (tag) {
            case CellTagAddress:
                return 44;
                break;
            case CellTagOwner:
                return 62;
                break;
            case CellTagParticipants:
                return 62;
                break;
            case CellTagEventPoster:
                if (self.event.posters.count>0||IsMyEvent(self.event)) {
                    
                    return 76;
                }else{
                    
                    return 0;
                }
                break;
            case CellTagDetail:
                return 143;
                break;
            default:
                return 44;
                break;
        }
    }else{
        id obj = _sections[indexPath.section][indexPath.row];
        
        if (![obj isKindOfClass:[NSNumber class]]) {
    
            Comment*com = obj;

            
            return 60;
        }
    }

    return 44;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"section %i %i",section,[_sections[section] count]);
    
    return [_sections[section] count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;

    if (indexPath.section==0) {
        NSInteger tag = [_sections[indexPath.section][indexPath.row][kCellTag] intValue];
    
        
        switch (tag) {
            case CellTagAddress:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
                
                LocationTableViewCell*eicell = (LocationTableViewCell*)cell;
                
                NSArray*items =  @[self.event.address.city?:@"",self.event.address.district?:@"",self.event.address.street1?:@"",self.event.address.street2?:@"",self.event.pio?:@""];
                
                eicell.infoLabel.text =  [items componentsJoinedByString:@" "];
                
                UILabel*label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 40)];
                
                FAKFontAwesome *icon =[FAKFontAwesome mapMarkerIconWithSize:20];
                
                [icon addAttributes:@{NSForegroundColorAttributeName:[UIHelper blueThemeColor]}];
                label.attributedText = icon.attributedString;
                
                cell.accessoryView = label;
                break;
            }
            case CellTagOwner:{
                
                cell = [tableView dequeueReusableCellWithIdentifier:@"ownercell"];
                
                OwnerTableViewCell*ocell = (OwnerTableViewCell*)cell;
                
                
                [ocell.floatingLabel setPlaceholder:NSLocalizedString(@"发起者", nil) floatingTitle:NSLocalizedString(@"发起者", nil)];
                
                ocell.floatingLabel.text = self.event.owner_name;
                
                break;
            }
            case CellTagParticipants:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"ownercell"];
                OwnerTableViewCell*ocell = (OwnerTableViewCell*)cell;
                
                
                [ocell.floatingLabel setPlaceholder:NSLocalizedString(@"参与者", nil) floatingTitle:NSLocalizedString(@"参与者", nil)];
                
                ocell.floatingLabel.text = [NSString localizedStringWithFormat:@"当前%i人", self.event.participant_count.intValue];
                
                break;
            }
            case CellTagEventPoster:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"posterscell"];
                
                EventPostersTableViewCell*eicell = (EventPostersTableViewCell*)cell;
                eicell.isMine = IsMyEvent(self.event);
                eicell.delegate = _viewController;
                
                break;
            }
            case CellTagDetail:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"infocell"];
                
                EventInfoTableViewCell*eicell = (EventInfoTableViewCell*)cell;
                eicell.infoTextView.text = self.event.eventDescription;
                break;
            }
            default:
                break;
        }

    }else{
        id obj = _sections[indexPath.section][indexPath.row];
        
        if (![obj isKindOfClass:[NSNumber class]]) {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
            CommentTableViewCell*cmc = (CommentTableViewCell*)cell;
            Comment*com = obj;
            
            
            
            cmc.commentLabel.text = com.comment_text;
            
            cmc.nameLabel.text = com.user.username?:@"未知用户";
            
            
            FAKFontAwesome*fa;
            if ([com.user.gender isEqualToString:@"M"]) {
                fa = [FAKFontAwesome maleIconWithSize:12];
                [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper maleColor]];
            }else if ([com.user.gender isEqualToString:@"F"]) {
                fa = [FAKFontAwesome femaleIconWithSize:12];
                [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper femaleColor]];
            }else{
                fa = [FAKFontAwesome userIconWithSize:12];
                [fa addAttribute:NSForegroundColorAttributeName value:[UIHelper ghostColor]];
            }
            
            cmc.genderLabel.attributedText = fa.attributedString;
            
            cmc.commentDateLabel.text = com.formatedCommentDate;
            
            [cmc.profileView loadAvatarWithURLComponents:com.user.avatar_path];
            
            
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCommentCell"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EmptyCommentCell"];
            }
 
     
            FAKFontAwesome*com = [FAKFontAwesome commentsOIconWithSize:16];
            
            NSMutableAttributedString*att = [[NSMutableAttributedString alloc] initWithAttributedString:com.attributedString];

            if ([_sections[indexPath.section] count]>0) {
                [att appendAttributedString:[[NSAttributedString alloc] initWithString:@" 我来说说!" attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:14]}]];
                
            }else{
                [att appendAttributedString:[[NSAttributedString alloc] initWithString:@" 活动还没人评论哦，快去抢个沙发吧!" attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:14]}]];
                
            }
      


            [att addAttributes:@{NSForegroundColorAttributeName:[UIHelper greyColor]} range:NSMakeRange(0, att.length)];
                                 
            
            cell.textLabel.attributedText = att;
        }
 
    }
   
    
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}


@end
