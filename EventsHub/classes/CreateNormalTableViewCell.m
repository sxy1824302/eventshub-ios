//
//  CreateNormalTableViewCell.m
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "CreateNormalTableViewCell.h"
#import "UIHelper.h"
#import <KVOController/FBKVOController.h>
@implementation CreateNormalTableViewCell

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"text"]) {
        NSLog(@"text");
    }
}

- (void)awakeFromNib {
    self.nameLabel.font = [UIHelper defaultFontWithSize:GuidlineMetric(32)];
    self.nameLabel.textColor = [UIHelper stoneColor];
    
    self.textField.font = [UIHelper defaultFontWithSize:GuidlineMetric(32)];
    
    FBKVOController* fbKVO = [FBKVOController controllerWithObserver:self];
    [fbKVO observe:self keyPath:@"textField" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        NSLog(@"%@,%@,%@",observer,object,change);
    }];
}

-(void)overSeeTextChangeWitch:(TextChangeBlock*)changeBlock{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


// TODO: 重构
-(void)setReuiredName:(NSString *)name{
    
    NSMutableAttributedString*string = [[NSMutableAttributedString alloc] initWithString:name attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:GuidlineMetric(32)],NSForegroundColorAttributeName:[UIHelper stoneColor]}];
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:@"*" attributes:@{NSFontAttributeName:[UIHelper defaultFontWithSize:GuidlineMetric(32)],NSForegroundColorAttributeName:[UIHelper orangeColor]}]];
    
    
    self.nameLabel.attributedText = string;
}

@end
