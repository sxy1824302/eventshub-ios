//
//  ProfileConfig.h
//  EventsHub
//
//  Created by alexander on 2/27/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#ifndef EventsHub_ProfileConfig_h
#define EventsHub_ProfileConfig_h

typedef NS_ENUM(NSInteger, FieldTag) {
    FieldTagName,
    FieldTagGender,
    FieldTagPhone,
    FieldTagEmail,
    FieldTagCount,
};


typedef NS_ENUM(NSInteger, OtherUserFieldTag) {
    OtherUserFieldTagName,
//    OtherUserFieldTagPhone,
//    OtherUserFieldTagEmail,
    OtherUserFieldTagCreatedEventsCount,
    OtherUserFieldTagCount,
};

#endif
