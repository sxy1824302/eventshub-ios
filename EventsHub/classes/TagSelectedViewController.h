//
//  TagSelectedViewController.h
//  EventsHub
//
//  Created by apple on 15/1/4.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsManager.h"
#import "EventTagView.h"
@class  CreateActivityViewController;
typedef void(^TagsSelectedBloack)(NSArray *selectedTags);
@interface TagSelectedViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UICollectionView *selectedTagsView;


@property (weak, nonatomic) IBOutlet UICollectionView *showAllTagsCollectionView;

@property (nonatomic,strong)TagsSelectedBloack selectedBlock;

-(void)setSelectedTagsWith:(NSArray*)array;


@end
