//
//  EventsDataCache.m
//  EventsHub
//
//  Created by alexander on 3/19/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "EventsDataCache.h"

@implementation EventsDataCache
+ (id)sharedEventsDataCache
{
    static dispatch_once_t onceQueue;
    static EventsDataCache *eventsDataCache = nil;
    
    dispatch_once(&onceQueue, ^{ eventsDataCache = [[self alloc] init]; });
    return eventsDataCache;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _events = [NSMutableArray array];
    }
    return self;
}

-(void)appendEvents:(NSArray*)events{
    [_events addObjectsFromArray:events];
}
@end
