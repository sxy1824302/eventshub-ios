//
//  SettingViewController.m
//  EventsHub
//
//  Created by 亮 姚 on 14-11-18.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "SettingViewController.h"
#import "AccountManager.h"
#import "NetworkManager.h"
#import "UIHelper.h"
#import "MenuViewViewController.h"
#import <SDImageCache.h>
#import "LocationManager.h"
#import "QuickSignInViewController.h"
@import StoreKit;

@interface SettingViewController ()<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,SKStoreProductViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *copyrightLabel;
@end

@implementation SettingViewController{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"设置", nil);
    self.tableView.tableFooterView = [UIView new];
    
    
    
    self.copyrightLabel.textColor = [UIHelper stoneColor];
    self.versionLabel.textColor = [UIHelper stoneColor];
    
    self.versionLabel.text = [NSString localizedStringWithFormat:@"版本 %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];//[NSString stringWithFormat:@"版本 %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView) name:User_LoginNotification object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}
-(void)goToAppStore
{
    SKStoreProductViewController *spvc =[[SKStoreProductViewController alloc] init];
    spvc.delegate = self;
    [spvc loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier:@"915681805"} completionBlock:^(BOOL result, NSError *error) {
        
    }];
    [self presentViewController:spvc animated:YES completion:^{
        
    }];
}
-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)logOutAction{
    [[AccountManager sharedAccountManager] userLogOut];
}

-(void)reloadTableView{
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return SettingsTagCount;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.textColor = [UIHelper darkColor];
     cell.accessoryType = UITableViewCellAccessoryNone;
    
    switch (indexPath.row) {
        case SettingsTagCity:{
            NSString*city = [[LocationManager sharedLocationManager] eventCity];
            
            if (city.length<1) {
                city = NSLocalizedString(@"WrongCity", nil);
                cell.textLabel.textColor = [UIHelper orangeColor];
            }
            cell.textLabel.text = city;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        }
        case SettingsTagAboutUs:
            cell.textLabel.text = NSLocalizedString(@"关于", nil);
            break;
        case SettingsTagWriteReview:
            cell.textLabel.text = NSLocalizedString(@"留下评价", nil);
            break;
        case SettingsTagHelpAndContact:
            cell.textLabel.text = NSLocalizedString(@"帮助与反馈", nil);
            break;
        case SettingsTagCleanCache:
            cell.textLabel.text = NSLocalizedString(@"清除缓存", nil);
            cell.textLabel.textColor = [UIHelper orangeColor];
            break;
        case SettingsTagLogout:
            cell.textLabel.text = [[AccountManager sharedAccountManager] checkUserSigned]?NSLocalizedString(@"退出登录", nil):NSLocalizedString(@"登录",nil);
            break;
        default:
            break;
    }
    
    
    return cell;
}
#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case SettingsTagAboutUs:
            [self performSegueWithIdentifier:@"AboutUsSegue" sender:self];
            break;
        case SettingsTagCleanCache:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CleanCacheTitle", nil) message:NSLocalizedString(@"CleanCacheMessage",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"不", nil) otherButtonTitles:NSLocalizedString(@"是的", nil), nil];
            alert.tag = indexPath.row;
            [alert show];
            break;
        }
        case SettingsTagLogout:
        {
            if ([[AccountManager sharedAccountManager] checkUserSigned]) {
                UIAlertView*alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LogoutTitle", nil) message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"不", nil) otherButtonTitles:NSLocalizedString(@"是的", nil), nil];
                alert.tag = indexPath.row;
                [alert show];
            }else{
                [self showInAppAuthView];
            }
            break;
        }
        case SettingsTagCity:
            [self performSegueWithIdentifier:@"CitySegue" sender:self];
            break;
        case SettingsTagWriteReview:
            [self goToAppStore];
            break;
        case SettingsTagHelpAndContact:
            [self performSegueWithIdentifier:@"HelpContactSegue" sender:self];
            break;
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex!=alertView.cancelButtonIndex) {
        switch (alertView.tag) {
            case SettingsTagCleanCache:{
                [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
                    UIAlertView*alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"CleanCacheComplete", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"好的", nil) otherButtonTitles:nil];
                    [alert show];
                }];
                break;
            }
            case SettingsTagLogout:
                [self logOutAction];
                [(MenuViewViewController*)self.sideMenuViewController.leftMenuViewController showMainPage];
                break;
            default:
                break;
        }
    }
}
@end
