//
//  PushValueTableViewCell.h
//  EventsHub
//
//  Created by alexander on 1/12/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ValueChangedDelegate.h"
@interface PushValueTableViewCell : UITableViewCell
@property (nonatomic)UILabel *accessoryLabel;
@property (nonatomic)id value;
@property (nonatomic,weak)id<ValueChangedDelegate>delegate;
@property (nonatomic)BOOL valid;
-(void)setShowArraw:(BOOL)show;
-(void)setReuiredName:(NSString *)name;
@end
