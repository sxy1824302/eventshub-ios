//
//  Event+Join.m
//  EventsHub
//
//  Created by 亮 姚 on 14-11-29.
//  Copyright (c) 2014年 Uriphium. All rights reserved.
//

#import "Event+Join.h"
#import "NetworkManager.h"
#import "AccountManager.h"
@implementation Event (Join)

-(void)joinWithData:(NSDictionary*)data   complete:(void(^)(NSDictionary* responce,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        UserAccount*account = [[AccountManager sharedAccountManager] userAccount];
        
        NSMutableDictionary *dict = data.mutableCopy;
        
        [dict addEntriesFromDictionary:@{@"username":account.username,@"registered_user_ref":[[AccountManager sharedAccountManager] currentAccountID]}];
        
        [networkManager.JSONEncodeHttpRequestManager POST:[NSString stringWithFormat:@"/events/%@/join",self._id] parameters:dict success:^(NSURLSessionDataTask *task, id responseObject) {
           
            completeBlock(responseObject,YES);
         
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            completeBlock(responseObject,NO);
            failed(error,responseObject);;
        }];
    }];
}

-(void)deleteParticipantWithUserID:(NSString*)userID andHandle:(void(^)(NSDictionary* responce,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.fileHttpRequestManager DELETE:[NSString stringWithFormat:@"/events/%@/participants/%@",self._id,userID] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"code"] integerValue]==1) {
                completeBlock(responseObject,YES);
            }else{
                completeBlock(responseObject,NO);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            completeBlock(responseObject,NO);
            failed(error,responseObject);;
            
        }];
    }];
}

-(void)checkJoinedWithUserID:(NSString*)userID andHandle:(void(^)(NSDictionary* responce,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        

        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/events/%@/participants/%@",self._id,userID] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
       
            completeBlock(responseObject,YES);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            completeBlock(responseObject,YES);
            failed(error,responseObject);;
        }];
        
    }];
}

@end
