//
//  AboutEventHubViewController.m
//  EventsHub
//
//  Created by apple on 15/3/12.
//  Copyright (c) 2015年 Uriphium. All rights reserved.
//

#import "AboutEventHubViewController.h"

@interface AboutEventHubViewController ()
@property (weak, nonatomic) IBOutlet UITextView *aboutEventHubTextView;

@end

@implementation AboutEventHubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"关于聚吧", nil);
    self.aboutEventHubTextView.text = NSLocalizedString(@"AboutEventHubDetail", nil);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
