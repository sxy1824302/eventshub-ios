//
//  Participant.h
//  EventsHub
//
//  Created by alexander on 1/9/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import "JSONModel.h"

@interface Participant : JSONModel

@property (nonatomic)NSString*date_attend;

@property (nonatomic)NSString*date_reserve;

@property (nonatomic)NSString*note;

@property (nonatomic)NSString*phone;

@property (nonatomic)NSString*registered_user_ref;

@property (nonatomic)NSString*username;

@property (nonatomic)NSNumber*check;

@end
