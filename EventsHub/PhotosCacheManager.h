//
//  PhotosCacheManager.h
//  EventsHub
//
//  Created by alexander on 1/30/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoBrowserViewController.h"

@protocol PhotoBrowsingDelegate <NSObject>



@optional
-(void)foredQuitPhotoBrowser;
-(void)deletePhotoAtIndex:(NSInteger)idx;
@end


@interface PhotosCacheManager : NSObject<PhotoBrowserViewActionDelegate,MWPhotoBrowserDelegate>

+ (id)sharedPhotosCacheManager;
-(void)cleanDsiplayCache;
@property (nonatomic,weak)UIViewController<PhotoBrowsingDelegate>*currentphotoBrowserBaserViewController;
@property (nonatomic,readonly)NSMutableArray*cachedImages;
-(void)displayImages:(NSArray*)images fromViewController:(UIViewController<PhotoBrowsingDelegate>*)vc showDelete:(BOOL)showDelete;
@end
