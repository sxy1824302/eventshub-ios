//
//  EventsManager.h
//  EventsHub
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

@import Foundation;
@import UIKit;


#import "Address.h"
#import "NSDate+ISO8601.h"
#import "Tag.h"
#import "AccountManager.h"
#import "Event+Update.h"
#import "Event+Interest.h"
#import "Event+Display.h"
#import "Event.h"
#import "Event+Participant.h"
#import "Event+Register.h"


#define IsMyEvent(__event__) [[[AccountManager sharedAccountManager] currentAccountID] isEqualToString:__event__.owner]



@interface EventsManager : NSObject
@property (nonatomic)BOOL moduleInitialized;

@property (nonatomic)NSArray*eventTags;

-(void)initializeModule;

+ (id)sharedEventsManager;



-(void)getAttendedActivity:(void(^)(NSArray* events,BOOL success))completeBlock;

-(void)getCreatedActivity:(void(^)(NSArray* events,BOOL success))completeBlock;

-(void)getInterestedEvents:(void(^)(NSArray* events))completeBlock;

-(void)getUserEvents:(NSString*)userID complete:(void(^)(NSArray* events,BOOL success))completeBlock;

-(void)createEventWithEventDic:(NSDictionary*)eventDic andPosters:(NSArray*)posters progress:(void(^)(CGFloat percentage))percentage succeed:(void(^)(BOOL succeed,NSString* idString))succeedBlock;

-(void)addPosters:(NSArray*)images toEvent:(NSString*)eventID progress:(void(^)(CGFloat percentage))percentage succeed:(void(^)(NSArray* addedPosters))succeed;


-(void)deletePoster:(NSString*)posterPath toEvent:(NSString*)eventID  succeed:(void(^)(BOOL succeed))succeed;


-(void)getEventsTags:(void(^)(NSArray* tags))completeBlock;


-(NSString*)tagPinyinFromTagID:(NSNumber*)tag_id;

@end
