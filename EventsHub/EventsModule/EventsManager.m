//
//  EventsManager.m
//  EventsHub
//
//  Created by alexander on 11/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventsManager.h"
#import "NetworkManager.h"
#import "AccountManager.h"
#import "MessageCenter.h"
#import "Config.h"
#import <FBKVOController.h>
#import "ImageDataConvertor.h"
@implementation EventsManager{
    FBKVOController*_progressObserver;
}
+ (id)sharedEventsManager
{
    static dispatch_once_t onceQueue;
    static EventsManager *manager = nil;
    
    dispatch_once(&onceQueue, ^{ manager = [[self alloc] init]; });
    return manager;
}


-(void)initializeModule{
    
    [[EventsManager sharedEventsManager] getEventsTags:^(NSArray *tags) {
        
        _moduleInitialized = YES;
        
        
    }];
    
}


-(id)init{
    self = [super init];
    if (self) {
        _progressObserver =[[FBKVOController alloc] initWithObserver:self retainObserved:NO];
    }
    return self;
}

-(void)createEventWithEventDic:(NSDictionary*)eventDic andPosters:(NSArray*)posters progress:(void(^)(CGFloat percentage))percentage succeed:(void(^)(BOOL succeed,NSString* idString))succeedBlock{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager POST:@"/events" parameters:eventDic success:^(NSURLSessionDataTask *task, id responseObject) {
            
             NSString *ID = responseObject[@"_id"];
            if (posters.count>0) {
           
                
                [self addPosters:posters toEvent:ID progress:percentage succeed:^(NSArray* addedPosters) {
                    if (addedPosters.count>0) {
                        
                        succeedBlock(YES,ID);
                        
                        [MessageCenter showMessage:@"活动创建成功" withType:MessageTypeSuccess inViewController:nil];
                        
                    }else{
                        failed(nil,responseObject);
                        succeedBlock(NO,nil);
                    }
                }];
                
            }else{
                 succeedBlock(YES,ID);
                [MessageCenter showMessage:@"活动创建成功" withType:MessageTypeSuccess inViewController:nil];
            }
            
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            failed(error,responseObject);;
            succeedBlock(NO,nil);
        }];
        
    }];
    
    
}




-(void)addPosters:(NSArray*)images toEvent:(NSString*)eventID progress:(void(^)(CGFloat percentage))percentage succeed:(void(^)(NSArray* addedPosters))succeed{
    
    if (images.count<1||images.count>3) {
        return;
    }
    
 
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        NSProgress*progress;
        
        [networkManager.fileHttpRequestManager POST:[NSString stringWithFormat:@"/events/%@/poster",eventID]  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            __block NSString*imageType;
            [ImageDataConvertor convertImage:images forEachImages:^(NSData *aData, float compression, float megabytes, NSString *httpType) {
                
                imageType = httpType;
                
                DDLogVerbose(@"total image size: %f mb",megabytes);
                
                [formData appendPartWithFileData:aData name:@"image" fileName:[NSString stringWithFormat:@"%@-%@",[NSDate date].description,[[AccountManager sharedAccountManager] currentAccountID]] mimeType:imageType];
            }];
            
            
        }progress:&progress success:^(NSURLSessionDataTask *task, id responseObject) {
            
            succeed(responseObject[@"files"]);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
          
            
        }];
     
        [_progressObserver observe:progress keyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            NSNumber*value = change[NSKeyValueChangeNewKey];
            
            percentage(value.floatValue);
        }];
    }];
}


-(void)deletePoster:(NSString*)posterPath toEvent:(NSString*)eventID  succeed:(void(^)(BOOL succeed))succeed{
    DDLogVerbose(@"delete:%@",posterPath);
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.fileHttpRequestManager DELETE:[NSString stringWithFormat:@"/events/%@/poster/%@",eventID,posterPath]  parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            succeed(YES);
        } failure:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            succeed(NO);
            failed(error,responseObject);
        }];
        
    }];
}


-(void)getEventsTags:(void(^)(NSArray* tags))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:@"/tags" parameters:@{@"locale":CURRENT_LOCALE_IDENTIFIER} success:^(NSURLSessionTask *task, id responseObject) {

            self.eventTags = [Tag arrayOfModelsFromDictionaries:responseObject[@"tags"]];

            completeBlock(responseObject);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            
            completeBlock(nil);
        }];
    }];
}

-(NSString*)tagPinyinFromTagID:(NSNumber*)tag_id{
    __block NSString*pinyin;
    [self.eventTags enumerateObjectsUsingBlock:^(Tag* obj, NSUInteger idx, BOOL *stop) {
        if ([obj.tag_id isEqualToNumber:tag_id]) {
            pinyin = obj.pinyin;
            *stop = YES;
        }
    }];
    return pinyin;
}


//获取当前活动参与者的列表
-(void)getParticipantsWithEventID:(NSString*)eventID andhandle:(void(^)(NSArray* participants))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/events/%@/participants",eventID] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"%@",task.response.URL);
            completeBlock(responseObject[@"participants"]);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            NSLog(@"%@",task.response.URL);
            completeBlock(nil);
            
        }];
    }];
}


#pragma mark - GETTERS


//参加的活动
-(void)getAttendedActivity:(void(^)(NSArray* events,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:@"/joint_events" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            NSError *error;
            NSArray *events = [Event arrayOfModelsFromDictionaries:responseObject[@"events"] error:&error];
            if (!error) {
                 completeBlock(events,YES);
            }else{
                
                 completeBlock(nil,NO);
                failed(error,responseObject);;
            }

        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            failed(error,responseObject);;
            completeBlock(nil,NO);
        }];
    }];
}


//自己创建的活动
-(void)getCreatedActivity:(void(^)(NSArray* events,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:@"/created_events" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            NSError *error;
            NSArray *events = [Event arrayOfModelsFromDictionaries:responseObject[@"events"] error:&error];
            if (events) {
                completeBlock(events,YES);
            }else{
                failed(error,responseObject);;
                completeBlock(nil,NO);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            failed(error,responseObject);;
            completeBlock(nil,NO);
        }];
    }];
}


//自己创建的活动
-(void)getUserEvents:(NSString*)userID complete:(void(^)(NSArray* events,BOOL success))completeBlock{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"/users/%@/events",userID] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            NSError *error;
            NSArray *events = [Event arrayOfModelsFromDictionaries:responseObject[@"events"] error:&error];
            if (events) {
                completeBlock(events,YES);
            }else{
                failed(error,responseObject);;
                completeBlock(nil,NO);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            failed(error,responseObject);;
            completeBlock(nil,NO);
        }];
    }];
}


-(void)getInterestedEvents:(void(^)(NSArray* events))completeBlock{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:@"/liked_events" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            NSError *error;
            
            NSArray *events = [Event arrayOfModelsFromDictionaries:responseObject[@"events"] error:&error];
            if (events) {
                completeBlock(events);
            }else{
                failed(error,responseObject);;
                completeBlock(nil);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error,id responseObject) {
            failed(error,responseObject);;
            completeBlock(nil);
        }];
    }];
    
}
@end
