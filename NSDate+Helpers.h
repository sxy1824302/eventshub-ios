//
//  NSDate+Helpers.h
//  EventsHub
//
//  Created by alexander on 1/7/15.
//  Copyright (c) 2015 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helpers)
- (NSDate *)beginningOfWeek ;
- (NSDate *)endOfWeek ;
- (NSDate *)endOfNextWeek ;
- (NSDate *)beginningOfMonth ;
- (NSDate *)endOfMonth ;
- (NSDate *)endOfNextMonth ;




+ (NSDate*)dateFromYear:(NSInteger)year;
@end
